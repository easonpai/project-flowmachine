package com.appmachine.flowmachinedemo;

import android.app.Dialog;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.appmachine.flowmachine.Constants;
import com.appmachine.flowmachine.R;
import com.appmachine.flowmachine.components.interfaces.Executable;
import com.appmachine.flowmachine.components.interfaces.Types;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements NarrativeAnimation.HostAdapter {

    RequestQueue volleyQueue;

    Spinner spinnerProjects, spinnerDataInput, spinnerStates, spinnerAccetableEvents;
    //    Spinner spinnerGotoState;
    AutoCompleteTextView eventText;
    String[] localAppPaths = new String[]{"test_basics.json", "test_p_parallel_hierarchy.json", "test_p_deep_geofence_ap.json", "test_p_2_geofence.json", "test_p_scratch_ap_lo.json", "test_p_scratch_ap_cl.json", "test_p_simple_fork_after_p.json", "test_p_fork_escape.json", "test_parallel_aprfoo.json", "test_parallel.json"};
    String[] pathSounds = new String[]{"typewriter-2.mp3", "typewriter-paper-roll-up-1.mp3"};
    private int _localAppLength = 0;
    TextView textLog, textAppTitle;
    Dialog dialogSimulation;
    public final String local_db = "story.json";
    public final String firebase_db = "https://flowbaker-9d2ed.firebaseio.com/stages";
    public final String firebase_db_stages = "https://flowbaker-9d2ed.firebaseio.com/stages.json";
    public StringBuilder storyLog;
    public String systemLog;
    public boolean isShowSystemLog = false;
    private Map stageApps;
    ArrayList<String> projectTitles;
    private ArrayList<String> arrayDataInput; // store data for further event trigger
    public String currentGpsState;
    HashMap<String, String> pendingEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init volley
        volleyQueue = Volley.newRequestQueue(this);
        storyLog = new StringBuilder();
        systemLog = "";
        projectTitles = new ArrayList<>();

        initMainView();
        initSimulationView();
        initFlow();

        pendingEvents = new HashMap<>();

        NarrativeAnimation.init(this);
    }

    private void initFlow() {

        arrayDataInput = new ArrayList<>();

        FlowMachineAPIs.init(this);
        FlowMachineAPIs.machine.cleanListeners()
                .on(Types.TTS, new Executable() {
                    @Override
                    public void execute(String state, Map<String, ?> property) {
                        trace(" [IRP][State] " + getProperty(Constants.Key.TYPE) + " (" + getProperty(Constants.Key.ID) + ")");
//                        trace( "\t > input = "+ getProperty(Constants.Key.TYPE).toString() +", "+ getProperty("text").toString() );
//
//                        // we use an external API to replace the behavior of internal TTS component
//
//                        ArrayList<String> tts = new ArrayList<>();
//                        tts.add(getProperty("text").toString());
//                        DemoActivity.this.displayNarrative(state, getProperty("type").toString(), "TTS", "系統", tts);

                        trace("\t > input = " + getProperty(Constants.Key.TYPE).toString() + ", " + getProperty("chapter").toString() + ", " + getProperty("char").toString() + ", " + (List) getProperty("dialog"));
                        MainActivity.this.displayNarrative(state, getProperty("type").toString(), getProperty("chapter").toString(), getProperty("char").toString(), (List) getProperty("dialog"));
                    }
                })
                .on(Types.PLAY_SOUND, new Executable() {
                    @Override
                    public void execute(String state, Map<String, ?> property) {
                        trace(" [IRP][State] " + getProperty(Constants.Key.TYPE) + " (" + getProperty(Constants.Key.ID) + ")");
                        log("\t > input = " + getProperty("source") + "," + getProperty("repeat") + "," + getProperty("crossScene"));

                        audioPlayer("media", pathSounds[0]);

                        // there is an external API for PLAY_SOUND
                    }
                })
                .on(Types.TAIPEILEGEND_NARRATIVE, new Executable() {
                    @Override
                    public void execute(String state, Map<String, ?> property) {
                        trace(" [IRP][State] " + getProperty(Constants.Key.TYPE) + " (" + getProperty(Constants.Key.ID) + ")");
                        trace("\t > input = " + getProperty(Constants.Key.TYPE).toString() + ", " + getProperty("chapter").toString() + ", " + getProperty("char").toString() + ", " + (List) getProperty("dialog"));

                        // there is an external API of TAIPEILEGEND_NARRATIVE

                        MainActivity.this.displayNarrative(state, getProperty("type").toString(), getProperty("chapter").toString(), getProperty("char").toString(), (List) getProperty("dialog"));
                    }
                })
                .on(Types.GEO_FENCE, new Executable() {

                    @Override
                    public void execute(String state, Map<String, ?> property) {
                        trace(" [IRP][State] " + getProperty(Constants.Key.TYPE) + " (" + getProperty(Constants.Key.ID) + ")");
                        trace("\t > input = " + ((List) getProperty("gps")));

                        // add data list to demo app

                        updateDataInputSpinner((List) getProperty("gps"));

                        MainActivity.this.storyLog.append("[ 進入 Geo Fence (" + getProperty(Constants.Key.ID) + ") , 請使用Simulator ]\n");

                    }
                });
    }

    private void initMainView() {

        // app title

        textAppTitle = (TextView) findViewById(R.id.lb_app_title);
        textAppTitle.setText(FlowMachineAPIs.AppTitle);

        // app spinner
        // inflate data from local and add to front

        spinnerProjects = (Spinner) findViewById(R.id.spinner_app);

        for (String path : localAppPaths) {
            projectTitles.add(path);
            _localAppLength++;
        }
        resetLocalApps();
        spinnerProjects.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, projectTitles.toArray()));


        // log view

        textLog = (TextView) findViewById(R.id.tv_log);
        textLog.setMovementMethod(new ScrollingMovementMethod());
        textLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                trace("NarrativeAnimation.isRunning = " + NarrativeAnimation.isRunning);
//                if (!NarrativeAnimation.isRunning) {
                // interaction with story
//                    completeNarrative();
//                pendingEvents.clear();
//                }
            }
        });

        Button btnLoad = (Button) findViewById(R.id.btn_load);
        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getStageProjects();
            }
        });

        // ------------------------
        // init state
        // ------------------------

        final EditText et_initstate = (EditText) findViewById(R.id.et_initstate);


        // ------------------------
        // event input
        // ------------------------

        Button btnRun = (Button) findViewById(R.id.btn_run);
        btnRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trace("getSelectedItem = " + spinnerProjects.getSelectedItem() + ", getSelectedItemPosition = " + spinnerProjects.getSelectedItemPosition());
                runApp(spinnerProjects.getSelectedItemPosition(), et_initstate.getText().toString());
            }
        });

        Button btnHistory = (Button) findViewById(R.id.btn_history);
        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlowMachineAPIs.logHistory();
            }
        });

    }

    private void resetLocalApps() {
        projectTitles.clear();
        _localAppLength = 0;
        for (String path : localAppPaths) {
            projectTitles.add(path);
            _localAppLength++;
        }
    }


    /**
     * init simulator view
     */
    private void initSimulationView() {

        Button btnSimulations = (Button) findViewById(R.id.btn_sim);
        btnSimulations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSimulationWindow();
            }
        });

        dialogSimulation = new Dialog(MainActivity.this);
        dialogSimulation.setContentView(R.layout.simmulation_dialog);

        // ------------------------
        // acceptable events
        // ------------------------

        spinnerAccetableEvents = (Spinner) dialogSimulation.findViewById(R.id.spinner_acceptables);
        Button btn_acceptables_fire = (Button) dialogSimulation.findViewById(R.id.btn_accetables_fire);
        btn_acceptables_fire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinnerAccetableEvents.getSelectedItem().toString().trim().length() > 0) {
                    dialogSimulation.cancel();
                    FlowMachineAPIs.fire(spinnerAccetableEvents.getSelectedItem().toString());
                }
            }
        });

        // ------------------------
        // event input
        // ------------------------

        et_event_input = (EditText) dialogSimulation.findViewById(R.id.et_event_input);

        Button btn_event_input = (Button) dialogSimulation.findViewById(R.id.btn_event_input);
        btn_event_input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_event_input.getText().toString().trim().length()>0){
                    dialogSimulation.cancel();
                    FlowMachineAPIs.fire(et_event_input.getText().toString());
                }
            }
        });

        // ------------------------
        // all states
        // ------------------------

        spinnerStates = (Spinner) dialogSimulation.findViewById(R.id.spinner_statelist);
        Button btn_statelist_fire = (Button) dialogSimulation.findViewById(R.id.btn_statelist_fire);
        btn_statelist_fire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinnerStates.getSelectedItem().toString().trim().length() > 0) {
                    dialogSimulation.cancel();
                    FlowMachineAPIs.fire(spinnerStates.getSelectedItem().toString());
                }
            }
        });

        Button btn_statelist_goto = (Button) dialogSimulation.findViewById(R.id.btn_statelist_goto);
        btn_statelist_goto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinnerStates.getSelectedItem().toString().trim().length() > 0) {
                    dialogSimulation.cancel();
                    FlowMachineAPIs.gotoState(spinnerStates.getSelectedItem().toString());
                }
            }
        });

        // ------------------------
        // data input
        // ------------------------

        spinnerDataInput = (Spinner) dialogSimulation.findViewById(R.id.spinner_data_input);
        et_data_input = (EditText) dialogSimulation.findViewById(R.id.et_data_input);
        Button btnDataInput = (Button) dialogSimulation.findViewById(R.id.btn_data_input);
        btnDataInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_data_input.getText().toString().trim().length() > 0) {
                    dialogSimulation.cancel();
                    FlowMachineAPIs.fire(getFireEvent(), et_data_input.getText().toString());
                }

                else if (spinnerDataInput.getSelectedItem().toString().trim().length() > 0) {
                    dialogSimulation.cancel();
                    FlowMachineAPIs.fire(getFireEvent(), spinnerDataInput.getSelectedItem().toString());
                }
            }
        });


    }

    EditText et_data_input;
    EditText et_event_input;
    public String getFireEvent() {

        if(et_event_input.getText().toString().trim().length()>0){
            return et_event_input.getText().toString();
        }

        else if(spinnerStates.getSelectedItem().toString().trim().length() > 0){
            return spinnerStates.getSelectedItem().toString();
        }

        else if (spinnerAccetableEvents.getSelectedItem().toString().trim().length() > 0){
            return spinnerAccetableEvents.getSelectedItem().toString();
        }

        return null;
    }

    private void openSimulationWindow() {

        updateAcceptableEventsSpinner(FlowMachineAPIs.getAcceptableEvents());

        dialogSimulation.show();
    }

    private void runApp(int selectedItemPosition, String initState) {

        // see if this is a local app

        if (selectedItemPosition < _localAppLength) {
            FlowMachineAPIs.setApp(getLocalConfig(localAppPaths[selectedItemPosition]), initState);
        } else {
            Collection apps = stageApps.values();
            Object app = apps.toArray()[selectedItemPosition - _localAppLength];

            // form a standard project config

            HashMap<String, Object> project = new HashMap<>();
            project.put("project", app);

            // set app
            FlowMachineAPIs.setApp(project, initState);
        }

        // update views

        updateLog();
        textLog.scrollTo(0, 0);
        updateStatesSpinner(FlowMachineAPIs.getAllStates());
    }


    public void audioPlayer(String path, String fileName) {
        //set up MediaPlayer
        MediaPlayer mp = new MediaPlayer();


        try {
            AssetFileDescriptor descriptor = getAssets().openFd(path + File.separator + fileName);
            mp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
//            mp.setDataSource(path + File.separator + fileName);
            mp.prepare();
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayNarrative(final String stateId, final String type, String chapter, String role, List<String> narratives) {
//        DemoActivity.this.addText("["+chapter+"]["+role+"]"+narratives.get(0)+" (click to continue)\n");
//        pendingEvents.put(stateId, stateId);

        // use an module to do text animation

        NarrativeAnimation.animateText(chapter, role, narratives, new NarrativeAnimation.EndAction() {
            @Override
            public void complete(String extra) {

                MainActivity.this.addText("(" + stateId + ")\n");

                // store state id as event keys
                pendingEvents.put(extra, extra);

            }
        }, stateId);

    }

    private void updateProjectSpinner(Map stages) {

        stageApps = stages;

        // inflate data from cloud
        resetLocalApps();

        for (Object item : stages.values()) {
            projectTitles.add(((Map) item).get("name").toString());
        }

        spinnerProjects.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, projectTitles.toArray()));
    }

    private void updateStatesSpinner(List<String> states) {
        ArrayList<String> stateList = new ArrayList<>();

        // add empty item

        stateList.add("");

        for (String state : states) {
            stateList.add(state);
        }
        spinnerStates.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, stateList.toArray()));
    }

    private void updateAcceptableEventsSpinner(List<String> states) {

        // add empty item

        states.add("");

        spinnerAccetableEvents.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, states.toArray()));
    }

    private void updateDataInputSpinner(List data) {

        arrayDataInput.addAll(data);

        spinnerDataInput.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, arrayDataInput.toArray()));
    }

    public void updateLog() {
        if (isShowSystemLog) {
            textLog.setText(systemLog);
        } else {
            textLog.setText(storyLog.toString());
        }
    }


    public void getCloudConfig(String url, Response.Listener<String> listener) {

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                listener, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        // Add the request to the RequestQueue.
        volleyQueue.add(stringRequest);
    }

    private void getStageProjects() {
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, firebase_db_stages,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        updateProjectSpinner(FlowMachineAPIs.machine.getConfigModelManager().parseJsonData(response));
                        spinnerProjects.performClick();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        // Add the request to the RequestQueue.
        volleyQueue.add(stringRequest);
    }


    @Nullable
    public String getLocalConfig(String path) {
        String json = null;
        try {
            InputStream is = getAssets().open(path);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    // =====================================
    // interfaces
    // =====================================

    public void addText(String text) {
        storyLog.append(text);
        updateLog();
    }

    public void delete(int offset) {
        storyLog.delete(offset, storyLog.length());
    }

    public int getTextLength() {
        return storyLog.length();
    }

    // =====================================
    // UTILITY
    // =====================================

    public void trace(Object msg) {
        Log.v(MainActivity.this.getLocalClassName(), msg.toString());
    }

}
