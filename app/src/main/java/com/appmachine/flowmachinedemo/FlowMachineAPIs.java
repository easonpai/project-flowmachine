package com.appmachine.flowmachinedemo;

import android.util.Log;

import com.appmachine.flowmachine.FlowMachine;
import com.appmachine.flowmachine.components.interfaces.Executable;
import com.appmachine.flowmachine.components.interfaces.Types;

import java.util.List;
import java.util.Map;

import static com.appmachine.flowmachine.Constants.Key.FLOWENGINE;

/**
 * Created by eason on 2016/11/14.
 * <p>
 * This class is design to act as a learning material of flow machine
 */

public class FlowMachineAPIs {

    static FlowMachine machine;
    private static MainActivity mDemoActivity;
    static String AppTitle = "Flow Demo (v"+FlowMachine.getVersion() +")";


    public static void init(MainActivity activity) {
        mDemoActivity = activity;
        try {
            machine = new FlowMachine();
        }catch (Exception ex) {
            trace("FlowMachine Exception = " + ex);
        }
    }


    public static void initFlowMachine() {
        machine = new FlowMachine();
    }


    public static void setApp(String appJson, String initState) {
        mDemoActivity.storyLog.delete(0, mDemoActivity.storyLog.length());
        machine.setApp(appJson, initState, true);
        mDemoActivity.updateLog();
    }
    public static void setApp(Map appJson, String initState) {
        mDemoActivity.storyLog.delete(0, mDemoActivity.storyLog.length());
        if(initState.length()==0){
            machine.setApp(appJson, true);
        }else{
            machine.setApp(appJson, initState, true);
        }
        mDemoActivity.updateLog();
    }
    public static void fire(String event, String command, String data) {
        machine.fire(new FlowMachine.EngineEvent().setEvent(event).setData(data));
        mDemoActivity.updateLog();
    }
    public static void fire(String event, String data) {
        machine.fire(new FlowMachine.EngineEvent().setEvent(event).setData(data));
        mDemoActivity.updateLog();
    }

    public static void fire(String event) {
        try{
            machine.fire(new FlowMachine.EngineEvent().setEvent(event));
        }catch (Exception ex){
            trace("FlowMachine Exception = " + ex);
        }
        mDemoActivity.updateLog();
    }


    public static List<String> getAcceptableEvents() {
        return machine.getAcceptableEvents();
    }

    public static String getState() {
        return machine.getState().toString();
    }

    public static List<String> getAllStates() {
        return machine.getAllRawStates();
    }


    public static void gotoState(String s) {
        machine.gotoState(s);
    }

    public static Executable getComponentAction(Types component) {
        return machine.getComponentAction(component);
    }

    // =====================================
    // UTILITY
    // =====================================
    public static void trace(String msg) {
        Log.v(FLOWENGINE, msg);
    }

    public static void logHistory() {
        mDemoActivity.storyLog.append(machine.logHistory());
        mDemoActivity.updateLog();
    }
}