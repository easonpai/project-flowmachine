package com.appmachine.flowmachinedemo;

import java.util.List;

/**
 * Created by easonp on 2016/12/3.
 */

public class NarrativeAnimation {

    static HostAdapter hostView;
    static public int stepDuration = 100;
    static private int textLength = 0;
    static private int animateCursor = 0;
    static private String narrative;
    static private String currentExtra;
    static private EndAction currentEndAction;
    static public boolean isRunning = false;

    public static List<String> init(HostAdapter view) {
        hostView = view;
        return null;
    }

    /**
     *
     * @param chapter
     * @param role
     * @param narratives
     * @param end
     * @param extra use this to carry extra info
     */
    static void animateText(String chapter, String role, List<String> narratives, EndAction end, final String extra) {


        if (isRunning) {

            // force to end animation if last animation is not complete yet

            hostView.addText(narrative);
            currentEndAction.complete(currentExtra);
        }

        animateCursor = 0;
        narrative = narratives.get(0);
        textLength = narrative.length();
        hostView.addText("["+chapter+"] " + role +":" );
        isRunning = true;
        currentExtra = extra;
        animate(end);
    }

    static private void animate(final EndAction endAction) {
        currentEndAction = endAction;
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        if(animateCursor <= textLength){

                            // delete last item

                            if(animateCursor>1){
                                hostView.delete(hostView.getTextLength() - animateCursor+1);
                            }

                            // append animation gradually

                            hostView.addText(narrative.substring(0, animateCursor));

                            animateCursor++;

                            animate(endAction);

                        }else {
                            endAction.complete(currentExtra);
                            isRunning = false;
                        }
                    }
                },
                stepDuration);
    }

    // =====================================
    // interfaces
    // =====================================

    public interface EndAction {
        void complete(String extra);
    }

    public interface HostAdapter {
        void addText(String text);
        void delete(int offset);
        int getTextLength();
    }

}
