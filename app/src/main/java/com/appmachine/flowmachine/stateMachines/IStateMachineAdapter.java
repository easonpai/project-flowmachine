package com.appmachine.flowmachine.stateMachines;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.appmachine.flowmachine.FlowMachine;
import com.appmachine.flowmachine.FlowMachine.LOG_LEVEL;

/**
 * @author Eason_Pai
 *
 */
public abstract class IStateMachineAdapter {
	protected Object mStateMachine;
	final protected FlowMachine mEngine;
	protected ArrayList<SquirrelAdapter.StateData> mConfigData;
	StringBuilder mLogStateConfig ;
	public IStateMachineAdapter(FlowMachine flowEngine) {
		this.mEngine = flowEngine;
	}
	
	public void init(){
	}
	
	public void start(String initState){
	}
	
	public void fire(String trigger){
	}

	protected FlowMachine getEngine(){
		return this.mEngine;
	}
	
	public Object getMachine(){
		return null;
	}

	/**
	 * this methods gets id of top state instead of sub-state if there are parallels
	 * @return
	 */
	public String getState(){
		return null;
	}
	public String getStateType(){
		return null;
	}
	
	public List<String> getDeepStates(){
		return null;
	}
	public List getDeepStatesByState(String state){
		return null;
	}

	public List<Object> getSubStates(String parent){
		return null;
	}
	public ArrayList<String>  getAllRawStates(){
		return null;
	}

	public List<String> getAcceptableDeepEvents(){
		return null;
	}
	
	public List<String> getAcceptableEvents(){
		return null;
	}
	public List<String> getAcceptableEventsById(String id){
		return null;
	}
	/**
	 * get complete events under state, 
	 * it works on both component and parallel.
	 * Component returns COMPONENT_END type event and parallel returns PARALLEL_END type event
	 * 
	 * @param id
	 * @return
	 */
	public List<String> getAcceptableComponentEndEventsById(String id){
		return null;
	}
	
	protected void logConfig() {
		
	}
	
	/**
	 * go to previous state
	 */
	public void gotoPrevious() {
	}
	
	public void setupConfig(List<Map> flows){
	}
	
	public Object getStateMachineConfig(){
		return null;
	}
	
	/**
	 *  dynamically insert machine configuration
	 * */
	public void configure(String from, String to , String on){
	}
	
	/**
	 *  dynamically insert machine configuration
	 * */
	public void configureStartWithCSR(String from, String scene, String state){
	}
	
	public boolean canFire(String trigger){
		return false;
	}
	public boolean canFire(String state, String trigger){
		return false;
	}
	
	/**
	 * utilities
	 * 
	 * @param msg
	 */
	protected void log(LOG_LEVEL level , String msg) {
		getEngine().log(level, msg);
	}
	/**
	 * utilities
	 * 
	 * @param msg
	 */
	protected void log(String msg) {
		getEngine().log(msg);
	}
}
