package com.appmachine.flowmachine.stateMachines;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.appmachine.flowmachine.Constants;

import org.squirrelframework.foundation.fsm.HistoryType;
import org.squirrelframework.foundation.fsm.ImmutableState;
import org.squirrelframework.foundation.fsm.StateCompositeType;
import org.squirrelframework.foundation.fsm.StateMachine;
import org.squirrelframework.foundation.fsm.StateMachine.TerminateEvent;
import org.squirrelframework.foundation.fsm.StateMachine.TerminateListener;
import org.squirrelframework.foundation.fsm.StateMachine.TransitionDeclinedEvent;
import org.squirrelframework.foundation.fsm.StateMachine.TransitionDeclinedListener;
import org.squirrelframework.foundation.fsm.StateMachine.TransitionEndEvent;
import org.squirrelframework.foundation.fsm.StateMachine.TransitionEndListener;
import org.squirrelframework.foundation.fsm.StateMachine.TransitionExceptionEvent;
import org.squirrelframework.foundation.fsm.StateMachine.TransitionExceptionListener;
import org.squirrelframework.foundation.fsm.StateMachineBuilderFactory;
import org.squirrelframework.foundation.fsm.StateMachineConfiguration;
import org.squirrelframework.foundation.fsm.UntypedStateMachine;
import org.squirrelframework.foundation.fsm.UntypedStateMachineBuilder;
import org.squirrelframework.foundation.fsm.annotation.StateMachineParameters;
import org.squirrelframework.foundation.fsm.impl.AbstractUntypedStateMachine;

import com.appmachine.flowmachine.FlowMachine;
import com.appmachine.flowmachine.FlowMachine.DevPhrase;
import com.appmachine.flowmachine.FlowMachine.LOG_LEVEL;

/**
 * @author Eason_Pai
 */
@SuppressWarnings("rawtypes")
public class SquirrelAdapter extends IStateMachineAdapter {

    private SquirrelStateMachine mStateMachine;
    private UntypedStateMachineBuilder mBuilder;
    private String _stateFrom;

    public SquirrelAdapter(FlowMachine flowEngine) {
        super(flowEngine);
    }

    @Override
    public void init() {
        mConfigData = new ArrayList<StateData>();
        mBuilder = StateMachineBuilderFactory.create(SquirrelStateMachine.class, FlowMachine.class);
    }

    @StateMachineParameters(stateType = String.class, eventType = String.class, contextType = String.class)
    static class SquirrelStateMachine extends AbstractUntypedStateMachine {

        private FlowMachine mEngine;

        public SquirrelStateMachine(FlowMachine engine) {
            mEngine = engine;
        }

        public static String EXECUTE = "execute";

        /**
         * @param from
         * @param to
         * @param event
         * @param context
         */
        public void execute(String from, String to, String event, String context) {
            System.out.println("[State] [execute]  from '" + from + "' to '" + to + "' on '" + event + "' (now: '"
                    + getCurrentState() + "' triggers: " + getCurrentRawState().getAcceptableEvents() + "')");
        }

        protected FlowMachine getEngine() {
            return this.mEngine;
        }
    } // end class

    public void fire(String trigger) {
        mStateMachine.fire(trigger);
    }

    public SquirrelStateMachine getMachine() {
        return mStateMachine;
    }

    @Override
    public String getState() {
        return mStateMachine.getCurrentState().toString();
    }

    @Override
    public String getStateType() {
        Map stateMap = getEngine().getConfigModel().findUnitModelById(getEngine().getCurrentScene(), getState());
        if (stateMap != null) {
            return stateMap.get(Constants.Key.TYPE).toString();
        } else {
            return null;
        }
    }

    /*
     * (non-Javadoc) Return null
     * TODO: not a smart way to convert list<obj> to list<str>
     */
    @Override
    public List<String> getDeepStates() {
        List<String> states = new ArrayList<>();
        for (Object object : mStateMachine.getSubStatesOn(getState())) {
            states.add(object != null ? object.toString() : null);
        }
        return states;
    }

    public List<String> getDeepStatesByState(String state) {
        return getDeepStatesByState(state, false);
    }

    public ArrayList<String> getAllRawStates() {

        ArrayList<String> result = new ArrayList<String>();
        for (Object raw : mStateMachine.getAllRawStates().toArray()) {
            result.add(raw.toString());
        }

        // remove system events
        result.remove(Constants.Runtime.SYSTEM_LIFECYCLE__STATE_INIT);

        return result;
    }

    public List<String> getDeepStatesByState(String state, boolean saveParallel) {
        List<String> states = new ArrayList<>();
        for (ImmutableState subState : mStateMachine.getAllRawStates()) {

            if (subState.hasChildStates()) {
                if (subState.toString().equals(state)) {

                    // find regions
                    for (int i = 0; i < subState.getChildStates().size(); i++) {
                        ImmutableState region = (ImmutableState) subState.getChildStates().get(i);

                        // find 1st states under region
                        states.add(region.getChildStates().get(0).toString());


//                        for (ImmutableState subSubState : mStateMachine.getAllRawStates()) {
//                            if (subSubState.getParentState() != null) {
//                                // skip parallel state
//                                if (!subSubState.getCompositeType().equals(StateCompositeType.PARALLEL)) {
//                                    if (subSubState.getParentState().toString().equals(region)) {
//                                        states.add(subSubState.toString());
//                                    }
//                                }
//                            }
//                        }
                    }
                }
            }
        }
        return states;
    }

    @Override
    public List<Object> getSubStates(String parent) {
        return mStateMachine.getSubStatesOn(parent);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getAcceptableEvents() {
        if (mStateMachine != null) {
            Set<Object> transitions = mStateMachine.getCurrentRawState().getAcceptableEvents();
            return new ArrayList(transitions);
        } else {
            return new ArrayList();
        }
    }

    @Override
    public List<String> getAcceptableDeepEvents() {
        ArrayList<String> results = new ArrayList<String>();
        for (String state : getDeepStates()) {
            results.addAll(getAcceptableEventsById(state));
        }
        return results;
    }

    @Override
    public List<String> getAcceptableEventsById(String state) {
        ArrayList<String> results = new ArrayList<String>();
        for (StateData stateData : mConfigData) {
            if (stateData.stateFrom.equals(state)) {
                results.add(stateData.stateOn);
            }
        }
        return results;
    }

    @Override
    public List<String> getAcceptableComponentEndEventsById(String state) {
        ArrayList<String> results = new ArrayList<String>();
        for (StateData stateData : mConfigData) {
            if (stateData.stateFrom.equals(state)) {
                if (stateData.stateOn.contains(Constants.Runtime.COMPONENT_END)) {
                    results.add(stateData.stateOn);
                } else if (stateData.stateOn.contains(Constants.Runtime.PARALLEL_END)) {
                    results.add(stateData.stateOn);
                }
            }
        }
        return results;
    }

    @Override
    public void start(String initState) {
        mStateMachine = (SquirrelStateMachine) mBuilder.newStateMachine(initState, StateMachineConfiguration.create()
                .enableDebugMode(false), getEngine());

        registerEvents();
    }

    /**
     * state control -- go to previous state
     */
    @Override
    public void gotoPrevious() {

        try {
            fire(Constants.Runtime.GOTO_PREVIOUS_EVENT + "_"
                    + getEngine().getPathManager().getPreviousStateInHistory(getState()));
        } catch (Exception e) {
            getEngine().log(LOG_LEVEL.DEBUG, "gotoPrevious error = " + e.toString());
        }

    }

    @Override
    public void setupConfig(List<Map> flows) {
        // init
        mLogStateConfig = new StringBuilder();
        try {
            prepareStateMachineConfig(flows, null, 0);
        } catch (UnsupportedOperationException e) {
            log("UnsupportedOperationException , e = " + e);
        }
    }

    /**
     * Algorithm to create state machine config from Editor config
     *
     * @param flows
     * @return
     */
    @SuppressWarnings("unchecked")
    private void prepareStateMachineConfig(List<Map> flows, Map hierarchyParent, int level) {

        // support hierarchy
        if (hierarchyParent != null) {

//            log("prepareStateMachineConfig:: " + hierarchyParent.get(Constants.Key.ID).toString());

            // important!!!
            // 1st sequence for defineSequentialStatesOn matters!
            // move 1st item to first

            String groupEntry = hierarchyParent.get(Constants.Unit.GROUP_ENTRY).toString();

            // build hierarchy data in advance

            ArrayList<String> hierarchicalUnitIDs = new ArrayList<String>();
            for (final Map component : flows) {

                // ignore regions, region is connected from parallel, not hierarchy
                // TODO: currently group type of group and region shared the same type
                if (!component.get(Constants.Key.TYPE).equals(Constants.Unit.GROUP)) {
                    // sequence matters, so skip 1st item and add it later

                    if (!groupEntry.equals(component.get(Constants.Key.ID))) {
                        hierarchicalUnitIDs.add(component.get(Constants.Key.ID).toString());
                    }
                }

            }
            // sequence matters, so insert 1st item here
            hierarchicalUnitIDs.add(0, groupEntry);

            // create group set
            mBuilder.defineSequentialStatesOn(hierarchyParent.get(Constants.Key.ID).toString(),
                    HistoryType.DEEP, hierarchicalUnitIDs.toArray());

//            log("prepareStateMachineConfig:: hierarchicalUnitIDs.toArray() = " + hierarchicalUnitIDs.toString());
        }

        // traverse flows

        for (final Map component : flows) {

            // beautify output log

            StringBuilder levelSymbol = new StringBuilder();
            for (int i = 0; i < level; i++) {
                levelSymbol.append("  ├─ ");
            }

            log("\t\t├─ " + ((hierarchyParent == null) ? "" : levelSymbol.toString()) + "state type =>"
                    + component.get(Constants.Key.TYPE) + " (" + component.get(Constants.Key.ID) + ")");

            // ----------------------------------------
            // build 'from'
            // ----------------------------------------
            _stateFrom = component.get(Constants.Key.ID).toString();

            // ----------------------------------------
            // build internal jump
            // ----------------------------------------
//			buildInternalJump(component);

            // ----------------------------------------
            // inflate lost properties
            // ----------------------------------------
            // TODO: hardcoded to restore empty goto (flowbaker)
            if (!component.containsKey(Constants.Key.GOTO)) {

                component.put(Constants.Key.GOTO, new ArrayList<>());

                // TODO: hardcoded to restore parallel properties
                // TODO: should have a more standard way to check or build from template (flowbaker)
                if (component.get(Constants.Key.TYPE).equals(Constants.Unit.PARALLEL)) {
                    component.put(Constants.Unit.PARALLEL_FINALSTATES, new ArrayList<>());
                }
                // goto_scene is a special type that dont't have goto property
//				log("empty goto >> "+ component.toString() );
            }

            // ----------------------------------------
            // build 'to's out of GOTO property
            // ----------------------------------------
            for (Map gotoItem : ((List<Map>) component.get(Constants.Key.GOTO))) {

                // find output
                Map<String, ?> outputFound = getEngine().getConfigModelManager().findOutputModelByGotoModel(component,
                        gotoItem);

                // error tolerant

                if (outputFound == null) {
                    log("[alert] prepareStateMachineConfig:: output== null");
                }

                // add event of transition by type

                if (outputFound != null) {

                    // add transition event according to type

                    switch (component.get(Constants.Key.TYPE).toString()) {

                        // ----------------------------------------
                        // find hierarchy
                        // ----------------------------------------
                        case Constants.Unit.GROUP:
                            // link group
                            // TODO: performance concern, string manipulation is not smart
                            configure(_stateFrom, gotoItem.get(Constants.Key.ID).toString(),
                                    Constants.Runtime.COMPONENT_END + _stateFrom);
                            // parse recursively
                            prepareStateMachineConfig((List<Map>) component.get(Constants.Key.UNITS), component, level + 1);
                            break;

                        // ----------------------------------------
                        // find parallel
                        // ----------------------------------------
                        case Constants.Unit.PARALLEL:

                            // link parallel with specific parallel finish event
                            // TODO: performance concern, string manipulation is not smart
                            configure(_stateFrom, gotoItem.get(Constants.Key.ID).toString(),
                                    Constants.Runtime.PARALLEL_END + _stateFrom);

                            break;

//					case Constants.Unit.CSR:
//						log("Unit.CSR >> gotoItem=  " + gotoItem.get(Constants.Key.ID).toString()
//								+ ", output = " + outputFound.get(Constants.Key.VALUE).toString());
                        default:
                            // -- defaultOutputType
                            if (outputFound.get(Constants.Key.TYPE).toString()
                                    .equals(Constants.Key.OUTPUT__TYPE_DEFAULT_TYPE)) {
                                // TODO: performance concern, string manipulation is not a too smart method
                                configure(_stateFrom, gotoItem.get(Constants.Key.ID).toString(),
                                        Constants.Runtime.COMPONENT_END + _stateFrom);
                            } // end defaultOutputType

                            // -- conditionalOutputs
                            if (outputFound.get(Constants.Key.TYPE).toString()
                                    .equals(Constants.Key.OUTPUT__TYPE_CONDITIONAL)) {

                                // TODO: performance concern, string manipulation is not a too smart method
                                configure(_stateFrom, gotoItem.get(Constants.Key.ID).toString(),
                                        Constants.Runtime.COMPONENT_END + _stateFrom + outputFound.get(Constants.Key.VALUE).toString());
                            }// end conditionalOutputs

                            // -- conditionalDefaultOutput
                            if (outputFound.get(Constants.Key.TYPE).toString()
                                    .equals(Constants.Key.OUTPUT__TYPE_CONDITIONAL_DEFAULT)) {

                                // TODO: asr's "no_one_speak" id is a special
                                // case, need to be revised

                                if (outputFound.get(Constants.Key.VALUE).toString()
                                        .equals(Constants.Unit.OUTPUT_TYPE_NO_ONE_SPEAK)) {

                                    configure(_stateFrom, gotoItem.get(Constants.Key.ID).toString(),
                                            Constants.Unit.OUTPUT_TYPE_TIME_OUT);

                                } else {

                                    configure(_stateFrom, gotoItem.get(Constants.Key.ID).toString(),
                                            outputFound.get(Constants.Key.VALUE).toString());
                                }
                            } // end conditionalDefaultOutput

                            break;
                    }// end switch
                } // output not null
            } // end of goto

            // ----------------------------------------
            // build TOs out of Container Type
            // ----------------------------------------
            switch (component.get(Constants.Key.TYPE).toString()) {

                // ----------------------------------------
                // find hierarchy
                // ----------------------------------------

                // TODO: don't know why Group is not working this way
                case Constants.Unit.GROUP:

                    // parse deeply
                    prepareStateMachineConfig((List<Map>) component.get(Constants.Key.UNITS), component, level + 1);

                    break;

                // ----------------------------------------
                // find parallel
                // ----------------------------------------
                case Constants.Unit.PARALLEL:
                    //
                    List<String> regions = (List<String>) component.get(Constants.Unit.PARALLEL_REGIONS);

                    // build parallel data
                    mBuilder.defineParallelStatesOn(_stateFrom, regions.toArray());


                    break;
            } // end of container type

        }// end traverse flows
    }

    private void buildInternalJump(Map component) {
        // Special case to build internal system transition
        // dynamically adding additional jump transitions
        if (component.get(Constants.Key.TYPE).equals(Constants.Unit.CSR)) {
            ArrayList<String> allPrevious = getEngine().getConfigModel().findAllPreviousStatesByUnitId(_stateFrom);
            // remove duplicate connections, we don't need it here
            Set<String> nonDuoPrevious = new LinkedHashSet<String>(allPrevious);
            for (String previousId : nonDuoPrevious) {
                // special rule to compose unique previous event
                configure(_stateFrom, previousId, Constants.Runtime.GOTO_PREVIOUS_EVENT + "_" + previousId);
            }
        }
    }

    private void executeComponent(String id) {
        getEngine().getComponentManager().componentBegin(id);
    }

    private void registerEvents() {
        // testing with events
        // getMachine().addListener(TransitionEvent.class, new
        // StateMachineListener<UntypedStateMachine, Object, Object, Object>() {
        // @Override
        // public void stateMachineEvent(StateMachineEvent<UntypedStateMachine,
        // Object, Object, Object> event) {
        // System.out.println("[State] [stateMachineEvent] event.toString = " +
        // event.toString() );
        // }
        // }, "stateMachineEvent");

//        getMachine().addTransitionEndListener(new TransitionEndListener<UntypedStateMachine, Object, Object, Object>() {
//            @Override
//            public void transitionEnd(TransitionEndEvent<UntypedStateMachine, Object, Object, Object> event) {
//
//                System.out.println("[IRP] [Engine] [TransitionEnd] getTargetState= "
//                        + event.getTargetState() + ", getCause="
//                        + event.getCause() + ", getSourceState =" +
//                        event.getSourceState() + ", getContext=" + event.getContext()
//                        + ", getCurrentState = " + getState().toString());
//
//            }
//        });
        getMachine().addTransitionCompleteListener(new StateMachine.TransitionCompleteListener<UntypedStateMachine, Object, Object, Object>() {
            @Override
            public void transitionComplete(StateMachine.TransitionCompleteEvent<UntypedStateMachine, Object, Object, Object> event) {
//                System.out.println("[State] [TransitionComplete] getTargetState= "  + event.getTargetState() + ", getCause="
//                        + event.getCause() + ", getSourceState =" + event.getSourceState() + ", getContext=" + event.getContext() + ", getState() = " + getState());
                    executeComponent(getState());
            }
        });
        getMachine().addTransitionExceptionListener(
                new TransitionExceptionListener<UntypedStateMachine, Object, Object, Object>() {
                    @Override
                    public void transitionException(
                            TransitionExceptionEvent<UntypedStateMachine, Object, Object, Object> event) {
                        System.out.println("[IRP] [Engine] [transitionException] getTargetState= "
                                + event.getTargetState() + ", getCause=" + event.getCause() + ", getSourceState ="
                                + event.getSourceState() + ", getContext=" + event.getContext());
                    }
                });


        getMachine().addTransitionDeclinedListener(
                new TransitionDeclinedListener<UntypedStateMachine, Object, Object, Object>() {
                    @Override
                    public void transitionDeclined(
                            TransitionDeclinedEvent<UntypedStateMachine, Object, Object, Object> event) {
                        System.out.println("[IRP] [Engine] [transitionDeclined] getCause=" + event.getCause()
                                + ", getSourceState =" + event.getSourceState() + ", getContext=" + event.getContext());

                    }
                });


        getMachine().addTerminateListener(new TerminateListener<UntypedStateMachine, Object, Object, Object>() {
            @Override
            public void terminated(TerminateEvent<UntypedStateMachine, Object, Object, Object> event) {
                System.out.println("[State] [terminated] event =" + event.toString());
                System.out.println("[State] [terminated]  getCurrentState = " + getMachine().getCurrentState());
            }
        });
    }

    /**
     * This is the main method to build states and transitions
     */
    @Override
    public void configure(String from, String to, String on) {
        mBuilder.externalTransition().from(from).to(to).on(on);
        // TODO: index and scene id is not added
        mConfigData.add(new StateData(0, "", from, to, on));
        if (getEngine().devPhrase == DevPhrase.DEBUG) {
            log(LOG_LEVEL.DEBUG, "\t\t\t   (from = \"" + from + "\", to \"" + to + "\" on \"" + on + "\")");
        }
    }

    @Override
    @SuppressWarnings("unused")
    public void logConfig() {
        if (mConfigData != null) {
            for (StateData stateData : mConfigData) {
                mLogStateConfig = new StringBuilder();

                mLogStateConfig.append("");
                mLogStateConfig.append("============================================================\n");
                mLogStateConfig.append("▼ State Config ▼ (user id = \"" + getEngine().getUserId() + "\", app id = \""
                        + getEngine().getProductId() + ")\n");
                for (StateData element : mConfigData) {
                    mLogStateConfig.append("\t ├─ (" + element.index + ") scene = \"" + element.sceneId
                            + "\", state from = \"" + element.stateFrom + "\", to = " + element.stateTo + "("
                            + element.stateOn + ")\n");
                }
                getEngine().log(LOG_LEVEL.DEBUG, mLogStateConfig.toString());
            }
        }
    }

    @Override
    public Object getStateMachineConfig() {
        return mBuilder.toString();
    }

    @Override
    public boolean canFire(String trigger) {
        if (!getStateType().equals(Constants.Unit.PARALLEL)) {
            return getAcceptableEvents().contains(trigger);
        } else {
            return getAcceptableDeepEvents().contains(trigger);
        }
    }

    @Override
    public boolean canFire(String state, String trigger) {
        return getAcceptableEventsById(state).contains(trigger);
    }

    public class StateData {
        public int index;
        public String sceneId;
        public String stateFrom;
        public String stateTo;
        public String stateOn;

        public StateData(int index, String sceneId, String stateFrom, String stateTo, String stateOn) {
            this.index = index;
            this.stateFrom = stateFrom;
            this.stateTo = stateTo;
            this.stateOn = stateOn;
        }
    }

}
