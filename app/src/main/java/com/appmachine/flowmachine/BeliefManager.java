package com.appmachine.flowmachine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;

//import com.appmachine.flowmachine.ComponentManager.TableBeliefDataObject;
//import com.appmachine.flowmachine.FlowMachine.LOG_LEVEL;
//import com.asus.Info.SemanticInfo;
//import com.asus.ctc.ams.smbdm.TimeDS_CSR;
//import com.appmachine.flowmachine.belief.FunctionOnTable;
//import com.appmachine.flowmachine.belief.VirtualTable;
//import com.asus.ctc.ams.smbdm.util.data.ExcelCell;
//import com.asus.ctc.ams.smbdm.util.data.SerializableSheets;

/**
 * @editby Wen-Ting_Li
 * @date 2015/08/25 support add belief ==> ${add [tablename][value]} support
 *       delete belief ==> ${del [tablename][value]}
 */
public class BeliefManager {
//	private HashMap<String, VirtualTable> mVirtualTableMaps = new HashMap<String, VirtualTable>();
//	public VirtualTable mVirtualTable;

	private HashMap<String, Object> mBeliefMap;
	private FlowMachine mFlowEngine;
	private Random mRandom;

	public BeliefManager(FlowMachine flowEngine) {
		mFlowEngine = flowEngine;
		setBeliefMap(new HashMap<String, Object>());
		mRandomMap = new HashMap<String, RandomDO>();

		initUtils();
	}

	private void initUtils() {
		mRandom = new Random();
	}

	public void resetBelief() {
		if (getBeliefMap() != null) {
			getBeliefMap().clear();
		}
		setBeliefMap(new HashMap<String, Object>());
//		mVirtualTableMaps.clear();
	}

	private class RandomDO {
		int _bound = 0;
		int index = 0;
		private ArrayList<Integer> list;

		public RandomDO(int bound) {
			_bound = bound;
			genSeeds();
		}

		private void genSeeds() {
			list = new ArrayList<Integer>();
			for (int i = 0; i < _bound; i++) {
				list.add(new Integer(i));
			}
			Collections.shuffle(list);
		}

		public int next() {
			int result = list.get(index);
			index++;
			if (index >= _bound) {
				index = 0;
				genSeeds();
			}
			return result;
		}
	}

	private HashMap<String, RandomDO> mRandomMap = new HashMap<String, RandomDO>();

	public int getRandomInt(String key, int bound, boolean repeat) {
		int result = 0;
		if (repeat) {
			result = mRandom.nextInt(bound);
			// clean map
			if (mRandomMap.containsKey(key)) {
				mRandomMap.remove(key);
			}
		} else {
			if (mRandomMap.containsKey(key)) {
				result = mRandomMap.get(key).next();
			} else {
				RandomDO randomDO = new RandomDO(bound);
				mRandomMap.put(key, randomDO);
				result = randomDO.next();
			}
		}
		return result;
	}

	// 2015/02/04 Modified by Michael
//	public void parseTableBeliefByFile(String id, String fileName, String sheetName) {
//		byte[] data = null;
//		Date processtimer = new Date();
//		mFlowEngine.log(LOG_LEVEL.ENGINE, " Add table belief using Excel file " + fileName);
//		if (fileName.endsWith("xlsx")) {
//			// 2015/02/04 Michael: try to load pre processed Excel file if it
//			// exists
//			String file_name_preprocessed_data = fileName + "so";
//			data = mFlowEngine.getResourceManager().downloadAppFileToByteArray(mFlowEngine.getProductId(),
//					file_name_preprocessed_data);
//			if (data != null) {
//				mFlowEngine.log(LOG_LEVEL.ENGINE, " fetch preprocessed data from " + file_name_preprocessed_data);
//				mFlowEngine.log(LOG_LEVEL.ENGINE, "\t  > File Length = " + data.length + " bytes , Time Used = "
//						+ ((double) new Date().getTime() - processtimer.getTime()) / 1000 + " sec. ");
//				ByteArrayInputStream bais = new ByteArrayInputStream(data);
//				try {
//					ObjectInputStream ois = new ObjectInputStream(bais);
//					Object loaded_obj = ois.readObject();
//					SerializableSheets serialized_sheets = (SerializableSheets) loaded_obj;
//					if (serialized_sheets != null && serialized_sheets.getSheetCount() > 0) {
//						mFlowEngine.log(LOG_LEVEL.ENGINE, "\t  > " + serialized_sheets.getSheetCount()
//								+ " sheets found:");
//						Vector<String> sheet_name_list = serialized_sheets.getSheetNameList();
//						for (String str : sheet_name_list) {
//							mFlowEngine.log(LOG_LEVEL.ENGINE, "\t\t  > Sheet '" + str + "'");
//						}
//						mFlowEngine.log(LOG_LEVEL.ENGINE, "\t  > fetch cells for Sheet = " + sheetName);
//						ArrayList<ExcelCell> sheet_cells = serialized_sheets.getSheetCells(sheetName);
//						if (sheet_cells != null) {
//							mVirtualTable = new VirtualTable();
//							processtimer = new Date();
//							//
//							mVirtualTable.loadVirtualTableFromExcelCells(sheet_cells);
//							//
//							//
//							mFlowEngine.log(LOG_LEVEL.ENGINE, "\t  > Preprocessed Excel sheet loaded. Time Used = "
//									+ ((double) new Date().getTime() - processtimer.getTime()) / 1000 + " sec. ");
//							//
//							getVirtualTableMaps().put(id, mVirtualTable);
//							//
//							return;
//						} else {
//							if (sheet_name_list.contains(sheetName))
//								mFlowEngine.log(LOG_LEVEL.ENGINE, "\t  > Sheets " + sheetName + " contains no cells.");
//							else {
//								mFlowEngine.log(LOG_LEVEL.ENGINE,
//										"\t  > ***************************************************");
//								mFlowEngine.log(LOG_LEVEL.ENGINE, "\t  > *** ERROR: Sheet " + sheetName
//										+ " does not exist.");
//								mFlowEngine.log(LOG_LEVEL.ENGINE,
//										"\t  > ***************************************************");
//								throw new Exception("Sheet " + sheetName + " does not exist.");
//							}
//						}
//					} else {
//						mFlowEngine.log(LOG_LEVEL.ENGINE, "\t  > Preprocessed Excel file contains no sheets.");
//					}
//				} catch (Exception ex) {
//					ex.printStackTrace();
//					data = null;
//				}
//			}
//		}
//		if (data == null) {
//			data = mFlowEngine.getResourceManager().downloadAppFileToByteArray(mFlowEngine.getProductId(), fileName);
//			mFlowEngine.log(LOG_LEVEL.ENGINE, "\t  > File Length = " + data.length + " bytes , Time Used = "
//					+ ((double) new Date().getTime() - processtimer.getTime()) / 1000 + " sec. ");
//
//			processtimer = new Date();
//			try {
//				mVirtualTable.loadExcelFile(data, sheetName);
//			} catch (Exception ex) {
//				ex.printStackTrace();
//				mFlowEngine.log(LOG_LEVEL.ENGINE, "\t  > *****************************************************");
//				mFlowEngine.log(LOG_LEVEL.ENGINE, "\t  > *** ERROR: Sheet '" + sheetName + "' does not exist.");
//				mFlowEngine.log(LOG_LEVEL.ENGINE, "\t  > *** ERROR: Specified sheet name maybe incorrect.");
//				mFlowEngine.log(LOG_LEVEL.ENGINE, "\t  > *****************************************************");
//				throw ex;
//			}
//			mFlowEngine.log(LOG_LEVEL.ENGINE, "\t  > Excel Parsed. Time Used = "
//					+ ((double) new Date().getTime() - processtimer.getTime()) / 1000 + " sec. ");
//
//			getVirtualTableMaps().put(id, mVirtualTable);
//		}
//	}

//	public String getTableBeliefValue(String id, String syntax) {
//		if (syntax.startsWith("del")) {
//			String tableName = "";
//			String removeData = "";
//			tableName = syntax.substring(syntax.indexOf("[") + 1, syntax.indexOf("]"));
//			syntax = syntax.substring(syntax.indexOf("]") + 1);
//			removeData = syntax.substring(syntax.indexOf("[") + 1, syntax.indexOf("]"));
//
//			// System.out.println("tableName:"+tableName+", removeData: "+removeData);
//			return removeDataFromTB(tableName, removeData);
//		} else if (syntax.startsWith("add")) {
//			String tableName = "";
//			String addData = "";
//			tableName = syntax.substring(syntax.indexOf("[") + 1, syntax.indexOf("]"));
//			syntax = syntax.substring(syntax.indexOf("]") + 1);
//			addData = syntax.substring(syntax.indexOf("[") + 1, syntax.indexOf("]"));
//
//			// System.out.println("tableName:"+tableName+", removeData: "+removeData);
//			return addDataFromTB(tableName, addData);
//
//		} else {
//			if(getVirtualTableMaps().get(id)==null){
//				return Constants.Notification.EMPTY_TABLE;
//			}else{
//				FunctionOnTable getResult = new FunctionOnTable(getVirtualTableMaps().get(id).getContainer());
//				return getResult.getStringValueFromTable(syntax);
//			}
//		}
//	}

//	private String addDataFromTB(String tableName, String addData) {
//		try {
//			if (!this.mVirtualTableMaps.get(tableName).getContainer().get("value").contains(addData)) {
//				int index = this.mVirtualTableMaps.get(tableName).getContainer().get("value").size();
//				this.mVirtualTableMaps.get(tableName).getContainer().get("id").add(index + "");
//				this.mVirtualTableMaps.get(tableName).getContainer().get("value").add(addData);
//				this.mVirtualTableMaps.get(tableName).getContainer().get("group").add("array");
//				return "complete addition";
//			} else {
//				return "";
//			}
//		} catch (Exception e) {
//			System.out.println("[BM]--" + e);
//			return "";
//		}
//	}

//	public String removeDataFromTB(String tableName, String removeData) {
//		try {
//			int index = this.mVirtualTableMaps.get(tableName).getContainer().get("value").indexOf(removeData);
//			this.mVirtualTableMaps.get(tableName).getContainer().get("id").remove(index);
//			this.mVirtualTableMaps.get(tableName).getContainer().get("value").remove(index);
//			this.mVirtualTableMaps.get(tableName).getContainer().get("group").remove(index);
//			return "complete deletion";
//		} catch (Exception e) {
//			System.out.println("[BM]--" + e);
//			return "";
//		}
//	}

//	public void createTableBeliefByArray(String name, ArrayList<String> list) {
//		if (getVirtualTableMaps().containsKey(name)) {
//			int maxId = 0;
//			for (String numberId : getVirtualTableMaps().get(name).getContainer().get("id")) {
//				if (maxId < Integer.parseInt(numberId)) {
//					maxId = Integer.parseInt(numberId);
//				}
//			}
//			int idIndex = maxId + 1;
//			for (String value : list) {
//				addDataToTable(name, idIndex, "group", value);
//			}
//		} else {
//			getVirtualTableMaps().put(name, new VirtualTable().createAnArrayTable(name, "group", "value", list));
//		}
//	}
//
//	public VirtualTable createTable(String tableName) {
//		ArrayList<String> newTitles = new ArrayList<String>();
//		newTitles.add(Constants.Key.ID);
//
//		VirtualTable result = new VirtualTable(tableName);
//		result.getContainer().put(Constants.Key.ID, new ArrayList<String>());
//		result.setTitles(newTitles);
//
//		result.getContainer().get(Constants.Key.ID).add("1");
//		getVirtualTableMaps().put(tableName, result);
//		return result;
//	}
//
//	public VirtualTable createTableIfObsent(String tableName) {
//		if (!getVirtualTableMaps().containsKey(tableName)) {
//			return createTable(tableName);
//		} else {
//			return getVirtualTableMaps().get(tableName);
//		}
//	}

	public String getTableNameOutOfSyntax(String syntax) {
		String tbName;
		int findSymbolL = syntax.indexOf("[");
		int findSymbolR = syntax.indexOf("]");
		if (findSymbolL != -1 && findSymbolR != -1) {
			tbName = syntax.substring(0, findSymbolL);
			if (tbName.length() > 0) {
				return tbName;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public class SyntaxQueryData {
		public String id;
		public String syntax;
		public String corpse;

		public SyntaxQueryData(String id, String syntax, String corpse) {
			this.id = id;
			this.syntax = syntax;
			this.corpse = corpse;
		}
	}

	// TODO: currently our process is pretty rough that check only TB for first
	// step, then check B letter
	// so a syntax like this
	// ${CurrentStep}?B?O${recipeflow2["id=1"][${CurrentStep}]["value"]} will
	// fail
	public SyntaxQueryData getTableBeliefSyntax(String source) {

		if (source.length() == 0)
			return null;

		// is table tag valid
		boolean tableTagPass = false;
		boolean isQueryLength = false;
		int tagLeftCount = StringUtils.countMatches(source, "[");
		int tagRightCount = StringUtils.countMatches(source, "]");
		// short syntax
		if (tagLeftCount == 2 && tagRightCount == 2) {
			tableTagPass = true;
			// full syntax
		} else if (tagLeftCount == 3 && tagRightCount == 3) {
			tableTagPass = true;
			// check length syntax
		} else if (tagLeftCount == 1 && tagRightCount == 1) {
			if (source.indexOf("length") != -1) {
				tableTagPass = true;
				isQueryLength = true;
			}
		}

		if (!tableTagPass)
			return null;

		// is belief braces valid
		int findbraceL = source.indexOf("[");
		int findbraceR = StringUtils.lastIndexOf(source, "]");
		if (findbraceL != -1 && findbraceR != -1) {
		} else {
			return null;
		}

		int findCurlybraceL = source.indexOf("{");
		int findCurlybraceR = StringUtils.lastIndexOf(source, "}");
		// if is not query length behavior, then we need to check syntax
		// validation
		if (!isQueryLength) {
			// right brace must follow by curly brace
			if (!source.substring(findbraceR + 1, findCurlybraceR + 1).equals("}")) {
				return null;
			}
		}

		// curly brace must exists before at left brace
		if (findCurlybraceL >= findbraceL) {
			return null;
		}

		return new SyntaxQueryData(source.substring(findCurlybraceL + 1, findbraceL), source.substring(
				findCurlybraceL + 1, findCurlybraceR), source.replace(
				"${" + source.substring(findCurlybraceL + 1, findCurlybraceR) + "}", "#"));
	}

	/**
	 * @return the mBeliefMap
	 */
	public HashMap<String, Object> getBeliefMap() {
		return mBeliefMap;
	}

	/**
	 * @param mBeliefMap
	 *            the mBeliefMap to set
	 */
	public void setBeliefMap(HashMap<String, Object> mBeliefMap) {
		this.mBeliefMap = mBeliefMap;
	}
//
//	/**
//	 * @return the mVirtualTableMaps
//	 */
//	public HashMap<String, VirtualTable> getVirtualTableMaps() {
//		return mVirtualTableMaps;
//	}
//
//	/**
//	 * @param mVirtualTableMaps
//	 *            the mVirtualTableMaps to set
//	 */
//	public void setVirtualTableMaps(HashMap<String, VirtualTable> virtualTableMaps) {
//		setVirtualTableMaps(virtualTableMaps);
//	}

	public static void main(String[] args) {
//		BeliefManager bm = new BeliefManager(null);
//		bm.getTableBeliefValue("123", "del [tableName=abcd][removeData=1234]");
//
//		VirtualTable virtualTable = bm.createTable("test");
//		System.out.println(" virtualTable , id=  " + virtualTable.getContainer().get("id"));
//		System.out.println(" virtualTable , value =  " + virtualTable.getContainer().get("value"));
//
//		bm.addDataToTable("test", 1, "group", "time");
//		bm.addDataToTable("test", 1, "year", "2015");
//		bm.addDataToTable("test", 1, "value", "2015.10.1 10:00");
//		System.out.println("time value = " + bm.getTableBeliefValue("test", "[group=time][value]"));
//		System.out.println("time year = " + bm.getTableBeliefValue("test", "[group=time][year]"));
//
//		int nextId = bm.getNextAvailableTableIndex("test");
//		System.out.println("getAvailableTableIndex = " + nextId);
//		bm.addDataToTable("test", nextId, "group", "people");
//		bm.addDataToTable("test", nextId, "name", "eason");
//		System.out.println("people name = " + bm.getTableBeliefValue("test", "[group=people][name]"));
//
//		// check repeat
//		bm.addDataToTable("test", nextId, "name", "eason");
//		System.out.println("people name = " + bm.getTableBeliefValue("test", "[group=people][name]"));
//
//		// TODO: failed
//		bm.addDataToTable("test", nextId, "value", "eason pai");
//		System.out.println(" virtualTable , value =  " + virtualTable.getContainer().get("value"));
//		System.out.println("people value = " + bm.getTableBeliefValue("test", "[group=people][value]"));
//
//		String tbName = "newCSROutput";
//		bm.createTable(tbName);
//		bm.addDataToTable(tbName, 101, "group", "semantic");
//		bm.addDataToTable(tbName, 101, "value", "我");
//		bm.addDataToTable(tbName, 101, "info_length", "3");
//		bm.addDataToTable(tbName, 101, "info1", "DialogueTerm_i");
//		bm.addDataToTable(tbName, 101, "info2", "pronoun");
//		bm.addDataToTable(tbName, 101, "info3", "i");
//
//		bm.addDataToTable(tbName, 102, "group", "semantic");
//		bm.addDataToTable(tbName, 102, "value", "想");
//		bm.addDataToTable(tbName, 102, "info_length", "3");
//		bm.addDataToTable(tbName, 102, "info1", "going_to");
//		bm.addDataToTable(tbName, 102, "info2", "want");
//		bm.addDataToTable(tbName, 102, "info3", "plan_to");
//
//		bm.addDataToTable(tbName, 103, "group", "semantic");
//		bm.addDataToTable(tbName, 103, "value", "看");
//		bm.addDataToTable(tbName, 103, "info_length", "2");
//		bm.addDataToTable(tbName, 103, "info1", "watch");
//		bm.addDataToTable(tbName, 103, "info2", "see");
//
//		bm.addDataToTable(tbName, 104, "group", "semantic");
//		bm.addDataToTable(tbName, 104, "value", "電影");
//		bm.addDataToTable(tbName, 104, "info_length", "1");
//		bm.addDataToTable(tbName, 104, "info1", "movie");
//
//		bm.addDataToTable(tbName, 105, "value", "想");
//		bm.addDataToTable(tbName, 105, "value", "電影");
//
//		System.out.println(" getVirtualTableMaps = " + bm.getVirtualTableMaps());
//		System.out.println(" getVirtualTableMaps = "
//				+ bm.getVirtualTableMaps().get("newCSROutput").getContainer().entrySet());
//
//		System.out.println(bm.getTableBeliefValue("newCSROutput", "${newCSROutput[\"value=我\"][1][\"info_length\"]}"));
//		System.out.println(bm.getTableBeliefValue("newCSROutput", "${newCSROutput[\"value=我\"][1][\"info1\"]}"));
//		System.out.println(bm.getTableBeliefValue("newCSROutput", "${newCSROutput[\"value=我\"][1][\"info2\"]}"));
//		System.out.println(bm.getTableBeliefValue("newCSROutput", "${newCSROutput[\"value=我\"][1][\"info3\"]}"));
//		System.out.println(bm.getTableBeliefValue("newCSROutput", "${newCSROutput[\"value=想\"][1][\"info_length\"]}"));
//		System.out.println(bm.getTableBeliefValue("newCSROutput", "${newCSROutput[\"value=想\"][1][\"info1\"]}"));
//		System.out.println(bm.getTableBeliefValue("newCSROutput", "${newCSROutput[\"value=想\"][1][\"info2\"]}"));
//		System.out.println(bm.getTableBeliefValue("newCSROutput", "${newCSROutput[\"value=想\"][1][\"info3\"]}"));
//		System.out.println(bm.getTableBeliefValue("newCSROutput", "${newCSROutput[\"value=看\"][1][\"info_length\"]}"));
//		System.out.println(bm.getTableBeliefValue("newCSROutput", "${newCSROutput[\"value=看\"][1][\"info1\"]}"));
//		System.out.println(bm.getTableBeliefValue("newCSROutput", "${newCSROutput[\"value=看\"][1][\"info2\"]}"));
//		System.out.println(bm.getTableBeliefValue("newCSROutput", "${newCSROutput[\"value=電影\"][1][\"info_length\"]}"));
//		System.out.println(bm.getTableBeliefValue("newCSROutput", "${newCSROutput[\"value=電影\"][1][\"info1\"]}"));

	}

//	/**
//	 * @author Wen-Ting_Li
//	 * @param tbName
//	 * @param titleName
//	 * @param valueOf
//	 *            this class provide a feature which developer can append a new
//	 *            row data. in specific situation
//	 */
//	public void addDataToTable(String tbName, int indexOfId, String titleName, String valueOf) {
//		// TODO Auto-generated method stub
//		// 1. examine the table name
//		// 2. examine the title name
//		// 2.1. create a title name and fill the empty
//		// 3. decide the index to append
//		if (getVirtualTableMaps().get(tbName) != null) {
//			int indexToAppend = 0;
//			FunctionOnTable fn = new FunctionOnTable(getVirtualTableMaps().get(tbName).getContainer());
//			if (!getVirtualTableMaps().get(tbName).getContainer().get("id").contains(indexOfId + "")) {
//				getVirtualTableMaps().get(tbName).getContainer().get("id").add(indexOfId + "");
//			}
//			indexToAppend = getVirtualTableMaps().get(tbName).getContainer().get("id").indexOf(indexOfId + "");
//			getVirtualTableMaps().get(tbName).getTitles().add(titleName);
//			if (!fn.isTitleExist(titleName)) {
//				ArrayList<String> newarray = new ArrayList<String>();
//				for (int i = 0; i < indexToAppend; i++)
//					newarray.add(null);
//				newarray.add(valueOf);
//				getVirtualTableMaps().get(tbName).getContainer().put(titleName, newarray);
//			} else {
//				// check empty space
//				while ((indexToAppend - getVirtualTableMaps().get(tbName).getContainer().get(titleName).size()) > 1) {
//					getVirtualTableMaps().get(tbName).getContainer().get(titleName).add(null);
//				}
//				if (indexToAppend >= getVirtualTableMaps().get(tbName).getContainer().get(titleName).size()) {
//					getVirtualTableMaps().get(tbName).getContainer().get(titleName).add(valueOf);
//				} else {
//
//					getVirtualTableMaps().get(tbName).getContainer().get(titleName).set(indexToAppend, valueOf);
//				}
//			}
//			// fill the rest of column
//			Iterator<Map.Entry<String, ArrayList<String>>> iterator = getVirtualTableMaps().get(tbName).getContainer()
//					.entrySet().iterator();
//			while (iterator.hasNext()) {
//				Map.Entry<String, ArrayList<String>> entry = iterator.next();
//				ArrayList<String> value = entry.getValue();
//				String key = entry.getKey();
//				for (int i = value.size(); i < indexToAppend + 1; i++) {
//					getVirtualTableMaps().get(tbName).getContainer().get(key).add(null);
//				}
//
//			}
//
//		} else {
//
//		}
//	}

//	public int getNextAvailableTableIndex(String tableName) {
//
//		if (!getVirtualTableMaps().containsKey(tableName)) {
//			return -1;
//		}
//
//		int maxId = 0;
//		for (String numberId : getVirtualTableMaps().get(tableName).getContainer().get("id")) {
//			if (maxId < Integer.parseInt(numberId)) {
//				maxId = Integer.parseInt(numberId);
//			}
//		}
//		return maxId + 1;
//	}
//
//	public ArrayList<String> processCsrTag(ArrayList<HashMap<String, String>> csrTags) {
//		// arrayList
//		ArrayList<String> flowResultList = new ArrayList<String>();
//		if (csrTags == null) {
//			return flowResultList;
//		}
//		for (HashMap<String, String> item : csrTags) {
//			flowResultList.add(item.get(Constants.Unit.CSR_SENTENCE_TERMOUTPUT));
//		}
//		return flowResultList;
//	}
//
//	public ArrayList<TableBeliefDataObject> processSemanticInfo(ArrayList<SemanticInfo> semanticInfos) {
//
//		// inflate semantic info to table data object
//		ArrayList<TableBeliefDataObject> tableDOs = new ArrayList<TableBeliefDataObject>();
//
//		if (semanticInfos == null)
//			return tableDOs;
//
//		// process basic semantic info
//		// TODO: semantic_id need a way to avoid being overwritten
//		int semantic_id = 100;
//		for (SemanticInfo semanticInfo : semanticInfos) {
//
//			// inflate key word
//			tableDOs.add(new TableBeliefDataObject(semantic_id, "group", Constants.SLU.SEMANTIC));
//			tableDOs.add(new TableBeliefDataObject(semantic_id, "value", semanticInfo.getWord()));
//
//			// get rid of duplicate info with Set
//			LinkedHashSet<String> normalizedInfos = new LinkedHashSet<String>();
//			// get tag or get tag set
//			if (semanticInfo.getTagSet() == null) {
//				// get rid of unwanted and duplicate info
//				String result = normalizeSLUInfo(semanticInfo.getTag());
//				if (result != null) {
//					normalizedInfos.add(result);
//				}
//			} else {
//				for (String info : semanticInfo.getTagSet()) {
//					// get rid of unwanted info
//					String result = normalizeSLUInfo(info);
//					if (result != null) {
//						normalizedInfos.add(result);
//					}
//				}
//			}
//
//			// count length
//			tableDOs.add(new TableBeliefDataObject(semantic_id, Constants.SLU.SEMANTIC_INFO_LENGTH, String
//					.valueOf(normalizedInfos.size())));
//
//			// inflate all info data
//			int infoOrder = 0;
//			for (String info : normalizedInfos) {
//				infoOrder++;
//				tableDOs.add(new TableBeliefDataObject(semantic_id, Constants.SLU.SEMANTIC_INFO + infoOrder,
//						info));
//			}
//			semantic_id++;
//
//		}
//
//		// process advanced semantic info
//		HashMap<String, LinkedHashSet<String>> infoMap = new HashMap<String, LinkedHashSet<String>>();
//		for (SemanticInfo semanticInfo : semanticInfos) {
//
//			if (semanticInfo.getTagSet() != null) {
//
//				for (String info : semanticInfo.getTagSet()) {
//					// get rid of unwanted info
//					String result = normalizeSLUInfo(info);
//					if (result != null) {
//						if (infoMap.containsKey(result)) {
//							infoMap.get(result).add(semanticInfo.getWord());
//						} else {
//							LinkedHashSet<String> normalizedInfos = new LinkedHashSet<String>();
//							normalizedInfos.add(semanticInfo.getWord());
//							infoMap.put(result, normalizedInfos);
//						}
//					}
//
//				}
//			}
//		}
//
//		// inflate semantic info to table data object
//		getEngine().log(" infoMap" + infoMap.toString());
//		for (String key : infoMap.keySet()) {
//			semantic_id++;
//			// convert key
//			// inflate key word
//			tableDOs.add(new TableBeliefDataObject(semantic_id, Constants.SLU.SEMANTIC, key));
//
//			// count length
//			tableDOs.add(new TableBeliefDataObject(semantic_id, Constants.SLU.SEMANTIC_INFO_LENGTH, String
//					.valueOf(infoMap.get(key).size())));
//
//			int infoOrder = 0;
//			for (String info : infoMap.get(key)) {
//				infoOrder++;
//				tableDOs.add(new TableBeliefDataObject(semantic_id, Constants.SLU.SEMANTIC_INFO + infoOrder,
//						info));
//			}
//		}
//
//		return tableDOs;
//	}
//
//	public ArrayList<TableBeliefDataObject> processWildCardInfo(ArrayList<HashMap<String, String>> csrTags,
//			ArrayList<SemanticInfo> semanticInfos) {
//		ArrayList<TableBeliefDataObject> tableDOs = new ArrayList<TableBeliefDataObject>();
//		// TODO: semantic_id need a way to avoid being overwritten
//		int semantic_id = 400;
//		ArrayList<String> processedCsrTag = processCsrTag(csrTags);
//		ArrayList<TableBeliefDataObject> processedSemanticInfo = processSemanticInfo(semanticInfos);
//
//		for (TableBeliefDataObject tableBeliefDataObject : processedSemanticInfo) {
//			if (tableBeliefDataObject.tableTitle != null) {
//				if (tableBeliefDataObject.tableTitle.equals(Constants.Key.VALUE)) {
//					if (!processedCsrTag.contains(tableBeliefDataObject.tableValue)) {
//						tableDOs.add(new TableBeliefDataObject(semantic_id, "group", Constants.SLU.WILDCARD));
//						tableDOs.add(new TableBeliefDataObject(semantic_id, "value", tableBeliefDataObject.tableValue));
//						semantic_id++;
//					}
//				}
//			}
//		}
//		return tableDOs;
//	}
//
//	/**
//	 * this process is to restore info by remove some bnf symbol that is added
//	 * among bnf generation and get null when whole product id is not belong to
//	 * current app
//	 *
//	 * @param info
//	 * @return
//	 */
//	private String normalizeSLUInfo(String info) {
//
//		boolean hasSymbol = info.startsWith(Constants.Key.B_SYMBOL);
//
//		// rule 1 - start with symbol
//		if (hasSymbol) {
//
//			String bnfGeneratedStr = Constants.Key.B_SYMBOL + getEngine().getProductId() + "_";
//
//			// rule 2 - start with the same product id
//			if (info.startsWith(bnfGeneratedStr)) {
//				return info.replace(bnfGeneratedStr, "");
//			} else {
//				return null;
//			}
//		}
//
//		return info;
//	}
//
//	public ArrayList<TableBeliefDataObject> processAdvancedOutput() {
//
//		ArrayList<TableBeliefDataObject> tableDOs = new ArrayList<TableBeliefDataObject>();
//
//		// TODO: for test purpose
//		// int test_id = 200;
//		// tableDOs.add(new TableBeliefDataObject(test_id, "group", "date"));
//		// tableDOs.add(new TableBeliefDataObject(test_id, "value",
//		// "2015.10.1,13:30:00"));
//		// tableDOs.add(new TableBeliefDataObject(test_id, "year", "2015"));
//		// tableDOs.add(new TableBeliefDataObject(test_id, "month", "10"));
//		// tableDOs.add(new TableBeliefDataObject(test_id, "day", "1"));
//		// tableDOs.add(new TableBeliefDataObject(test_id, "hour", "13"));
//		// tableDOs.add(new TableBeliefDataObject(test_id, "minute", "30"));
//		// tableDOs.add(new TableBeliefDataObject(test_id, "second", "00"));
//		// tableDOs.add(new TableBeliefDataObject(test_id, "date_from",
//		// "2015.10.1"));
//		// tableDOs.add(new TableBeliefDataObject(test_id, "date_to",
//		// "2015.10.2"));
//
//		// 時 edited by Wen-Ting, Li 2015/09/22
//		/*----------------------------------------------------------------------------------------------------------*/
//		// we are now talking about time
//		// time will be separate into three parts
//		// 1. Date
//		// 2. Time
//		// 3. Duration
//		TimeDS_CSR timeExpress = new TimeDS_CSR();
//		boolean isDateExist = false;
//		try {
//			timeExpress = getEngine().getSysContext().getCurrentCSRDates().get(0).get(0);
//			isDateExist = true;
//			Date normalizedDate = timeExpress.getNormalizedDate();
//
//			// part 1. Date
//			int Year = normalizedDate.getYear();
//			int Month = normalizedDate.getMonth();
//			int DayOfMonth = normalizedDate.getDate();
//			// part 2. Time
//			int Hour = normalizedDate.getHours();
//			int Minute = normalizedDate.getMinutes();
//			int Second = normalizedDate.getSeconds();
//			// part 3. Duration
//			// Duration_CSR duration = timeExpress.getDuration();
//			Date dateFrom = timeExpress.getRangeFrom();
//			Date dateTo = timeExpress.getRangeTo();
//			// if (duration != null) {
//			// dateFrom = duration.getDurationFrom();
//			// dateTo = duration.getDurationTo();
//			// }
//
//			// append output data to table
//			int time_id = 1;
//			tableDOs.add(new TableBeliefDataObject(time_id, "group", "date"));
//			tableDOs.add(new TableBeliefDataObject(time_id, "value", normalizedDate.toString()));
//			tableDOs.add(new TableBeliefDataObject(time_id, "year", String.valueOf(Year)));
//			tableDOs.add(new TableBeliefDataObject(time_id, "month", String.valueOf(Month)));
//			tableDOs.add(new TableBeliefDataObject(time_id, "day", String.valueOf(DayOfMonth)));
//			tableDOs.add(new TableBeliefDataObject(time_id, "hour", String.valueOf(Hour)));
//			tableDOs.add(new TableBeliefDataObject(time_id, "minute", String.valueOf(Minute)));
//			tableDOs.add(new TableBeliefDataObject(time_id, "second", String.valueOf(Second)));
//			tableDOs.add(new TableBeliefDataObject(time_id, "date_from", String.valueOf(dateFrom)));
//			tableDOs.add(new TableBeliefDataObject(time_id, "date_to", String.valueOf(dateTo)));
//
//		} catch (Exception e) {
//			isDateExist = false;
//
//			int time_id = 1;
//			tableDOs.add(new TableBeliefDataObject(time_id, "group", "date"));
//			tableDOs.add(new TableBeliefDataObject(time_id, "value", "-1"));
//			tableDOs.add(new TableBeliefDataObject(time_id, "year", "-1"));
//			tableDOs.add(new TableBeliefDataObject(time_id, "month", "-1"));
//			tableDOs.add(new TableBeliefDataObject(time_id, "day", "-1"));
//			tableDOs.add(new TableBeliefDataObject(time_id, "hour", "-1"));
//			tableDOs.add(new TableBeliefDataObject(time_id, "minute", "-1"));
//			tableDOs.add(new TableBeliefDataObject(time_id, "second", "-1"));
//			tableDOs.add(new TableBeliefDataObject(time_id, "date_from", "-1"));
//			tableDOs.add(new TableBeliefDataObject(time_id, "date_to", "-1"));
//		}
//
//		// isDateExist is true while this data available.
//		// As talked above, on expression (which app developer may
//		// concern)
//		// 1. Date concern : Year, Month, DayOfMonth (title name)
//		// 2. Time concern : Hour, Minute, Second (title name)
//		// 3. Duration concern : date1, date2 (title name)
//		// we may talked more about item3 for expression of duration.
//		/*----------------------------------------------------------------------------------------------------------*/
//
//		/*
//		 * here we can access the language, to know which language is spoken by
//		 * user.
//		 */
//		boolean isNumbersExist = false;
//		boolean isLanguageTypeExist = false;
//		boolean act_language = true;
//		boolean act_NumberCatch = true;
//		String lang = "";
//		ArrayList<String> numbers = new ArrayList<String>();
//		try {
//			String originalSentence = getEngine().getDMContext().getSluResult().multiSentenceResult.get(0)
//					.getSentence();
//			if (act_language) {
//
//				Matcher matcher = Pattern.compile("\\p{InCJKUnifiedIdeographs}").matcher(originalSentence);
//				StringBuffer out = new StringBuffer();
//				while (matcher.find()) {
//					out.append(matcher.group());
//				}
//				// no chinese word
//				if (out.toString().length() == 0) {
//					lang = "en";
//				} else {
//					lang = "zh";
//				}
//				isLanguageTypeExist = true;
//
//				// append output data to table
//				int language_id = 2;
//				tableDOs.add(new TableBeliefDataObject(language_id, "group", "lang"));
//				tableDOs.add(new TableBeliefDataObject(language_id, "value", lang));
//			}
//			/*
//			 * here we are talking about the number, which developer can know
//			 * numbers
//			 */
//			if (act_NumberCatch) {
//				numbers = getNumberSetFromString(originalSentence);
//				isNumbersExist = true;
//			}
//		} catch (Exception e) {
//			// sentence from slu doesnot exist;
//		}
//
//		return tableDOs;
//	}
//
//	private static ArrayList<String> getNumberSetFromString(String string) {
//		ArrayList<String> numberSet = new ArrayList<String>();
//
//		String collectNum = "";
//		int continum = 0;
//		for (int i = 0; i < string.length(); i++) {
//			int tempC = string.charAt(i);
//			if (tempC <= 57) {
//				continum = 1;
//				collectNum = collectNum + string.charAt(i);
//			} else {
//				continum = 0;
//				if (!collectNum.equals("")) {
//					numberSet.add(collectNum);
//					collectNum = "";
//				}
//
//			}
//		}
//
//		if (continum == 1) {
//			numberSet.add(collectNum);
//		}
//
//		return numberSet;
//	}

	private FlowMachine getEngine() {
		return mFlowEngine;
	}

}
