package com.appmachine.flowmachine;
public class Constants {
	public static final String version = "0.11.10.0";
	public static final String updateDate = "17.2.21";
	public class Key {
		public static final String FLOWENGINE = "FlowMachine";
		public static final String B_SYMBOL = "$";
		public static final String ID = "id";
		public static final String NAME = "name";
		public static final String TYPE = "type";
		public static final String PLAY_TYPE = "playType";
		public static final String VALUE = "value";
		public static final String GOTO = "goto";
		public static final String INPUT = "input";
		public static final String OUTPUT = "output";
		public static final String OUTPUT__TYPE_DEFAULT_TYPE = "defaultOutputType";
		public static final String OUTPUT__TYPE_CONDITIONAL = "conditionalOutputs";
		public static final String OUTPUT__TYPE_CONDITIONAL_DEFAULT = "conditionalDefaultOutput";
		public static final String SAVE_OUTPUT_TO_BELIEF = "setBelief";
		public static final String LIBRARY = "library";
		public static final String SOURCE = "source";
		public static final String WAITFORCOMPLETE = "waitForComplete";
		public static final String UNITS = "units";
		public static final String TARGET = "target";
		public static final String TEXT = "text";
		
		public static final String DATA = "data";
		public static final String USER_ID = "sysinfo_userid";
		public static final String APP_ID = "sysinfo_appid";
		public static final String APP_LANG = "sysinfo_lang";
		public static final String G11N = "g11nDefault";
		public static final String ENV = "env";
		public static final String MODE = "mode";
		public static final String MODE_TYPE = TYPE;
		public static final String MODE_TYPE_NORMAL = "normal";
		public static final String MODE_TYPE_DEBUG = "debug";
		public static final String MODE_KEY = "key";
		public static final String MODE_TIMESTAMP = "timestamp";
		public static final String FLOW = "flow";
	}
	public class Unit {
		public static final String REGISTER_EVENT = "register_event";
		public static final String UNREGISTER_EVENT = "unregister_event";
		
		public static final String EVENT = "event";
		public static final String EVENT_TYPE = "eventType";
		public static final String EVENT_SOURCE = Key.SOURCE;
		public static final String EVENT_PARAMETERS = "parameters";

		public static final String BNF = "bnf";
		public static final String GROUP = "group";
		public static final String GROUP_ENTRY = "groupEntry";
		public static final String GROUP_LOOP = "loop";
		public static final String GROUP_LOOP_START = "loopStart";
		public static final String GROUP_LOOP_END = "loopEnd";
		public static final String PARALLEL = "parallel2";
		public static final String PARALLEL_REGIONS = "regions";
		public static final String PARALLEL_FINALSTATES = "finals";
		public static final String SET_PROPERTIES = "set_properties";
		public static final String SET_PROPERTIES__PROPERTY = "property";
		public static final String SET_PROPERTIES__PROPERTIES = "properties";
		public static final String EXIT_APP = "exit_app";
		public static final String RANDOM_CHOICE = "random_choice";
		public static final String OUTPUT_TYPE_TIME_OUT = "time_out";
		public static final String OUTPUT_TYPE_NO_ONE_SPEAK = "no_one_speak";
		
		public static final String BNF_INTENTION = "intention";
		
		public static final String CSR = "csr";
		public static final String CSR_TIMEOUT = "timeout";
		public static final String CSR_INTENTIONS = "sentences";
		public static final String CSR_SENTENCE_TEXT = "sentenceText";
		public static final String CSR_SENTENCE_TERMTYPE = "termType";
		public static final String CSR_SENTENCE_TERMTYPE_CLASS = "class";
		public static final String CSR_SENTENCE_TERMTYPE_INSTANCE = "instance";
		public static final String CSR_SENTENCE_TERMOUTPUT = "termOutput";
		public static final String CSR_CLARIFICATION= "clarification";
		
		public static final String CSR_CONDITIONAL_OUTPUTS = "conditionalOutputs";
		public static final String CSR_OUTPUT_DEFAULT = "output_default";
		
		public static final String IF = "if";
		public static final String TTS = "tts";
		public static final String TTS__TEXT = "text";
		public static final String TTS__EMOTION = "emotion";
		public static final String TTS__EMOTION__DEFAULT = "0";
		public static final String TTS__ALPHA = "alpha";
		public static final String TTS__ALPHA__DEFAULT = "0.55";
		public static final String CALL_API = "call_api";
		public static final String CALL_API__APP_COMPONENT_TYPE = "appComponentType";
		public static final String CALL_API__APP_PACKAGE_NAME = "AppPackageName";
		public static final String CALL_API__INTENT_CATEGORY	= "IntentCategory";
		public static final String CALL_API__INTENT_ACTION	= "IntentAction";
		public static final String CALL_API__INTENT_PARAMS = "IntentDataParams";
	}
	
	public class RuntimeError {
		public static final String FAIL_TO_RETREIVE_SLU= "[Warning] Fail To Retrieve SLU data";
	}
	public class Notification {
		public static final String EMPTY_TABLE = "[Caution] No table belief with given name is found";
		public static final String RESUME_CSR = "[Caution] Nothing triggered with this input, resume to CSR again";
		public static final String EVENT_ACK_NOT_MATCHED = "[Caution] Event Ack not matched, nothing triggered";
		public static final String NOT_A_PLAYER_EVENT = "[Caution] Nothing triggered, % is not a Player Event";
		public static final String NOT_ACCEPTABLE_EVENT_TYPE = "[Caution] Nothing triggered, Event % is not an acceptable event type for now";
		public static final String UNKNOWN_EVENT_TYPE = "[Caution] Nothing triggered. Unknown event type for now";
		public static final String EVENT_DECLINED = "[Caution] Nothing triggered, event declined";
		public static final String QA_FINISHED = "[QA] Interupted QA Finished. Go back to previous state.";
		public static final String EOF = "EOF (End Of Flow)";
	}
	
	public class G11N {
		public static final String EN_US= "en_US";
	}
	
	public static final class System {
		public static final String[] SYSTEM_INNER_TRANSITIONS = {Runtime.GOTO_PREVIOUS_EVENT};
	}
	
	public class Event {
		public static final String SCENE_BEGIN = "scene_begin";
	}
	
	public class SLU {
		public static final String SEMANTIC = "semantic";
		public static final String WILDCARD = "wildcard";
		public static final String SEMANTIC_INFO = "info";
		public static final String SEMANTIC_INFO_LENGTH = "info_length";
	}
	public class Debbuger {
		public static final String SERVER = "https://platforms.firebaseio.com/debugger";
		public static final String SERVER_CLIENTS = "https://platforms.firebaseio.com/debugger/clients/";
		public static final String CLIENTS = "clients";
		public static final String USER_ID = "user_id";
		public static final String PRODUCT_ID = "productId";
		public static final String PROJECT_ID = "projectId";
		public static final String CONTROL = "control";
		public static final String CONTROL_RUN = "setup";
		public static final String CONTROL_STOP = "stop";
		public static final String CONTROL_STEP = "step";
		public static final String CONTROL_PAUSE = "pause";
		public static final String CONTROL_EXIT = "exit";
		public static final String CONTROL_STEPOVER = "step_over";
		public static final String FLOW = "flow";
	}
	public class Runtime {
		public static final String COMMAND_QA = "#QA_FINISHED";
		public static final String COMMAND_SLU = "#SLU_RESULT";
		public static final String COMMAND_PLAYER = "#PLAYER_RESULT";
		
		public static final String APP_TRANSITION_SENTENCE = "我聽到你說\"";
		public static final String EVENT_TTS_COMPLETED= "tts.completed";
		public static final String EVENT_SYSTEM_TIMEOUT= "timeout";
		
		public static final String SYSTEM_LIFECYCLE__EVENT_INIT= "#SystemInitEvent";
		public static final String SYSTEM_LIFECYCLE__STATE_INIT= "#SystemInitEvent";
		public static final String GOTO_PREVIOUS_EVENT = "#goto_previous";
		public static final String COMPONENT_END = "#compEnd_";
		public static final String GOTO = "#goto_";
		public static final String PARALLEL_END = "#paEnd_";
		
		public static final String PLAYER__EVENT_NAME = "eventName";
		public static final String PLAYER__EVENT_INIT_APP = "init_app";
		public static final String PLAYER__EVENT_INIT_SCENE = "init_scene";
		public static final String PLAYER__EVENT_TYPE_COMPONENT = "component";
		public static final String PLAYER__EVENT_TYPE_EOF = "end_of_flow";
		public static final String PLAYER__EVENT_TYPE_DECLINED = "event_declined";
		public static final String PLAYER__EVENT_TYPE_SYSERR = "sys_err";
		public static final String PLAYER__EVENT_TYPE_SYSERR_OF_COMP = "sys_err_of_component";
		public static final String PLAYER__EVENT_NAME_PERCEPTION = "irp.perception.ctl";		
		public static final String PLAYER__MESSAGE = "message";
		public static final String PLAYER__MESSAGE_EOF = "End Of Flow";
		public static final String PLAYER__SYSERR_COMP = "compType";
		public static final String PLAYER__DATA = "data";
		public static final String PLAYER__EVENT_NAME_PLAYER_GENERAL = "player.general";
		public static final String PLAYER__EVENT_ACK = "eventAck";
		public static final String PLAYER__EVENT_SOURCE = Key.SOURCE;
		public static final String PLAYER__EVENT_SOURCE__PERCEPTION_DS_FLOWENGINE = "module.perception.ds.flowEngine";
		public static final String PLAYER__CONFIG = "config";
		public static final String PLAYER__INTENT = "intent";
		public static final String PLAYER__TYPE = "type";
		public static final String PLAYER__TYPE_ACK = "ack";

		public static final String PLAYER__CONFIG_TYPE__EXEC = "exec";
		public static final String PLAYER__CONFIG_TYPE__PLAYCOTNENT = "play_content";
		public static final String PLAYER__CONFIG_EVENTTYPE = "eventType";
		public static final String PLAYER__CONFIG_EVENTTYPE__TTS = "irp_tts";
		public static final String PLAYER__CONFIG_EVENTTYPE__CSR = "irp_csr";
		public static final String PLAYER__CONFIG_SERVICENAME = "serviceName";
		public static final String PLAYER__CONFIG_SERVICENAME__SPEECH = "module.perception.speech";
		public static final String PLAYER__CONFIG_TYPE_PLAYCOTNENT = "play_content";
		public static final String PLAYER__CONFIG_APPID = "appID";
		public static final String PLAYER__CONFIG_PRODUCTID = "productID";
		public static final String PLAYER__CONFIG_SCENE_ID = "sceneId";
		public static final String PLAYER__CONFIG_STATEID = "stateID";
		public static final String PLAYER__CONFIG_DECLINE_ACK = "declinedEventAck";
		public static final String PLAYER__CONFIG_INITSCENE = "initScene";
	}
	
}
