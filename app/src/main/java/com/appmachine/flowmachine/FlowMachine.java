package com.appmachine.flowmachine;

import android.support.annotation.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.appmachine.flowmachine.components.interfaces.Executable;
import com.appmachine.flowmachine.components.interfaces.Types;
import com.appmachine.flowmachine.stateMachines.IStateMachineAdapter;
import com.appmachine.flowmachine.stateMachines.SquirrelAdapter;
import com.dep.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.asus.atc.client.irp.uadm.api.UserAccountDataManagementService;
//import com.asus.atc.client.market.hws.api.MarketClientSupportService;
//import com.asus.ctc.ams.drivers.SystemContext_SMEngine;
//import com.asus.ctc.ams.smbdm.DialogueManagerContext;
//import com.asus.ctc.ams.smbdm.OntologyQuestionAnswerEngine;
//import com.asus.ctc.ams.smbdm.StateBNF;
import com.appmachine.flowmachine.PathManager.QueueDO;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import static java.lang.System.err;

/**
 * @author Eason Pai
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class FlowMachine {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    public DevPhrase devPhrase = DevPhrase.DEBUG;

    public class FlowEngineAppDO {
        public String initScene, initState, initEvent;
        public String trigger = null;
        public String productId = null;
        public String appId;
        public String currentScene;
        public String utterance;
        public boolean hasUtterance;
        public HashMap<String, Object> playerProtocolSent;
        public String playerEventTypeSent;
        public String playerEventAckSent;
        public boolean similationMode = true;
        public boolean hasDefaultOutput = false;
        public boolean hasInitScene = false;
        public boolean hasInitState = false;
        public boolean hasInitEvent = false;
        public ArrayList<HashMap<String, String>> sluData = null;
        private String initEventSource = null;
        private String initEventParameter = null;

        /**
         * @param protocol the playerProtocolSent to set
         */
        public void setPlayerProtocolSent(HashMap<String, Object> protocol) {
            this.playerProtocolSent = protocol;

            // this is special event type
            if (((Map) protocol.get(Constants.Runtime.PLAYER__DATA)).containsKey("type")) {
                this.playerEventTypeSent = ((Map) protocol.get(Constants.Runtime.PLAYER__DATA)).get("type")
                        .toString();
                if (((Map) protocol.get(Constants.Runtime.PLAYER__DATA))
                        .containsKey(Constants.Runtime.PLAYER__CONFIG_DECLINE_ACK)) {
                    this.playerEventAckSent = ((Map) protocol.get(Constants.Runtime.PLAYER__DATA)).get(
                            Constants.Runtime.PLAYER__CONFIG_DECLINE_ACK).toString();
                }
            }

            // normal player event
            else {
                this.playerEventTypeSent = ((Map) ((Map) protocol.get(Constants.Runtime.PLAYER__DATA))
                        .get(Constants.Runtime.PLAYER__CONFIG)).get("type").toString();
                this.playerEventAckSent = ((Map) protocol.get(Constants.Runtime.PLAYER__DATA)).get(
                        Constants.Runtime.PLAYER__EVENT_ACK).toString();
            }
        }

        /**
         * @param scene the init scene to set
         */
        public FlowEngineAppDO setInitScene(String scene) {
            if (scene != null) {
                this.hasInitScene = (scene.length() > 0);
            } else {
                this.hasInitScene = false;
            }
            this.currentScene = this.initScene = scene;
            return this;
        }

        /**
         * @param state the init state to set
         */
        public void setInitState(String state) {
            if (state != null) {
                this.hasInitState = (state.length() > 0);
            } else {
                this.hasInitState = false;
            }
            this.initState = state;
        }

        /**
         * @param event the init event to set
         */
        public void setBeginWithEvent(String event, String source, String parameter) {
            if (event != null) {
                this.hasInitEvent = (event.length() > 0);
            } else {
                this.hasInitEvent = false;
            }
            this.initEvent = event;
            this.initEventSource = source;
            this.initEventParameter = parameter;
        }


        /**
         * @param utterance the utterance to set
         */
        public void setBeginWithUtterance(String utterance) {
            if (utterance != null) {
                this.hasUtterance = (utterance.length() > 0);
            } else {
                this.hasUtterance = false;
            }
            this.utterance = utterance;
        }
    }

    public enum EnginePhrase {
        ENGINE_INITIALIZED, STARTING_APP, INITIALIZING_APP, APP_INITIALIZED, INITIALIZING_FLOW, FLOW_INITIALIZED, APP_STARTED, FLOW_RUNNING, FLOW_STOPPED, END_APP, APP_EXITED
    }

    public enum DevPhrase {
        NORMAL, DEBUG
    }

    public enum PlayerProtocolType {
        INIT_APP, INIT_SCENE, COMPONENT, EOF
    }

    private FlowEngineAppDO mAppDO;
    private Long mUserId;
    private ConfigModelManager mConfigModelManager;
    private EventManager mEventManager;
    private ComponentManager mComponentManager;
    private BeliefManager mBeliefManager;
    private FlowChangeNotification mStateChangeAction;
    private IStateMachineAdapter mMachineAdapter;
    private PathManager mPathManager;
    private SystemContext_SMEngine mSysContext = null;
    private UserAccountDataManagementService mUserAccountDataManagementService = null;
    protected MarketClientSupportService mAppMarketServer;
    private ResourceManager mResourceManager;
    private CompatibilityManager mCompatibilityManager;
    private EnginePhrase mEngineLifeCycle;
    private DialogueManagerContext dmContext;
    private OntologyQuestionAnswerEngine qaEngine;
    private boolean isSimulationMode = false;
    private HashMap<String, Object> systemPlayerProtocol;
    private StringBuilder mLogTimeString = null;
    private ArrayList<EngineChangeAction> mEngineChangeListeners; // when flow
    // engine
    // start/re-start,
    // stop
    // existing
    // timer

    /**
     */
    public FlowMachine() {
        setUserId(0L);
        setSysContext(new SystemContext_SMEngine());
        initEngine();
    }

    /**
     * setup AppSetting object for app
     *
     * setApp --> startApp --> initApp -->
     *
     * @param config
     * @param similationMode
     */
    public void setApp(Map config, boolean similationMode) {

        startApp(null, new AppSettings("0", config)
//				.setBeginWithScene(scene)
//				.setBeginWithUtterance(userUtterance)
                .setSimulationMode(similationMode)
                .setAppID("0"));
    }

    public void setApp(Map config, String initState, boolean similationMode) {

        startApp(null, new AppSettings("0", config)
//				.setBeginWithScene(scene)
//				.setBeginWithUtterance(userUtterance)
                .setBeginWithState(initState)
                .setSimulationMode(similationMode)
                .setAppID("0"));
    }

    public void setApp(String jsonString, boolean similationMode) {

        startApp(null, new AppSettings("0", getConfigModel().parseJson(jsonString))
//				.setBeginWithScene(scene)
//				.setBeginWithUtterance(userUtterance)
                .setSimulationMode(similationMode)
                .setAppID("0"));
    }

    public void setApp(String jsonString, String initState, boolean similationMode) {

        startApp(null, new AppSettings("0", getConfigModel().parseJson(jsonString))
//				.setBeginWithScene(scene)
//				.setBeginWithUtterance(userUtterance)
                .setBeginWithState(initState)
                .setSimulationMode(similationMode)
                .setAppID("0"));
    }

    public FlowMachine cleanListeners() {
        getComponentManager().resetListeners();
        return this;
    }


    /**
     * Register a callback to be invoked when a state has been changed
     *
     * @param component
     */
    public FlowMachine on(Types component, @Nullable Executable executable) {

        log("[Flow] Registering Event :" + component.name());

        getComponentManager().registerComponent(component.name(), executable);
        return this;
    }

    public Executable getComponentAction(Types component) {
        return getComponentManager().getRegisteredComponentAction(component.name());
    }

    // ===========================================================


    /**
     * @param userNum    unique user id
     * @param sysContext interface to system modules
     */
    public FlowMachine(Long userNum, SystemContext_SMEngine sysContext) {
        setUserId(userNum);
        setSysContext(sysContext);
        initEngine();
    }

    /**
     * For testing purpose
     *
     * @param userNum
     * @param userAccountDataManagementService
     */
    public FlowMachine(Long userNum, UserAccountDataManagementService userAccountDataManagementService) {
        setUserId(userNum);
        setUsrAccMgrService(userAccountDataManagementService);
        initEngine();
    }

    /**
     * offline mode
     *
     * @param userNum
     * @param userAccountDataManagementService
     * @param appMarketServer
     */
    public FlowMachine(Long userNum, UserAccountDataManagementService userAccountDataManagementService,
                       MarketClientSupportService appMarketServer) {
        setUserId(userNum);
        setUsrAccMgrService(userAccountDataManagementService);
        mAppMarketServer = appMarketServer;
        initEngine();
    }

    private void initEngine() {
        mLogTimeString = new StringBuilder();

        log(LOG_LEVEL.ENGINE, "======================================================================================");
        log(LOG_LEVEL.ENGINE, "▼ Starting Engine ( version:" + Constants.version + ", User:" + getUserId()
                + ")");

        setBeliefManager(new BeliefManager(this));
        setComponentManager(new ComponentManager(this));
        setConfigManager(new ConfigModelManager(this));

        setMachineAdapter(new SquirrelAdapter(this));
        // setMachineAdapter(new Stateless4jAdapter(this));
        setPathManager(new PathManager(this));
        setResourceManager(new ResourceManager(this));
        mCompatibilityManager = new CompatibilityManager(this);

        setEngineLifeCycle(EnginePhrase.ENGINE_INITIALIZED);

    }

    static public class AppSettings {
        public String productId = null;
        public String appId = null;
        public String eventId = null;
        public String eventSource = null;
        public String eventParameter = null;
        public String utterance = null;
        public Map appConfig = null;
        public String initScene = null;
        public String initState = null;
        public boolean similationMode = false;
        ArrayList<HashMap<String, String>> sluData = null;

        public AppSettings(String productId, Map appConfig) {
            this.productId = productId;
            this.appConfig = appConfig;
        }

        public AppSettings setAppID(String appId) {
            this.appId = appId;
            return this;
        }

        public AppSettings setSimulationMode(boolean similationMode) {
            this.similationMode = similationMode;
            return this;
        }

        public AppSettings setBeginWithUtterance(String utterance) {
            this.utterance = utterance;
            return this;
        }

        public AppSettings setBeginWithUtterance(String utterance, ArrayList<HashMap<String, String>> sluData) {
            this.utterance = utterance;
            this.sluData = sluData;
            return this;
        }

        public ArrayList<HashMap<String, String>> getSLUData() {
            return this.sluData;
        }

        public AppSettings setBeginWithEventType(String eventType) {
            this.eventId = eventType;
            return this;
        }

        public AppSettings setBeginWithEvent(String eventType, String source, String parameter) {
            this.eventId = eventType;
            this.eventSource = source;
            this.eventParameter = parameter;
            return this;
        }

        public AppSettings setBeginWithScene(String scene) {
            this.initScene = scene;
            return this;
        }

        public AppSettings setBeginWithState(String state) {
            this.initState = state;
            return this;
        }
    }

    /**
     * - update Life Cycle
     * - dump app info
     *
     * @param dmContext
     * @param appSettings
     */
    public void startApp(DialogueManagerContext dmContext, AppSettings appSettings) {
        setEngineLifeCycle(EnginePhrase.STARTING_APP);
        log(LOG_LEVEL.ENGINE, "");
        log(LOG_LEVEL.ENGINE, "======================================================================================");
        log(LOG_LEVEL.ENGINE, "▼ Starting App : " + appSettings.productId + " (" + Constants.version
                + ", simulation = " + appSettings.similationMode + ") ");

//		setDMContext(dmContext);
//		setQaEngine(dmContext.getQuestionAnswerEngine());
        // log app config when in debug mode
        if (devPhrase == DevPhrase.DEBUG) {
            log(LOG_LEVEL.DEBUG, " config= " + appSettings.appConfig);
        }

        // ======================================
        // init app
        // ======================================
        initApp(appSettings);
    }

    /**
     *
     * - initialize Config Model
     * - reset beliefs
     * - setup event for external observers (DEPRECATED)
     *
     * @param appSettings
     */
    private void initApp(AppSettings appSettings) {
        setEngineLifeCycle(EnginePhrase.INITIALIZING_APP);
        // ======================================
        // switch app with found app
        // ======================================
        // status transition
//		if (getDMContext() != null) {
//			if (getDMContext().getBestCorrectedSentence() != null) {
//				log(LOG_LEVEL.TRANSITION, Constants.Runtime.APP_TRANSITION_SENTENCE
//						+ getDMContext().getBestCorrectedSentence() + "\"...");
//			}
//		}

        // ======================================
        // init model
        // ======================================
        mAppDO = new FlowEngineAppDO();
        mAppDO.setInitScene(appSettings.initScene);
        mAppDO.setInitState(appSettings.initState);
        mAppDO.currentScene = appSettings.initScene;
        mAppDO.appId = appSettings.appId;
        mAppDO.productId = appSettings.productId;
        mAppDO.setBeginWithUtterance(appSettings.utterance);
        mAppDO.setBeginWithEvent(appSettings.eventId, appSettings.eventSource, appSettings.eventParameter);
        mAppDO.similationMode = appSettings.similationMode;
        mAppDO.sluData = appSettings.sluData;

        // parse config
//        getConfigModel().parseApp(appSettings.appConfig);
        getConfigModel().setModel(appSettings.appConfig);
        // release
        appSettings = null;

        // reset belief data
        getBeliefManager().resetBelief();

        // ======================================
        // notify listeners
        // ======================================
        if (mEngineChangeListeners != null) {
            for (EngineChangeAction listener : mEngineChangeListeners) {
                listener.action(EnginePhrase.INITIALIZING_APP);
            }
            mEngineChangeListeners.clear();
        }
        mEngineChangeListeners = new ArrayList<EngineChangeAction>();

        // ======================================
        // notify player
        // ======================================
        systemPlayerProtocol = notifyPlayer(PlayerProtocolType.INIT_APP, true, null);

        // ======================================
        // simulation mode
        // ======================================
        initScene(mAppDO);
    }

    /**
     * take no effect right now
     */
    public void exitApp() {
    }

    /**
     * - set entry point for state machine
     * - reset path
     * - fire 1st event to trigger app
     *
     * @param appDO
     */
    protected void startScene(FlowEngineAppDO appDO) {
        log(LOG_LEVEL.ENGINE, "▼ Start Scene ▼");
        setEngineLifeCycle(EnginePhrase.APP_STARTED);

        getPathManager().reset();
        getMachineAdapter().start(appDO.initState);

        // if contains defaultOutputType then go defaultOutputType first
        // this special manipulation is handle in ComponentManager.componentEnd
        // and Components too
        // an app have multiple entrance in design of IRP architecture
        if (appDO.hasDefaultOutput) {
            getMachineAdapter().fire(Constants.Runtime.COMPONENT_END + appDO.initState);
        } else {
            getMachineAdapter().fire(appDO.trigger);
        }
    }

    public void gotoScene(String scene) {
        initScene(new FlowEngineAppDO().setInitScene(scene));
    }

    /**
     * - init state machine
     * - prepare startup settings
     *
     * @param appDO
     */
    protected void initScene(FlowEngineAppDO appDO) {
        // engine state
        setEngineLifeCycle(EnginePhrase.INITIALIZING_FLOW);

        // ======================================
        // find init scene and state of app
        // ======================================
        getMachineAdapter().init();

        if (appDO.hasUtterance) {

            // find target state
            Map entryState = getConfigModel().findCSRModelByIntention(appDO.utterance);
            if (entryState == null) {
                log(LOG_LEVEL.ENGINE, "[Alert] intention (" + appDO.utterance + ") for start app is not found");
                return;
            }

            log(LOG_LEVEL.ENGINE, "intention (" + appDO.utterance + ") to start is found in flow");
            // set entry state
            appDO.setInitScene(getConfigModel().getSceneIdContainsUnitById(entryState.get("id").toString()));
            appDO.currentScene = appDO.initScene;
            appDO.initState = entryState.get("id").toString();
            appDO.hasDefaultOutput = getConfigModel().isContainsDefaultOutputTypeByUnit(entryState);

            // set trigger
            saveSLUToBelief(appDO);
            appDO.trigger = appDO.utterance;

        } else if (appDO.hasInitEvent) {
            // find target state
            Map event = getConfigModel().find1stEventModelByEventTypeAndSource(appDO.initEvent, appDO.initEventSource);
            if (event == null) {
                log(LOG_LEVEL.ENGINE, "[Alert] event (" + appDO.initEvent + ") to start is not found");
                return;
            }
            log(LOG_LEVEL.ENGINE, "event (" + appDO.initEvent + ") to start is found in flow");
            // set entry state
            appDO.setInitScene(getConfigModel().getSceneIdContainsUnitById(event.get("id").toString()));
            appDO.currentScene = appDO.initScene;
            appDO.initState = event.get("id").toString();
            appDO.hasDefaultOutput = getConfigModel().isContainsDefaultOutputTypeByUnit(event);

            // save event output
            Object saveOutputToBelief = event.get(Constants.Key.SAVE_OUTPUT_TO_BELIEF);
            if (saveOutputToBelief != null && saveOutputToBelief.toString().length() > 0) {
                getBeliefMap().put(saveOutputToBelief.toString(), appDO.initEventParameter);
            }

            appDO.trigger = Constants.Runtime.COMPONENT_END + event.get("id").toString();
        } else if (appDO.hasInitState) {

            // TODO: priority of init state is higher than init scene
            appDO.initScene = appDO.currentScene = getConfigModel().getInitScene();

            // dynamically insert config
            getMachineAdapter().configure(Constants.Runtime.SYSTEM_LIFECYCLE__STATE_INIT,
                    appDO.initState, Constants.Runtime.SYSTEM_LIFECYCLE__EVENT_INIT);

            // set init state
            appDO.trigger = Constants.Runtime.SYSTEM_LIFECYCLE__STATE_INIT;

            // reset initState
            appDO.initState = Constants.Runtime.SYSTEM_LIFECYCLE__STATE_INIT;

        } else if (appDO.hasInitScene) {
            appDO.initState = getConfigModel().getSceneBeginIdByScene(appDO.initScene);
            if (appDO.initState == null) {
                log(LOG_LEVEL.ENGINE, "[Alert] scene begin not found");
                notifyObserver("stop");
                return;
            }

            // dynamically insert config
            getMachineAdapter().configure(Constants.Runtime.SYSTEM_LIFECYCLE__STATE_INIT,
                    appDO.initState, Constants.Runtime.SYSTEM_LIFECYCLE__EVENT_INIT);

            // set init state
            appDO.initState = Constants.Runtime.SYSTEM_LIFECYCLE__STATE_INIT;
            appDO.trigger = Constants.Runtime.SYSTEM_LIFECYCLE__STATE_INIT;
        } else {
            log(LOG_LEVEL.ENGINE, "App start with default scene");
            appDO.initScene = appDO.currentScene = getConfigModel().getInitScene();

            // fallback to the 1st scene if no scene found
            try {
                appDO.initState = getConfigModel().getSceneBeginIdOfInitScene();
            } catch (Exception err){
                appDO.initScene = getConfigModel().getFirstScene();
                appDO.initState = getConfigModel().getSceneBeginIdByScene(appDO.initScene);
            }

            // dynamically insert config
            getMachineAdapter().configure(Constants.Runtime.SYSTEM_LIFECYCLE__STATE_INIT,
                    appDO.initState, Constants.Runtime.SYSTEM_LIFECYCLE__EVENT_INIT);

            // set init state
            appDO.initState = Constants.Runtime.SYSTEM_LIFECYCLE__STATE_INIT;
            appDO.trigger = Constants.Runtime.SYSTEM_LIFECYCLE__STATE_INIT;
        }

        // ------------------------------------------------------------------------------------------
        // init flow
        // ------------------------------------------------------------------------------------------

        // prepare scene flow data
        List<Map> flows = getConfigModel().findFlowUnitsBySceneId(appDO.initScene);
        if (flows == null) {
            log(LOG_LEVEL.ENGINE, "[ALERT] scene data not found");
            return;
        }

        // ------------------------------------------------------------------------------------------
        // setup state machine
        // ------------------------------------------------------------------------------------------
        // prepare state machine config
        log(LOG_LEVEL.ENGINE, "============================================================");
        log(LOG_LEVEL.ENGINE, "▼ Scene Info ▼ (scene = " + getCurrentScene() + " ('" + getCurrentSceneName() + "'), state length =" + flows.size()
                + ")");
        getMachineAdapter().setupConfig(flows);
        log(LOG_LEVEL.ENGINE, "============================================================");

        // ======================================
        // notify client
        // ======================================
        HashMap<String, Object> protocol = new HashMap<String, Object>();
        protocol.put(Constants.Runtime.PLAYER__CONFIG_SCENE_ID, appDO.initScene);
        systemPlayerProtocol = notifyPlayer(PlayerProtocolType.INIT_SCENE, true, protocol);

        // ======================================
        // simulation mode
        // ======================================
        // then we will wait for event ACK from player
        if (appDO.similationMode) {
            // pass fire process
            startScene(appDO);
        }
    }

    // TODO: should move save output to belief out
    private void saveSLUToBelief(FlowEngineAppDO appDO) {
        if (dmContext != null && appDO.sluData != null) {

            // extract flow result output as
            log(LOG_LEVEL.DEBUG, "SLU Tag data = " + appDO.sluData.toString());

            // simulate intention labeling data while test in Engine
            Map componentConfig = getConfigModel().findUnitModelById(appDO.initScene, appDO.initState);

            Object saveOutputToBelief = componentConfig.get("setBelief");
            if (saveOutputToBelief != null && saveOutputToBelief.toString().length() > 0) {

                // arrayList
                ArrayList<String> flowResultList = new ArrayList<String>();
                for (HashMap<String, String> item : appDO.sluData) {
                    flowResultList.add(item.get(Constants.Unit.CSR_SENTENCE_TERMOUTPUT));
                }

                // save output according to length of result no label data
                if (flowResultList.size() == 0) {
                    getBeliefMap().put(saveOutputToBelief.toString(), "0");
                } else if (flowResultList.size() == 1) {
                    // 1 label extract as string
                    getBeliefMap().put(saveOutputToBelief.toString(), flowResultList.get(0).toString());
                } else {
                    // save as table belief when
                    // length > 1
//					getBeliefManager().createTableBeliefByArray(saveOutputToBelief.toString(),
//							(ArrayList<String>) flowResultList);
                }
            }


        } else {

            log(LOG_LEVEL.ENGINE, "[Alert] dmContext or flowResults of SLU is null !!!! use pseudo data instead");
            // simulate intention labeling data while test in Engine
            Map componentConfig = getConfigModel().findUnitModelById(appDO.initScene, appDO.initState);
            Object saveOutputToBelief = componentConfig.get("setBelief");
            if (saveOutputToBelief != null && saveOutputToBelief.toString().length() > 0) {
                getBeliefMap().put(saveOutputToBelief.toString(), Constants.RuntimeError.FAIL_TO_RETREIVE_SLU);
            }
        }
    }

    /**
     * state machine adapter method
     *
     * @return
     */
    public Object getState() {
        return getMachineAdapter().getState();
    }

    /**
     * @return
     */
    public ArrayList<String> getAllRawStates() {
        return getMachineAdapter().getAllRawStates();
    }

    /**
     * state bnf format example:
     * <p>
     * HashMaps in ArrayList [{intention=86_Patten00f1764c03},
     * {intention=86_Patten00f177c805}, {intention=86_Patten00f17e28c7}]
     *
     * @return StateBNF BNF infos
     */
    public StateBNF getStateBNFs() {
//
//		if (getEngineLifeCycle() != EnginePhrase.APP_STARTED) {
//			log(LOG_LEVEL.ENGINE, "[getStateBNFs] engine not started");
//			return null;
//		}
//		Map unitModel = getConfigModel().findUnitModelById(getCurrentScene(), getState().toString());
//
//		if (unitModel == null) {
//			log(LOG_LEVEL.ENGINE, "[getStateBNFs] state not found");
//			return null;
//		}
//		String unitType = unitModel.get(Constants.Key.TYPE).toString();
//
//		// TODO: type is hard-coded
//		// react only for CSR type
//		if (unitType.equals(Constants.Unit.CSR)) {
//
//			ArrayList<HashMap<String, String>> stateBNFs = new ArrayList<HashMap<String, String>>();
//
//			// TODO: hard-coded test
//			for (String intention : (List<String>) unitModel.get(Constants.Unit.CSR_INTENTIONS)) {
//
//				// build check bnf data
//				HashMap<String, String> intentionMap = new HashMap<String, String>();
//				intentionMap.put(Constants.Unit.BNF_INTENTION, intention);
//				stateBNFs.add(intentionMap);
//
//			}
//
//			// check if contains defaultOutput
//			boolean containsConditionalDefaultOutput = getConfigModel().isContainsConditionalDefaultOutputTypeByUnit(
//					unitModel);
//			boolean containsDefaultOutput = getConfigModel().isContainsDefaultOutputTypeByUnit(unitModel);
//			int clarificationPossibility = 100; // default value
//			if (unitModel.containsKey(Constants.Unit.CSR_CLARIFICATION)) {
//				clarificationPossibility = Integer.parseInt(unitModel.get(Constants.Unit.CSR_CLARIFICATION)
//						.toString());
//			}
//
//			log(LOG_LEVEL.ENGINE,
//					"[getStateBNFs] product = " + getProductId() + ", state="
//							+ unitModel.get(Constants.Key.ID).toString() + ", hasDefaultAll ="
//							+ containsDefaultOutput + ", hasConditionalDefault=" + containsConditionalDefaultOutput
//							+ ", clarificationPossibility = " + clarificationPossibility + ", intentions = "
//							+ stateBNFs.toString());
//			return new StateBNF(getProductId(), stateBNFs, containsConditionalDefaultOutput, containsDefaultOutput,
//					clarificationPossibility);
//
//		} else {
//			return null;
//		}
        return null;
    }

    /**
     * state machine adapter method
     *
     * @return
     */
    public List<String> getAcceptableEvents() {
        return getPathManager().getRuntimeQueueIDsWithoutParallel();
    }


    /**
     * Returns true if {@code trigger} can be fired in the current state
     *
     * @param trigger Trigger to test
     * @return True if the trigger can be fired, false otherwise
     */
    public boolean canHandle(String trigger) {
        if (getMachineAdapter() == null) {
            return false;
        }
        return getMachineAdapter().canFire(trigger);
    }

    /**
     * This is the main method that triggers transition to change with
     * designated event string. A specific command name and a arbitrarily output
     * value are required.
     * <p>
     * Specific Command Name:
     * <li>QA Module: "#QA_FINISHED" command will make flow to go back to last
     * state. Example: fire(dmContext, "", "#QA_FINISHED", null)</li>
     * <p>
     * <li>SLU Module: "#SLU_RESULT". Example: fire(dmContext, "Patten001",
     * "#SLU_RESULT", {"termType":"instance|class", "termOutput":"Banana"})</li>
     * <p>
     * <li>Player Module: "#PLAYER_RESULT". Example: fire(dmContext,
     * "Patten001", "#PLAYER_RESULT", EVENT_JSON})</li>
     * </p>
     * </br>
     *
     * @param dmContext
     * @param event
     */
    @Deprecated
    public void fire(DialogueManagerContext dmContext, EngineEvent event) {
//        setDMContext(dmContext);

        // ======================================
        // handle compatibility issues
        // ======================================

        // ======================================
        // take action according to state of engine
        // ======================================
        if (getEngineLifeCycle() == EnginePhrase.APP_STARTED) {

            List<String> acceptableEvents = getMachineAdapter().getAcceptableEvents();
            log("acceptableEvents = " + acceptableEvents);

            QueueDO queueDO = getPathManager().getQueueById(event.eventId);
            if(queueDO!=null){
                if (getComponentManager().hasRegistered(queueDO.stateType)) {

                    if (event.data != null) {
                        queueDO.compInstance.completeListner(event.getEvent(), event.data.toString());
                    } else {
                        queueDO.compInstance.completeListner(event.getEvent(), "");
                    }
                } else {
                    queueDO.compInstance.postExecute(event.getEvent(), event.type, event.data);
                }
            }

            else{
                getMachineAdapter().fire(event.getEvent());
            }


        }

        // still under initialization stage

        if (isEventAckPass(event)) {
            // state of INITIALIZING_APP
            if (getEngineLifeCycle() == EnginePhrase.INITIALIZING_APP) {
                setEngineLifeCycle(EnginePhrase.APP_INITIALIZED);
                initScene(mAppDO);

                // state of INITIALIZING_FLOW
            } else if (getEngineLifeCycle() == EnginePhrase.INITIALIZING_FLOW) {
                setEngineLifeCycle(EnginePhrase.FLOW_INITIALIZED);
                startScene(mAppDO);
            }

        }

        // EVENT_ACK_NOT_MATCHED
        else {
            log(LOG_LEVEL.PLAYER, Constants.Notification.EVENT_ACK_NOT_MATCHED);
            systemPlayerProtocol = notifyDeclined(((PlayerEvent) event).eventAck);
            mAppDO.setPlayerProtocolSent(systemPlayerProtocol);
        }

    }

    /**
     * TODO: use this api in the future
     *
     * @param event
     */
    public void fire(EngineEvent event) {
        fire(null, event);
    }

    private boolean isEventAckPass(EngineEvent event) {
        if (mAppDO.similationMode) {
            return true;
        } else {
            return ((PlayerEvent) event).eventAck.equals(mAppDO.playerEventAckSent);
        }
    }

    /**
     * force to go next state
     */
    public void gotoNext() {

    }

    static public class EngineEvent {
        public static final String TYPE = "#ENGINE_EVENT";
        public String eventId = null;
        public String type;
        public Object data;

        public EngineEvent setData(String data) {
//            this.data = new GsonBuilder().create().toJsonTree(json);
            this.data = data;
            return this;
        }

//        public EngineEvent setData(JsonElement data) {
//            this.data = data;
//            return this;
//        }

        public EngineEvent setEvent(String eventId) {
            this.eventId = eventId;
            return this;
        }

        // TODO: workaround to use string to merge data with event, it's better to have a standard (event, data) api
        public String getEvent() {
            return Constants.Runtime.COMPONENT_END + eventId + ((data == null) ? "" : data.toString());
        }
    }

    static public class QAEvent extends EngineEvent {
        public static final String TYPE = "#QA_FINISHED";

        public QAEvent() {
            this.type = TYPE;
        }
    }

    static public class PlayerEvent extends EngineEvent {
        public static final String TYPE = "#PLAYER_RESULT";
        public String eventAck = null;

        public PlayerEvent(String json) throws IOException {
            this.type = TYPE;
            this.data = new GsonBuilder().create().toJsonTree(json);
            parseEvent();
        }

        public PlayerEvent(JsonElement data) throws IOException {
            this.type = TYPE;
            this.data = data;
            parseEvent();
        }

        public PlayerEvent setEventAck(String eventAck) {
            this.eventAck = eventAck;
            return this;
        }

        private void parseEvent() throws IOException {
            // parse player event
            JsonObject eventJson = ((JsonElement) data).getAsJsonObject();
            // check event format
            if (eventJson.get("type").getAsString().equals("ack")) {
                eventAck = eventJson.get("eventAck").getAsString();
            } else {
                throw new IOException();
            }

        }
    }

    static public class SLUEvent extends EngineEvent {
        public static final String TYPE = "#SLU_RESULT";
        public String utterance;

        public SLUEvent(String utterance) {
            this.type = TYPE;
            this.eventId = this.utterance = utterance;
        }

        public SLUEvent setSLUData(ArrayList<HashMap<String, String>> sluData) {
            this.data = sluData;
            return this;
        }

        public ArrayList<HashMap<String, String>> getSLUData() {
            return (ArrayList<HashMap<String, String>>) this.data;
        }
    }

    /**
     * this is the official way to get belief map
     *
     * @return
     */
    public HashMap<String, Object> getBeliefMap() {
        return getBeliefManager().getBeliefMap();
    }

    public String getBelief(String key) {
        // TODO: default value should be defined
        if (getBeliefMap().get(key) == null) {
            getBeliefMap().put(key, "0");
        }
        return getBeliefMap().get(key).toString();
    }

    protected void setBelief(HashMap<String, Object> belief) {
//		getBeliefManager().setBeliefMap(belief);
    }

    /**
     * @return type string of current flow, return null if flow is not started
     * or exited
     */
    public String getCurrentStateType() {
        if (getEngineLifeCycle() != EnginePhrase.APP_STARTED) {
            if (getEngineLifeCycle() != EnginePhrase.APP_EXITED) {
                return null;
            }
        }
        return getMachineAdapter().getStateType();
    }

//    public boolean isParallelState() {
////        return getMachineAdapter().getCurrentStateType().equals(Constants.Unit.PARALLEL);
//        return getPathManager().getQueueById().isParallel;
//    }

    public String getProjectName() {
        return getConfigModel().getProject().get(Constants.Key.NAME).toString();
    }

    public String getCurrentSceneName() {
        return getConfigModel().getSceneName(getCurrentScene());
    }

    public void addEventListener(FlowChangeNotification stateChangeAction) {
        mStateChangeAction = stateChangeAction;
    }

    public void addEventListener(EngineChangeAction changeAction) {
        mEngineChangeListeners.add(changeAction);
    }

    protected void notifyObserver(Object msg) {
        if (mStateChangeAction != null) {
            mStateChangeAction.action(msg);
        }
    }

    public ConfigModelManager getConfigModel() {
        return getConfigModelManager();
    }

    private void setConfigManager(ConfigModelManager mConfigManager) {
        this.setConfigModelManager(mConfigManager);
    }

    public IStateMachineAdapter getMachineAdapter() {
        return mMachineAdapter;
    }

    private void setMachineAdapter(IStateMachineAdapter mMachineAdapter) {
        this.mMachineAdapter = mMachineAdapter;
    }

    /**
     * utilities
     */
    public enum LOG_LEVEL {
        STATE, ENGINE, COMPONENT, DEBUG, PLAYER, TESTING, COMPATIBILITY, TRANSITION
    }

    public void log(String msg) {
        log(LOG_LEVEL.STATE, msg);
    }

    public void log(LOG_LEVEL level, String msg) {

//		if (mSysContext != null) {
//			if (mSysContext.getEngineLogHelper() != null) {
//				mSysContext.getEngineLogHelper().consoleOutputAndOrLog(false, true,
//						"[" + new Date().toString() + "][IRP][" + level.toString() + "] " + msg);
//				System.out.println("[" + new Date().toString() + "][IRP][" + level.toString() + "] " + msg);
//			}
//		} else {
        System.out.println("\t\t[IRP][" + level.toString() + "] " + msg);
//		}
    }

    public SystemContext_SMEngine getSysContext() {
        return mSysContext;
    }

    protected void setSysContext(SystemContext_SMEngine mSysContext) {
        this.mSysContext = mSysContext;
        if (mSysContext != null) {
            setUsrAccMgrService(mSysContext.getUsrAccMgrService());
        }
    }

    public BeliefManager getBeliefManager() {
        return mBeliefManager;
    }

    public void setBeliefManager(BeliefManager mBeliefManager) {
        this.mBeliefManager = mBeliefManager;
    }

    /**
     * @return the mResourceManager
     */
    protected ResourceManager getResourceManager() {
        return mResourceManager;
    }

    /**
     * @param mResourceManager the mResourceManager to set
     */
    protected void setResourceManager(ResourceManager mResourceManager) {
        this.mResourceManager = mResourceManager;
    }

    /**
     * @return the mProductId
     */
    public String getProductId() {
        if (mAppDO.productId != null) {
            return mAppDO.productId;
        } else {
            log(LOG_LEVEL.ENGINE, "[Alert] Product ID is null");
            return "0";
        }
    }

    /**
     * @return the mUserId
     */
    public Long getUserId() {
        if (mUserId == null) {
            log(LOG_LEVEL.ENGINE, "[Alert] User ID is null");
            return 0l;
        } else {
            return mUserId;
        }
    }

    /**
     * @param userId the mUserId to set
     */
    protected void setUserId(Long userId) {
        this.mUserId = userId;
    }

    /**
     * @return the dmContext
     */
    public DialogueManagerContext getDMContext() {
        return dmContext;
    }

    /**
     * Notice that DialogueManagerContext wont get updated when passed-in
     * DialogueManagerContext is null, this means that we assume every
     * DialogueManagerContext is binded to the next coming CSR. Before a CSR is
     * executed and a DialogueManagerContext is consumed, a passed-in null
     * reference will be ignore.
     * <p>
     * TODO: should think more if this will cause problem when the process get
     * extended/changed in the future, and should provide a way to notice system
     * that some problem may arise.
     *
     * @param dmContext the dmContext to set
     */
    protected void setDMContext(DialogueManagerContext dmContext) {
        if (dmContext != null) {
            this.dmContext = dmContext;
        }
    }

    /**
     * @return the mUserAccountDataManagementService
     */
    public UserAccountDataManagementService getUserAccountDataManagementService() {
        return mUserAccountDataManagementService;
    }

    /**
     * @param mUserAccountDataManagementService the mUserAccountDataManagementService to set
     */
    private void setUsrAccMgrService(UserAccountDataManagementService mUserAccountDataManagementService) {
        this.mUserAccountDataManagementService = mUserAccountDataManagementService;
    }

    /**
     * @return the mConfigModelManager
     */
    public ConfigModelManager getConfigModelManager() {
        return mConfigModelManager;
    }

    /**
     * @param mConfigModelManager the mConfigModelManager to set
     */
    private void setConfigModelManager(ConfigModelManager mConfigModelManager) {
        this.mConfigModelManager = mConfigModelManager;
    }

    /**
     * @return the qaEngine
     */
    protected OntologyQuestionAnswerEngine getQaEngine() {
        return qaEngine;
    }

    /**
     * @param qaEngine the qaEngine to set
     */
    private void setQaEngine(OntologyQuestionAnswerEngine qaEngine) {
        this.qaEngine = qaEngine;
    }

    /**
     * @return the isSimulationMode
     */
    public boolean isSimulationMode() {
        return mAppDO.similationMode;
    }

    /**
     * Notify when engine life cycle changed
     *
     * @author Eason_Pai
     */
    public interface EngineChangeAction {
        /**
         * Perform operation
         */
        void action(EnginePhrase output);
    }

    /**
     * Notify when state changed
     *
     * @author Eason_Pai
     */
    public interface FlowChangeNotification {
        /**
         * Perform operation
         */
        void action(Object output);
    }

    /**
     * print out path history of flow of current app
     */
    public String logHistory() {
        return getPathManager().logHistory();
    }

    public void gotoState(String state) {

    }

    /**
     * go to previous state
     */
    public void gotoPrevious() {
        getMachineAdapter().gotoPrevious();
    }

    /**
     * @return the mEngineLifeCycle
     */
    public EnginePhrase getEngineLifeCycle() {
        return mEngineLifeCycle;
    }

    public FlowEngineAppDO getAppDO() {
        return mAppDO;
    }

    /**
     * @param phrase set the mEngineLifeCycle
     */
    protected void setEngineLifeCycle(EnginePhrase phrase) {
        this.mEngineLifeCycle = phrase;
    }

    /**
     * @return the mComponentManager
     */
    public ComponentManager getComponentManager() {
        return mComponentManager;
    }

    /**
     * @param mComponentManager the mComponentManager to set
     */
    private void setComponentManager(ComponentManager mComponentManager) {
        this.mComponentManager = mComponentManager;
    }

    /**
     * @return the PathManager
     */
    public PathManager getPathManager() {
        return mPathManager;
    }

    /**
     * @param target the PathManager to set
     */
    private void setPathManager(PathManager target) {
        this.mPathManager = target;
    }

    /**
     * @return the mEventManager
     */
    public EventManager getEventManager() {
        return mEventManager;
    }

    /**
     * @param mEventManager the mEventManager to set
     */
    public void setEventManager(EventManager mEventManager) {
        this.mEventManager = mEventManager;
    }

    /**
     * @return the mCurrentSceneId
     */
    public String getCurrentScene() {
        return mAppDO.currentScene;
    }

    /**
     * @param currentSceneId the mCurrentSceneId to set
     */
    private void setCurrentScene(String currentSceneId) {
        this.mAppDO.currentScene = currentSceneId;
    }

    public static String getVersion() {
        return Constants.version;
    }

    public boolean getIsDebugMode() {
        return (devPhrase == DevPhrase.DEBUG);
    }


    // ----------------------------------------------
    // @deprecated area
    // ----------------------------------------------

    /**
     * <pre>
     * init_app example:
     *    {
     * 	  "eventName": "player.general",
     * 	  "data": {
     * 	    "eventAck": "2e9a3974-ab28-4fc6-9ddb-5b4aab287ff4",
     * 	    "config": {
     * 	      "type": "init_app",
     * 	      "productID": "56"
     *        }
     *      }
     *    }
     *
     * init_scene example:
     *    {
     * 	  "eventName": "player.general",
     * 	  "data": {
     * 	    "eventAck": "2e9a3974-ab28-4fc6-9ddb-5b4aab287ff4",
     * 	    "config": {
     * 	      "type": "init_scene",
     * 	      "initScene": "2cd62710-9e80-11e4-c9fd-a190bc04353a"
     *        }
     *      }
     *    }
     * </pre>
     *
     * @param type
     * @param type
     * @param data
     */
    protected HashMap<String, Object> notifyPlayer(PlayerProtocolType type, boolean waitForComplete,
                                                   HashMap<String, Object> data) {

        // generate root node of protocol
        HashMap<String, Object> rootNode = new HashMap<String, Object>();
        rootNode.put(Constants.Runtime.PLAYER__EVENT_NAME,
                Constants.Runtime.PLAYER__EVENT_NAME_PLAYER_GENERAL);

        // init config node
        HashMap<String, Object> configNode;

        // generate id of event ack and add to root
        HashMap<String, Object> dataNode = new HashMap<String, Object>();
        rootNode.put(Constants.Runtime.PLAYER__DATA, dataNode);
        // TODO: confirm if empty uid is needed
        // if (waitForComplete) {
        dataNode.put(Constants.Runtime.PLAYER__EVENT_ACK, getConfigModelManager().getUID());
        // } else {
        // dataNode.put(Constants.Runtime.PLAYER__EVENT_ACK, "");
        // }

        String s = type.toString().toLowerCase();
        if (s.equals(Constants.Runtime.PLAYER__EVENT_INIT_APP)) {
            configNode = new HashMap<String, Object>();
            dataNode.put(Constants.Runtime.PLAYER__CONFIG, configNode);

            configNode.put(Constants.Runtime.PLAYER__TYPE, Constants.Runtime.PLAYER__EVENT_INIT_APP);
            configNode.put(Constants.Runtime.PLAYER__CONFIG_PRODUCTID, getProductId());


        } else if (s.equals(Constants.Runtime.PLAYER__EVENT_INIT_SCENE)) {
            configNode = new HashMap<String, Object>();
            dataNode.put(Constants.Runtime.PLAYER__CONFIG, configNode);

            configNode.put(Constants.Runtime.PLAYER__TYPE, Constants.Runtime.PLAYER__EVENT_INIT_SCENE);
            configNode.put(Constants.Runtime.PLAYER__CONFIG_INITSCENE,
                    data.get(Constants.Runtime.PLAYER__CONFIG_SCENE_ID));


        } else if (s.equals(Constants.Runtime.PLAYER__EVENT_TYPE_COMPONENT)) {
            dataNode.put(Constants.Runtime.PLAYER__CONFIG, data);

        } else {
        }

        // store protocol
        mAppDO.setPlayerProtocolSent((HashMap<String, Object>) rootNode.clone());

        // send event to player
//		JsonElement eventJson = new GsonBuilder().create().toJsonTree(rootNode);
//		if (getSysContext() != null) {
//			getSysContext().getOutboundEventSender().postEvent_Player(this, eventJson);
//		}
//		log(LOG_LEVEL.PLAYER, "sending " + type.toString().toLowerCase() + " event = " + eventJson.toString());
//        log(LOG_LEVEL.PLAYER, "sending " + type.toString().toLowerCase() + " event = ");
        return rootNode;
    }

    /**
     * Notify player state of Flow Engine come to the End-Of-Flow (EOF)
     * <p>
     * <pre>
     * End Of Flow example:
     * {
     * 		  "eventName": "player.general",
     * 		  "data": {
     * 		    "type": "end_of_flow",
     * 		    "message": "end of flow",
     * 		    "productID": "UID-xxxx1", // app product id, 秀出來可以作為debug info
     * 		    "stateId": "UID-xxxx2" // current scene id, 秀出來可以作為debug info
     *          }
     *        }
     * </pre>
     */
    protected HashMap<String, Object> notifyPlayerEOF() {

        // event type
        HashMap<String, Object> rootNode = new HashMap<String, Object>();
        rootNode.put(Constants.Runtime.PLAYER__EVENT_NAME,
                Constants.Runtime.PLAYER__EVENT_NAME_PLAYER_GENERAL);

        // data node
        HashMap<String, Object> dataNode = new HashMap<String, Object>();
        dataNode.put(Constants.Runtime.PLAYER__TYPE, Constants.Runtime.PLAYER__EVENT_TYPE_EOF);

        // data parameters
        dataNode.put(Constants.Runtime.PLAYER__MESSAGE, Constants.Runtime.PLAYER__MESSAGE_EOF);
        dataNode.put(Constants.Runtime.PLAYER__CONFIG_PRODUCTID, getProductId());
        dataNode.put(Constants.Runtime.PLAYER__CONFIG_STATEID, getState());

        // append data
        rootNode.put(Constants.Runtime.PLAYER__DATA, dataNode);

        // send event to player
//		JsonElement eventJson = new GsonBuilder().create().toJsonTree(rootNode);
//		if (getSysContext() != null) {
//			getSysContext().getOutboundEventSender().postEvent_Player(this, eventJson);
//		}

//        log(LOG_LEVEL.PLAYER,
//                "sending " + Constants.Runtime.PLAYER__EVENT_TYPE_EOF + " event = ");
        // store protocol in engine
        mAppDO.setPlayerProtocolSent(rootNode);
        return rootNode;
    }


    /**
     * Notify player state of Flow Engine come to the End-Of-Flow (EOF)
     * <p>
     * <pre>
     * End Of Flow example:
     * {
     * "eventName": "player.general",
     *   "data": {
     *       "type": "event_declined", // event type
     *       "stateID": "089d9600-eca0-11e4-e37b-6f03cfdc0f4d", // 目前當下的state id
     *       "declinedEventAck": "xxxxxxxxxxxxxxxxxxx", // 被擋掉的 event ack
     *        "message": "Event Declined" //  message
     *   }
     * }
     * </pre>
     */
    protected HashMap<String, Object> notifyDeclined(String eventAck) {

        // event type
        HashMap<String, Object> rootNode = new HashMap<String, Object>();
        rootNode.put(Constants.Runtime.PLAYER__EVENT_NAME,
                Constants.Runtime.PLAYER__EVENT_NAME_PLAYER_GENERAL);

        // data node
        HashMap<String, Object> dataNode = new HashMap<String, Object>();
        dataNode.put(Constants.Runtime.PLAYER__TYPE, Constants.Runtime.PLAYER__EVENT_TYPE_DECLINED);

        // data parameters
        dataNode.put(Constants.Runtime.PLAYER__MESSAGE, Constants.Runtime.PLAYER__MESSAGE_EOF);
        dataNode.put(Constants.Runtime.PLAYER__CONFIG_PRODUCTID, getProductId());
        dataNode.put(Constants.Runtime.PLAYER__CONFIG_STATEID, getState());
        dataNode.put(Constants.Runtime.PLAYER__CONFIG_DECLINE_ACK, eventAck);

        // append data
        rootNode.put(Constants.Runtime.PLAYER__DATA, dataNode);

        // send event to player
//		JsonElement eventJson = new GsonBuilder().create().toJsonTree(rootNode);
//		if (getSysContext() != null) {
//			getSysContext().getOutboundEventSender().postEvent_Player(this, eventJson);
//		}

        log(LOG_LEVEL.PLAYER, "sending " + Constants.Runtime.PLAYER__EVENT_TYPE_DECLINED + " event = ");
        return rootNode;
    }

    /**
     * Notify player there are exception out of Component of Flow Engine
     * <p>
     * <pre>
     * Component Exception example:
     * {
     * 		  "eventName": "player.general",
     * 		  "data": {
     * 		    "type": "sys_err_of_component",
     * 		    "message": "XXXX",
     * 			"compType":
     *          }
     *        }
     * </pre>
     */
    protected HashMap<String, Object> notifyPlayerSysErr(String msg, String componentType) {

        // event type
        HashMap<String, Object> rootNode = new HashMap<String, Object>();
        rootNode.put(Constants.Runtime.PLAYER__EVENT_NAME,
                Constants.Runtime.PLAYER__EVENT_NAME_PLAYER_GENERAL);

        // data node
        HashMap<String, Object> dataNode = new HashMap<String, Object>();
        dataNode.put(Constants.Runtime.PLAYER__TYPE,
                Constants.Runtime.PLAYER__EVENT_TYPE_SYSERR_OF_COMP);

        // data parameters
        dataNode.put(Constants.Runtime.PLAYER__MESSAGE, msg);
        dataNode.put(Constants.Runtime.PLAYER__SYSERR_COMP, componentType);
        dataNode.put(Constants.Runtime.PLAYER__CONFIG_PRODUCTID, getProductId());

        // append data
        rootNode.put(Constants.Runtime.PLAYER__DATA, dataNode);

        // send event to player
//		JsonElement eventJson = new GsonBuilder().create().toJsonTree(rootNode);
//		if (getSysContext() != null) {
//			getSysContext().getOutboundEventSender().postEvent_Player(this, eventJson);
//		}

//		log(LOG_LEVEL.PLAYER, eventJson.toString());
        // store protocol in engine
        mAppDO.setPlayerProtocolSent(rootNode);
        return rootNode;
    }

    public Map<String, Object> getPlayerProtocolSent() {
        return mAppDO.playerProtocolSent;
    }

    /**
     * check if Flow Engine currently is in exit app state
     */
    public boolean isAppExited() {
        if (getEngineLifeCycle() == EnginePhrase.APP_EXITED) {
            log(LOG_LEVEL.ENGINE, "app =" + mAppDO.productId + " is exited");
            return true;
        }
        return false;
    }
}
