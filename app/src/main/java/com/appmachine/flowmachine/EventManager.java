package com.appmachine.flowmachine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

@SuppressWarnings({"rawtypes", "unchecked"})
public class EventManager {
	private FlowMachine mFlowEngine;
	private Map<String, Object> mPlayerProtocolSent;

	public EventManager(FlowMachine flowEngine) {
		setFlowEngine(flowEngine);
	}

	static public class EngineEvent {
		public static final String TYPE = "#ENGINE_EVENT";
		public String eventId = null;
		public String type;
		public Map eventMap;
		public String eventAck = null;
		public EngineEvent setData(String json) {
			this.eventMap = parseJSON(json);
			return this;
		}
		public EngineEvent setEvent(String eventId) {
			this.eventId = eventId;
			return this;
		}
		protected void parseEventAck(){
			this.eventAck = ((Map<String, Object>) eventMap.get("data")).get("eventAck").toString();
		}
		protected Map parseJSON(String data) {
			try {
				JSONParser parser = new JSONParser();
				Map model = (Map) parser.parse(data, new ContainerFactory() {
					public List creatArrayContainer() {
						return new LinkedList();
					}
					public Map createObjectContainer() {
						return new LinkedHashMap();
					}
				});
				return model;
			} catch (ParseException ex) {
				ex.printStackTrace();
			} catch (NullPointerException ex) {
				ex.printStackTrace();
			}
			return null;
		}
	}

	static public class QAEvent extends EngineEvent {
		public static final String TYPE = "#QA_FINISHED";
		public QAEvent() {
			this.type = TYPE;
		}
	}

	static public class PlayerEvent extends EngineEvent {
		public static final String TYPE = "#PLAYER_EVENT";
		public PlayerEvent(String json){
			this.type = TYPE;
			this.eventMap = parseJSON(json);
			parseEventAck();
		}
		public PlayerEvent setEventAck(String eventAck) {
			this.eventAck = eventAck;
			return this;
		}
	}

	static public class SLUEvent extends EngineEvent {
		public static final String TYPE = "#SLU_RESULT";
		public String eventJson;
		public String utterance;
		public ArrayList<HashMap<String, String>> sluData;
		public SLUEvent(String json) {
			this.type = TYPE;
			this.eventJson = json;
			this.eventMap = parseJSON(json);
			parseEventAck();
		}
		public SLUEvent setUtterance(String utterance) {
			this.utterance = utterance;
			return this;
		}
		public SLUEvent setSLUData(ArrayList<HashMap<String, String>> sluData) {
			this.sluData = sluData;
			return this;
		}

		public ArrayList<HashMap<String, String>> getSLUData() {
			return this.sluData;
		}
	}
	
	public Map<String, Object> getPlayerProtocolSent() {
		return mPlayerProtocolSent;
	}

	/**
	 * @param playerProtocolSent
	 *            the playerProtocolSent to set
	 */
	public void setPlayerProtocolSent(Map<String, Object> playerProtocolSent) {
		this.mPlayerProtocolSent = playerProtocolSent;
	}

	public String getPlayerEventAckSent() {
		return ((Map<String, Object>) getPlayerProtocolSent().get("data")).get("eventAck").toString();
	}

	/**
	 * 
	 * {
	 * "eventName": "player.general", 
	 * 	"data": { 
	 * 		"type": "ack",
	 * 		"eventAck":"ddb19006-c29b-4051-8bd4-8e5a0a035d73",
	 * 		"result":"success|fail" 
	 * 	} 
	 * }
	 * 
	 * @param sentEventAck
	 * @param eventJson
     * @return
     */
	public boolean validatePlayerEvent(String sentEventAck, String eventJson) {

		// parse json as map
		Map<String, Object> eventMap = getEngine().getConfigModelManager().parseJson(eventJson);

		if (eventMap.containsKey(Constants.Runtime.PLAYER__EVENT_NAME)) {
			if (eventMap.get(Constants.Runtime.PLAYER__EVENT_NAME).equals(
					Constants.Runtime.PLAYER__EVENT_NAME_PLAYER_GENERAL)) {

				// ACK is passed
				String inputEventAck = ((Map) eventMap.get("data")).get(Constants.Runtime.PLAYER__EVENT_ACK).toString();
				
				if (inputEventAck.equals(sentEventAck)) {
					return true;
				}

				// incorrect ack
				else {
					getEngine().log(FlowMachine.LOG_LEVEL.PLAYER, Constants.Notification.EVENT_ACK_NOT_MATCHED);
					notifyDeclined(inputEventAck);
				}
			}

			// incorrect format
			else {
				getEngine().log(FlowMachine.LOG_LEVEL.PLAYER, Constants.Notification.NOT_A_PLAYER_EVENT);
				notifyDeclined(Constants.Notification.NOT_A_PLAYER_EVENT);
			}
		}else{
			getEngine().log(FlowMachine.LOG_LEVEL.PLAYER, Constants.Notification.NOT_A_PLAYER_EVENT);
			notifyDeclined(Constants.Notification.NOT_A_PLAYER_EVENT);
		}

		return false;
	}

	/**
	 * Notify player state of Flow Engine come to the End-Of-Flow (EOF)
	 * 
	 * <pre>
	 * End Of Flow example:
	 * {
	 * "eventName": "player.general",
	 *   "data": {
	 *       "type": "event_declined", // event type
	 *       "stateID": "089d9600-eca0-11e4-e37b-6f03cfdc0f4d", // 目前當下的state id
	 *       "declinedEventAck": "xxxxxxxxxxxxxxxxxxx", // 被擋掉的 event ack
	 *        "message": "Event Declined" //  message
	 *   }
	 * }
	 * </pre>
	 */
	protected HashMap<String, Object> notifyDeclined(String eventAck) {

		// data node
		HashMap<String, Object> dataNode = new HashMap<String, Object>();
		dataNode.put(Constants.Runtime.PLAYER__TYPE,
				Constants.Runtime.PLAYER__EVENT_TYPE_DECLINED);

		// data parameters
		dataNode.put(Constants.Runtime.PLAYER__MESSAGE,
				Constants.Runtime.PLAYER__MESSAGE_EOF);
		dataNode.put(Constants.Runtime.PLAYER__CONFIG_PRODUCTID, getEngine().getProductId());

		if (getEngine().getEngineLifeCycle() == FlowMachine.EnginePhrase.INITIALIZING_APP) {
			dataNode.put(Constants.Runtime.PLAYER__CONFIG_STATEID,
					FlowMachine.EnginePhrase.INITIALIZING_APP.toString());
		} else {
			dataNode.put(Constants.Runtime.PLAYER__CONFIG_STATEID, getEngine().getState());
		}
		dataNode.put(Constants.Runtime.PLAYER__CONFIG_DECLINE_ACK, eventAck);

		// append data
		HashMap<String, Object> rootNode = getPlayerEventScaffold(dataNode);

		// send event to player
//		JsonElement eventJson = new GsonBuilder().create().toJsonTree(rootNode);
//		if (getEngine().getSysContext() != null) {
//			getEngine().getSysContext().getOutboundEventSender().postEvent_Player(this, eventJson);
//		}

//		log(FlowMachine.LOG_LEVEL.PLAYER, eventJson.toString());
		// store protocol in engine
		setPlayerProtocolSent(rootNode);
		return rootNode;
	}

	/**
	 * <pre>
	 * play_content example:
	 * 	{
	 * 	  "eventName": "player.general",
	 * 	  "data": {
	 * 	    "eventAck": "2e9a3974-ab28-4fc6-9ddb-5b4aab287ff4",
	 * 	    "config": {
	 * 	      "type": "play_content",
	 * 	      "productID": "56",
	 * 	      "initScene": "06822500-7770-11e4-e756-899dca689570"
	 * 	    }
	 * 	  }
	 * 	}
	 * </pre>
	 * 
	 * <pre>
	 * return event 
	 * {
	 * 		"eventName": "player.general",
	 * 		"data": {  // must exists
	 * 	  		"type": "ack", // must exists
	 *  		"eventAck": "ddb19006-c29b-4051-8bd4-8e5a0a035d73" // 這個值必須要與上述送出來的 ack 相同
	 *  	      	"result": "success"  |  "fail"    //   新增一個回傳值，表示動作是否成功
	 * 		}
	 * }
	 *
	 * </pre>
	 * 
	 * @param type
	 * @param type
	 * @param protocol
	 */
	protected HashMap<String, Object> notifyPlayer(FlowMachine.PlayerProtocolType type, boolean waitForComplete,
												   HashMap<String, Object> protocol) {

		// when ACK is true
		HashMap<String, Object> dataNode = new HashMap<String, Object>();
		// TODO: confirm if empty uid is needed
		if (waitForComplete) {
			dataNode.put(Constants.Runtime.PLAYER__EVENT_ACK, getEngine().getConfigModelManager()
					.getUID());
		} else {
			dataNode.put(Constants.Runtime.PLAYER__EVENT_ACK, "");
		}
		HashMap<String, Object> rootNode = getPlayerEventScaffold(dataNode);

		switch (type.toString()) {
		case Constants.Runtime.PLAYER__CONFIG_TYPE_PLAYCOTNENT:

			HashMap<String, Object> configNode = new HashMap<String, Object>();
			configNode.put(Constants.Runtime.PLAYER__TYPE,
					Constants.Runtime.PLAYER__CONFIG_TYPE__PLAYCOTNENT);
			configNode.put(Constants.Runtime.PLAYER__CONFIG_PRODUCTID, getEngine().getProductId());
			configNode.put(Constants.Runtime.PLAYER__CONFIG_INITSCENE, protocol.get("sceneId"));

			dataNode.put(Constants.Runtime.PLAYER__CONFIG, configNode);
			break;
		case Constants.Runtime.PLAYER__EVENT_TYPE_COMPONENT:
			dataNode.put(Constants.Runtime.PLAYER__CONFIG, protocol);
			break;
		default:
			break;
		}

		// store protocol
		setPlayerProtocolSent(rootNode);

		// send event to player
		JsonElement eventJson = new GsonBuilder().create().toJsonTree(rootNode);
//		if (getEngine().getSysContext() != null) {
//			getEngine().getSysContext().getOutboundEventSender().postEvent_Player(this, eventJson);
//		}
		log(FlowMachine.LOG_LEVEL.PLAYER, "\t event =" + eventJson.toString());
		return rootNode;
	}

	private HashMap<String, Object> getPlayerEventScaffold(HashMap<String, Object> dataNode) {
		HashMap<String, Object> rootNode = new HashMap<String, Object>();
		rootNode.put(Constants.Runtime.PLAYER__EVENT_NAME,
				Constants.Runtime.PLAYER__EVENT_NAME_PLAYER_GENERAL);
		rootNode.put(Constants.Runtime.PLAYER__DATA, dataNode);
		return rootNode;
	}

	private final String timeout = "timeout";

	public String normalizeEventString(String event) {
		//
		if (event.equals(timeout)) {
			getEngine().log(
					FlowMachine.LOG_LEVEL.COMPATIBILITY,
					"[Compatibility] normalized event string from [" + event + "] to ["
							+ Constants.Unit.OUTPUT_TYPE_TIME_OUT + "]");
			return Constants.Unit.OUTPUT_TYPE_TIME_OUT;
		}
		return event;
	}

	/**
	 * Notify player state of Flow Engine come to the End-Of-Flow (EOF)
	 * 
	 * <pre>
	 * End Of Flow example:
	 * {
	 * 		  "eventName": "player.general",
	 * 		  "data": {
	 * 		    "type": "end_of_flow",
	 * 		    "message": "end of flow",
	 * 		    "productID": "UID-xxxx1", // app product id, 秀出來可以作為debug info
	 * 		    "stateId": "UID-xxxx2" // current scene id, 秀出來可以作為debug info
	 * 		  }
	 * 		}
	 * </pre>
	 */
	protected HashMap<String, Object> notifyPlayerEOF() {

		// data node
		HashMap<String, Object> dataNode = new HashMap<String, Object>();
		dataNode.put(Constants.Runtime.PLAYER__TYPE,
				Constants.Runtime.PLAYER__EVENT_TYPE_EOF);

		// data parameters
		dataNode.put(Constants.Runtime.PLAYER__MESSAGE,
				Constants.Runtime.PLAYER__MESSAGE_EOF);
		dataNode.put(Constants.Runtime.PLAYER__CONFIG_PRODUCTID, getEngine().getProductId());
		dataNode.put(Constants.Runtime.PLAYER__CONFIG_STATEID, getEngine().getState());

		// append data
		HashMap<String, Object> rootNode = getPlayerEventScaffold(dataNode);

		// send event to player
		JsonElement eventJson = new GsonBuilder().create().toJsonTree(rootNode);
//		if (getEngine().getSysContext() != null) {
//			getEngine().getSysContext().getOutboundEventSender().postEvent_Player(this, eventJson);
//		}

		log(FlowMachine.LOG_LEVEL.PLAYER, eventJson.toString());
		// store protocol in engine
		setPlayerProtocolSent(rootNode);
		return rootNode;
	}

	/**
	 * Notify player there are exception out of Component of Flow Engine
	 * 
	 * <pre>
	 * Component Exception example:
	 * {
	 * 	"eventName": "player.general",
	 * 		"data": {
	 * 		"type": "sys_err_of_component",
	 * 		"message": "XXXX",
	 * 			"compType": 
	 * 	}
	 * }
	 * </pre>
	 */
	protected HashMap<String, Object> notifyPlayerSysErr(String msg, String componentType) {

		// data node
		HashMap<String, Object> dataNode = new HashMap<String, Object>();
		dataNode.put(Constants.Runtime.PLAYER__TYPE,
				Constants.Runtime.PLAYER__EVENT_TYPE_SYSERR_OF_COMP);

		// data parameters
		dataNode.put(Constants.Runtime.PLAYER__MESSAGE, msg);
		dataNode.put(Constants.Runtime.PLAYER__SYSERR_COMP, componentType);
		dataNode.put(Constants.Runtime.PLAYER__CONFIG_PRODUCTID, getEngine().getProductId());

		// append data
		HashMap<String, Object> rootNode = getPlayerEventScaffold(dataNode);

		// send event to player
		JsonElement eventJson = new GsonBuilder().create().toJsonTree(rootNode);
//		if (getEngine().getSysContext() != null) {
//			getEngine().getSysContext().getOutboundEventSender().postEvent_Player(this, eventJson);
//		}

		log(FlowMachine.LOG_LEVEL.PLAYER, eventJson.toString());
		// store protocol in engine
		setPlayerProtocolSent(rootNode);

		return rootNode;
	}

	/**
	 * send-out perception event { "eventName": "player.general", "data": {
	 * "eventName": "irp.perception.ctl", "eventAck": "123123XXX", "source":
	 * "module.perception.ds.flowEngine", "config": { "type": "exec",
	 * "eventType": "irp_tts", "serviceName": "module.perception.speech",
	 * "value": { "sentence": "我要說你好嗎", "emotion": "0", "alpha": "0.55" } } } }
	 * 
	 * return event ack { "eventName": "player.general", "data": { "eventName":
	 * "irp.perception.event.trigger", "eventAck": "123123XXX", "source":
	 * "module.perception", "config": { "type":"event_ctl" "eventType":
	 * "ctc_tts", // 註冊的事件名稱 "serviceName": "module.perception.speech", //
	 * 使用的服務名稱 "value":{ "sentence":"我要說你好嗎" } } } }
	 * 
	 * @return
	 */
	public HashMap<String, Object> buildTTSEvent(HashMap<String, Object> parameters) {

		HashMap<String, Object> dataNode = new HashMap<String, Object>();

		// PLAYER__EVENT_NAME
		dataNode.put(Constants.Runtime.PLAYER__EVENT_NAME,
				Constants.Runtime.PLAYER__EVENT_NAME_PERCEPTION);
		// PLAYER__EVENT_ACK
		dataNode.put(Constants.Runtime.PLAYER__EVENT_ACK, getEngine().getConfigModelManager().getUID());
		// PLAYER__EVENT_SOURCE
		dataNode.put(Constants.Runtime.PLAYER__EVENT_SOURCE,
				Constants.Runtime.PLAYER__EVENT_SOURCE__PERCEPTION_DS_FLOWENGINE);

		// PLAYER__CONFIG
		HashMap<String, Object> configNode = new HashMap<String, Object>();
		dataNode.put(Constants.Runtime.PLAYER__CONFIG, configNode);
		// PLAYER__TYPE
		configNode.put(Constants.Runtime.PLAYER__TYPE,
				Constants.Runtime.PLAYER__CONFIG_TYPE__EXEC);
		// PLAYER__CONFIG_EVENTTYPE
		configNode.put(Constants.Runtime.PLAYER__CONFIG_EVENTTYPE,
				Constants.Runtime.PLAYER__CONFIG_EVENTTYPE__TTS);
		// PLAYER__CONFIG_SERVICENAME
		configNode.put(Constants.Runtime.PLAYER__CONFIG_SERVICENAME,
				Constants.Runtime.PLAYER__CONFIG_SERVICENAME__SPEECH);

		// VALUE node
		if (parameters != null) {
			configNode.put(Constants.Key.VALUE, parameters);
		}

		return getPlayerEventScaffold(dataNode);
	}

	/*
	 * return event ack (1) { "eventName": "player.general", "data": {
	 * "eventName": "irp.perception.event.trigger", "eventAck": "123123XXX",
	 * "source": "module.perception", "config": { "type":"event_ctl"
	 * "eventType": "ctc_tts", // 註冊的事件名稱 "serviceName":
	 * "module.perception.speech", // 使用的服務名稱 "value":{ "sentence":"我要說你好嗎" } }
	 * }}(2) { "eventName": "player.general", "data": { // must exists "type":
	 * "ack", // must exists "eventAck": "ddb19006-c29b-4051-8bd4-8e5a0a035d73"
	 * "result": "success" | "fail" } }
	 * 
	 * @return
	 */
	public HashMap<String, Object> buildPlayerEventAck(HashMap<String, Object> configs, String eventAck) {

		HashMap<String, Object> dataNode = new HashMap<String, Object>();

		// PLAYER__EVENT_NAME
		dataNode.put(Constants.Runtime.PLAYER__TYPE, Constants.Runtime.PLAYER__TYPE_ACK);
		// PLAYER__EVENT_ACK
		dataNode.put(Constants.Runtime.PLAYER__EVENT_ACK, eventAck);

		if (configs != null) {
			// PLAYER__CONFIG
			dataNode.put(Constants.Runtime.PLAYER__CONFIG, configs);
		}

		return getPlayerEventScaffold(dataNode);
	}

	/**
	 * 
	 { "eventName": "irp.perception.ctl ", "eventAck": "123123XXX", "source":
	 * "module.perception.ds.flowEngine", "config": { "type": "exec",
	 * "eventType": "irp_csr", "serviceName": "module.perception.speech",
	 * "value": {} } }
	 * 
	 * @return
	 */
	public HashMap<String, Object> buildCSREvent(HashMap<String, Object> parameters) {

		HashMap<String, Object> dataNode = new HashMap<String, Object>();

		// PLAYER__EVENT_NAME
		dataNode.put(Constants.Runtime.PLAYER__EVENT_NAME,
				Constants.Runtime.PLAYER__EVENT_NAME_PERCEPTION);
		// PLAYER__EVENT_ACK
		dataNode.put(Constants.Runtime.PLAYER__EVENT_ACK, getEngine().getConfigModelManager().getUID());
		// PLAYER__EVENT_SOURCE
		dataNode.put(Constants.Runtime.PLAYER__EVENT_SOURCE,
				Constants.Runtime.PLAYER__EVENT_SOURCE__PERCEPTION_DS_FLOWENGINE);

		// PLAYER__CONFIG
		HashMap<String, Object> configNode = new HashMap<String, Object>();
		dataNode.put(Constants.Runtime.PLAYER__CONFIG, configNode);
		// PLAYER__TYPE
		configNode.put(Constants.Runtime.PLAYER__TYPE,
				Constants.Runtime.PLAYER__CONFIG_TYPE__EXEC);
		// PLAYER__CONFIG_EVENTTYPE
		configNode.put(Constants.Runtime.PLAYER__CONFIG_EVENTTYPE,
				Constants.Runtime.PLAYER__CONFIG_EVENTTYPE__CSR);
		// PLAYER__CONFIG_SERVICENAME
		configNode.put(Constants.Runtime.PLAYER__CONFIG_SERVICENAME,
				Constants.Runtime.PLAYER__CONFIG_SERVICENAME__SPEECH);

		// VALUE node
		configNode.put(Constants.Key.VALUE, parameters);

		return getPlayerEventScaffold(dataNode);
	}

	// =========================================================
	// Helpers
	// =========================================================

	private void log(FlowMachine.LOG_LEVEL level, String string) {
		getEngine().log(level, string);
	}

	/**
	 * @return the mFlowEngine
	 */
	public FlowMachine getEngine() {
		return mFlowEngine;
	}

	/**
	 * @param mFlowEngine
	 *            the mFlowEngine to set
	 */
	public void setFlowEngine(FlowMachine mFlowEngine) {
		this.mFlowEngine = mFlowEngine;
	}

}
