package com.appmachine.flowmachine;

import com.appmachine.flowmachine.FlowMachine.LOG_LEVEL;


public class CompatibilityManager {
	private FlowMachine mFlowEngine;

	public CompatibilityManager(FlowMachine flowEngine) {
		mFlowEngine = flowEngine;
	}

	private final String timeout = "timeout";
	public String normalizeEventString(String event) {
		if(event==null){
			return null;
		}
		// 
		if(event.equals(timeout)){
			mFlowEngine.log(LOG_LEVEL.COMPATIBILITY , "[Compatibility] normalized event string from [" + event +"] to [" + Constants.Unit.OUTPUT_TYPE_TIME_OUT +"]");
			return Constants.Unit.OUTPUT_TYPE_TIME_OUT;
		}
		return event;
	}

}
