package com.appmachine.flowmachine;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONValue;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

//import com.asus.atc.client.common.CallResult;
import com.appmachine.flowmachine.FlowMachine.LOG_LEVEL;

@SuppressWarnings({"rawtypes", "unchecked"})
public class ConfigModelManager {

	private static final String EVENT__SCENE_BEGIN = "Scene Begin";
	private static final String EVENT__SCENE_BEGIN2 = "scene_begin";
	private static final String LIB__TYPE_TABLE_BELIEF = "table_belief";
	private Map mModel;
	private Map mProject;
	private Map mConfig;
	private FlowMachine mFlowEngine;
	private ArrayList<Map> mTableBeliefs;
	private JSONParser mParser;

	public ConfigModelManager(FlowMachine flowEngine) {
		mFlowEngine = flowEngine;
		mParser = new JSONParser();
	}

	public void setModel(Map model) {
		mModel = model;
		mProject = (Map) mModel.get("project");
		mConfig = (Map) mProject.get("config");

		// collect table belief info
//		setTableBeliefs(new ArrayList<Map>());
//		for (Map element : (List<Map>) mConfig.get("library")) {
//			if (element.get("type").toString().equals(LIB__TYPE_TABLE_BELIEF)) {
//				// testing print
//				mFlowEngine.log(LOG_LEVEL.ENGINE ,"Load Resources = " + element.get("name")
//						+ " (Table Belief)");
//				getTableBeliefs().add(element);
//				mFlowEngine.getBeliefManager().parseTableBeliefByFile(
//						element.get("name").toString(), getLibraryItemFileNameByMap(element),
//						element.get("sheet").toString());
//			}
//		}
	}

	private String getLibraryItemFileNameByMap(Map element) {
		return ((Map) ((Map) element.get("source")).get("en_US")).get("file").toString();
	}

	public Map getModel() {
		return mModel;
	}

	public String getFirstScene() {
		return ((Map)((List<Map>) mConfig.get("flow")).get(0)).get("id").toString();
	}
	public String getInitScene() {
		return ((Map) mProject.get("env")).get("initScene").toString();
	}
	public String getG11N() {
		return ((Map) mProject.get("env")).get(Constants.Key.G11N).toString();
	}

	public Map getProject() {
		return mProject;
	}

	public String getProjectID() {
		return getProject().get("uid").toString();
	}
	
	public String getLang() {
		return getProject().get("uid").toString();
	}

	public List<Map> getInitSceneFlowUnits() {
		for (Map scene : (List<Map>) mConfig.get("flow")) {
			if (scene.get("id").equals(getInitScene())) {
				return (List<Map>) scene.get("units");
			}
		}
		return null;
	}

	public List<Map> findFlowUnitsBySceneId(String targetScene) {
		for (Map scene : (List<Map>) mConfig.get("flow")) {
			if (scene.get("id").equals(targetScene)) {
				return (List<Map>) scene.get("units");
			}
		}
		return null;
	}

	public String getSceneBeginIdOfInitScene() {

		for (Map unit : getInitSceneFlowUnits()) {
			if (unit.get("type").equals("event")) {
				if (unit.get("eventType").equals(EVENT__SCENE_BEGIN)
						|| unit.get("eventType").equals(EVENT__SCENE_BEGIN2)) {
					return unit.get("id").toString();
				}
			}
		}
		return null;
	}

	// on first level, there are 2 types of output -- default output Type &
	// conditional output type
	public boolean isDefaultOutputType(Map<String, ?> config) {
		for (final Map gotoItem : (List<Map>) config.get("goto")) {
			for (final Map output : (List<Map>) config.get("output")) {
				// find type of exists goto
				String outputId = output.get("id").toString();
				String gotoOutput = gotoItem.get("output").toString();

				if (gotoOutput.equals(outputId)) {
					if (output.get("type").toString().equals("defaultOutputType")) {
						return true;
					}
				}
			}
		}

		return false;
	}
	
	public boolean isParallelEntry(String parallelId, Map component) {
		for (Map input : ((List<Map>) component.get(Constants.Key.INPUT))) {
			if(parallelId.equals(input.get(Constants.Key.ID))){
				return true;
			}
		}
		return false;
	}

	public Map findUnitModelById(String targetScene, String id) {
		return findUnitModelByIdRecursively(findFlowUnitsBySceneId(targetScene), id);
	}

	public Map findCSRModelByIntention(String intention) {
		for (Map scene : (List<Map>) mConfig.get("flow")) {
			for (Map unit : (List<Map>) scene.get("units")) {
				if (unit.get(Constants.Key.TYPE).equals(Constants.Unit.CSR)) {
					if (((List<String>) unit.get(Constants.Unit.CSR_INTENTIONS)).contains(intention)) {
						
						// check if is isolated unit, isolated unit means this unit is not connected with others
						if(find1stPreviousStateByUnitId(unit.get(Constants.Key.ID).toString())!=null){
							return unit;
						}
						
					};
				}
			}
		}
		return null;
	}
	
	public ArrayList findEventModelsByEventType(String eventType) {
		ArrayList<Map> events = new ArrayList<Map>();
		for (Map scene : (List<Map>) mConfig.get("flow")) {
			for (Map unit : (List<Map>) scene.get("units")) {
				if (unit.get(Constants.Key.TYPE).equals(Constants.Unit.EVENT)) {
					if (unit.get(Constants.Unit.EVENT_TYPE).equals(eventType)) {
						events.add(unit);
					}
				}
			}
		}
		return events;
	}
	
	public Map find1stEventModelByEventTypeAndSource(String eventType, String source) {
		ArrayList<Map> events = findEventModelsByEventType(eventType);
		
		// no matching condition
		if(source == null){
			return events.get(0);
		}
		
		for (Map event : events) {
			if(event.get(Constants.Unit.EVENT_SOURCE).equals(source)){
				return event;
			}
		}
		return null;
	}
	
	/**
	 * this is already a hierarchical query method
	 * 
	 * @param id
	 * @return
	 */
	public Map findUnitModelById(String id) {
		for (Map scene : (List<Map>) mConfig.get("flow")) {
			Map result = findUnitModelByIdRecursively( (List<Map>) scene.get("units") , id);
			if(result!=null){
				return result;
			}
		}
		return null;
	}
	
	public Map findUnitModelByIdRecursively(List<Map> findInBase , String id) {
		
		if(findInBase==null) return null;
		
			for (Map unit : findInBase) {
				
				if (unit.get(Constants.Key.ID).equals(id)) {
					return unit;
				};
				
				if(unit.get(Constants.Key.TYPE).equals(Constants.Unit.GROUP)){
					Map insideGroup = findUnitModelByIdRecursively( (List<Map>) unit.get("units"), id );
					if(insideGroup!=null){
						return insideGroup;
					}
				}
				
			}
		return null;
	}

	public Map findBNFModelByIntention(String intention) {
		for (Map element : (List<Map>) mConfig.get(Constants.Key.LIBRARY)) {
			Map source = (Map) element.get(Constants.Key.SOURCE);
			Map en_US = ((Map) source.get("en_US"));
			String intentionStr = en_US.get("intention").toString();
			if (intentionStr.equals(intention)) {
				return element;
			}
		}
		return null;
	}

	public List findBNFSentencesByIntention(String intention) {
		for (Map element : (List<Map>) mConfig.get(Constants.Key.LIBRARY)) {
			if (element.get(Constants.Key.TYPE).equals(Constants.Unit.BNF)) {
				Map source = (Map) element.get(Constants.Key.SOURCE);
				Map en_US = ((Map) source.get("en_US"));
				String intentionStr = en_US.get("intention").toString();
				if (intentionStr.equals(intention)) {
					return ((List) en_US.get("sentenceText"));
				}
			}
		}
		return null;
	}

	public String findNextStateByEventId(Map unit, String event) {
		for (Map output : (List<Map>) unit.get("output")) {
			if (!output.get("type").equals("defaultOutputType")) {
				if (output.get("value").equals(event)) {
					if(unit.containsKey(Constants.Key.GOTO)){
						for (Map go_to : (List<Map>) unit.get("goto")) {
							if (go_to.get("output").equals(output.get("id"))) {
								return go_to.get("id").toString();
							}
						}
					}
				}
			}
		}
		return null;
	}

	public String find1stPreviousStateByUnitId(String id) {

		// find units
		for (Map scene : (List<Map>) mConfig.get(Constants.Key.FLOW)) {
			for (Map unit : (List<Map>) scene.get(Constants.Key.UNITS)) {
				if(unit.containsKey(Constants.Key.GOTO)){
					for (Map go_to : (List<Map>) unit.get(Constants.Key.GOTO)) {
						if (go_to.get(Constants.Key.ID).equals(id)) {
							return unit.get(Constants.Key.ID).toString();
						}
					}
				}else{
					// this unit has no goto scene
				}
			}
		}

		return null;
	}
	
	public Map find1stPreviousStateModelyByUnitId(String id) {
		
		// find units
		for (Map scene : (List<Map>) mConfig.get(Constants.Key.FLOW)) {
			for (Map unit : (List<Map>) scene.get(Constants.Key.UNITS)) {
				if(unit.containsKey(Constants.Key.GOTO)){
					for (Map go_to : (List<Map>) unit.get(Constants.Key.GOTO)) {
						if (go_to.get(Constants.Key.ID).equals(id)) {
							return unit;
						}
					}
				}
			}
		}
		
		return null;
	}
	
	public ArrayList<String> findAllPreviousStatesByUnitId(String id) {
		
		ArrayList<String> ids = new ArrayList<String>();
		// find units
				for (Map scene : (List<Map>) mConfig.get(Constants.Key.FLOW)) {
					for (Map unit : (List<Map>) scene.get(Constants.Key.UNITS)) {
						if(unit.containsKey(Constants.Key.GOTO)){
							
								for (Map go_to : (List<Map>) unit.get(Constants.Key.GOTO)) {
									if (go_to.get(Constants.Key.ID).equals(id)) {
										ids.add(unit.get(Constants.Key.ID).toString());
									}
								}
						}
					}
				}
		return ids;
	}

	public Map getSceneContainsUnitById(String id) {
		for (Map scene : (List<Map>) mConfig.get(Constants.Key.FLOW)) {
			for (Map unit : (List<Map>) scene.get(Constants.Key.UNITS)) {
				if (unit.get(Constants.Key.ID).equals(id)) {
					return scene;
				}
			}
		}
		return null;
	}
	public String getSceneIdContainsUnitById(String id) {
		for (Map scene : (List<Map>) mConfig.get(Constants.Key.FLOW)) {
			for (Map unit : (List<Map>) scene.get(Constants.Key.UNITS)) {
				if (unit.get(Constants.Key.ID).equals(id)) {
					return scene.get(Constants.Key.ID).toString();
				}
			}
		}
		return null;
	}

	public String getSceneName(String targetScene) {
		for (Map scene : (List<Map>) mConfig.get("layout")) {
			if (scene.get("id").equals(targetScene)) {
				return scene.get("name").toString();
			}
		}
		return null;
	}

	public String getSceneBeginIdByScene(String targetScene) {
		for (Map unit : findFlowUnitsBySceneId(targetScene)) {
			if (unit.get("type").equals("event")) {
				if (unit.get("eventType").equals(EVENT__SCENE_BEGIN)
						|| unit.get("eventType").equals(EVENT__SCENE_BEGIN2)) {
					return unit.get("id").toString();
				}
			}
		}
		return null;
	}

	public void parseApp(String jsonString) {
		try {
			Map model = (Map) mParser.parse(jsonString, new ContainerFactory() {
				public List creatArrayContainer() {
					return new LinkedList();
				}

				public Map createObjectContainer() {
					return new LinkedHashMap();
				}
			});

			setModel(model);

		} catch (ParseException ex) {
			ex.printStackTrace();
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
	}
	

	public Map parseJson(String jsonString) {
		try {
			Map model = (Map) mParser.parse(jsonString, new ContainerFactory() {
				public List creatArrayContainer() {
					return new LinkedList();
				}

				public Map createObjectContainer() {
					return new LinkedHashMap();
				}
			});

			return model;

		} catch (ParseException ex) {
			ex.printStackTrace();
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public Map parseJsonData(String jsonString) {
		try {
			Map model = (Map) mParser.parse(jsonString, new ContainerFactory() {
				public List creatArrayContainer() {
					return new LinkedList();
				}

				public Map createObjectContainer() {
					return new LinkedHashMap();
				}
			});

			return model;

		} catch (ParseException ex) {
			ex.printStackTrace();
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public void parse(FileReader configReader) {
		try {
			JSONParser parser = new JSONParser();
			Map model = (Map) parser.parse(configReader, new ContainerFactory() {
				public List creatArrayContainer() {
					return new LinkedList();
				}

				public Map createObjectContainer() {
					return new LinkedHashMap();
				}
			});

			mFlowEngine.log(LOG_LEVEL.ENGINE , "Config Model Parsed");
			// printTestInfo(configModel);
			setModel(model);
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (ParseException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
	}

	// TODO: for testing only, removable
	private void printTestInfo(Map<String, ?> configModel) {
		// testing print
		Iterator iter = configModel.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			mFlowEngine.log(LOG_LEVEL.ENGINE ,entry.getKey() + " = " + entry.getValue() + "\n");
		}

		// testing print
		mFlowEngine.log("[Engine] Config JSON =>\n" + JSONValue.toJSONString(configModel));
	}

	public void getLibraryItemFileNameByType(String type) {

	}

	/**
	 * @return the mTableBeliefs
	 */
	public ArrayList<Map> getTableBeliefs() {
		return mTableBeliefs;
	}

	/**
	 * @param mTableBeliefs
	 *            the mTableBeliefs to set
	 */
	public void setTableBeliefs(ArrayList<Map> mTableBeliefs) {
		this.mTableBeliefs = mTableBeliefs;
	}

	public String getUID() {
		return UUID.randomUUID().toString();
	}

	public String parseCSRSentenceLabel() {
		return UUID.randomUUID().toString();
	}

	public ArrayList<String> parseLabelSyntax(String sentence) {
		Matcher matcher;
		ArrayList<String> result = new ArrayList<String>();
		
		// check if is class type
		matcher = Pattern.compile("<(.*?)>").matcher(sentence);
		boolean isClass = matcher.find();
		
		if(isClass){
			result.add("Class");
			result.add(matcher.group(1));
			return result;
		}
		
		// check if is instance type
		matcher = Pattern.compile("\"(.*?)\"").matcher(sentence);
		boolean isInstance = matcher.find();
		if(isInstance){
			result.add("Instance");
			result.add(matcher.group(1));
			return result;
		}
		
		return null;
	}

	public boolean isContainsConditionalDefaultOutputTypeByUnit(Map unitModel) {
		for (Map gotoElement : (List<Map>) unitModel.get(Constants.Key.GOTO)) {
			for (Map output : (List<Map>) unitModel.get(Constants.Key.OUTPUT)) {
				if(gotoElement.get(Constants.Key.OUTPUT).equals(output.get(Constants.Key.ID))){
					if(output.get(Constants.Key.TYPE).equals(Constants.Key.OUTPUT__TYPE_CONDITIONAL_DEFAULT)){
						return true;
					}
				}
			}
		}
		return false;
	}
	public boolean isContainsDefaultOutputTypeByUnit(Map unitModel) {
		for (Map gotoElement : (List<Map>) unitModel.get(Constants.Key.GOTO)) {
			for (Map output : (List<Map>) unitModel.get(Constants.Key.OUTPUT)) {
				if(gotoElement.get(Constants.Key.OUTPUT).equals(output.get(Constants.Key.ID))){
					if(output.get(Constants.Key.TYPE).equals(Constants.Key.OUTPUT__TYPE_DEFAULT_TYPE)){
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public boolean isNextStateOf(HashMap<String, Object> component, String nextStateId) {
		for (Map gotoItem : (List<Map>) component.get(Constants.Key.GOTO)) {
			if (gotoItem.get(Constants.Key.ID).equals(nextStateId)) {
				return true;
			}
		}
		return false;
	}

	public Map<String, ?> findOutputModelByGotoModel(Map component, Map<String, ?> gotoItem) {
		List<Map<String, ?>> outputs = (List) (component.get(Constants.Key.OUTPUT));
		Map<String, ?> outputFound = null;
		for (Map<String, ?> output : outputs) {
			if (gotoItem.get(Constants.Key.OUTPUT).equals(
					output.get(Constants.Key.ID).toString())) {
				outputFound = output;
				break;
			}
		}
		return outputFound;
	}
	
	public boolean equalLists(List<String> one, List<String> two) {
		if (one == null && two == null) {
			return true;
		}

		if ((one == null && two != null) || one != null && two == null || one.size() != two.size()) {
			return false;
		}

		// to avoid messing the order of the lists we will use a copy
		// as noted in comments by A. R. S.
		one = new ArrayList<String>(one);
		two = new ArrayList<String>(two);

		Collections.sort(one);
		Collections.sort(two);
		return one.equals(two);
	}

}
