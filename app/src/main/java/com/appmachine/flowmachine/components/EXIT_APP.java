package com.appmachine.flowmachine.components;

import java.util.Map;

import com.appmachine.flowmachine.ComponentManager;
import com.appmachine.flowmachine.components.interfaces.IComponent;

/**
 * 
 * { "id": "0138ed10-5060-11e4-9711-21df067afdb6", "goto": [], "type":
 * "exit_app" }
 * 
 * @author Eason_Pai
 *
 */
public class EXIT_APP extends IComponent {

	@Override
	public void execute(String state, Map<String, ?> property) {
		log("[State] " + getProperty("type") + " (" + getProperty("id") + ")");

		// send event to player
		sendPlayerProtocol();
		setEngineStopped();
		waitForComplete("");
	}

}
