package com.appmachine.flowmachine.components;

import java.util.Map;

import com.appmachine.flowmachine.ComponentManager;
import com.appmachine.flowmachine.components.interfaces.IComponent;

/*
 * {
      "type": "if",
      "input": "compareValue",
      "compareValue": "0",
      "operator": "=",
      "compareToValue": "0",
      "id": "d6042a00-7770-11e4-d41b-a3e42f774c45",
      "output": [
        {
          "id": "d6045110-7770-11e4-d41b-a3e42f774c45",
          "type": "defaultOutputType",
          "dataType": "boolean",
          "label": "boolean"
        },
        {
          "id": "d6045111-7770-11e4-d41b-a3e42f774c45",
          "type": "conditionalDefaultOutput",
          "value": "io_true",
          "label": "io_true"
        },
        {
          "id": "d6045112-7770-11e4-d41b-a3e42f774c45",
          "type": "conditionalDefaultOutput",
          "value": "io_false",
          "label": "io_false"
        }
      ],
      "goto": [
        {
          "id": "db4253c0-7770-11e4-d41b-a3e42f774c45",
          "perform": "enter",
          "output": "d6045111-7770-11e4-d41b-a3e42f774c45"
        },
        {
          "id": "e4cbbbc0-7770-11e4-d41b-a3e42f774c45",
          "perform": "enter",
          "output": "d6045112-7770-11e4-d41b-a3e42f774c45"
        }
      ],
      "description": "",
      "setBelief": ""
    }
 * */
public class IF extends IComponent {

	@Override
	public void execute(String state, Map<String, ?> property) {
		log( "[State] "+ getProperty("type") + " (" + getProperty("id") + ")" );
		log( "\t > input = "+ getProperty("compareValue") +","+ getProperty("operator") +","+ getProperty("compareToValue"));
		
		String compareValue = getProperty("compareValue").toString();
		
		// TODO: support more operators
		// operate by type
		switch (getProperty("operator").toString()) {
		case "=":
			if(compareValue.equals(getProperty("compareToValue").toString())){
				complete("io_true" , "true");
			}else{
				complete("io_false", "false");
			}
			break;
		case "!=":
			if(compareValue.equals(getProperty("compareToValue").toString())){
				complete("io_false", "false");
			}else{
				complete("io_true", "true");
			}
			break;
		case "<":
			if(compare(Operator.LESS , compareValue, getProperty("compareToValue"))){
				complete("io_true", "true");
			}else{
				complete("io_false", "false");
			}
			break;
		case "<=":
			if(compare(Operator.LESS_EQUAL , compareValue, getProperty("compareToValue"))){
				complete("io_true", "true");
			}else{
				complete("io_false", "false");
			}
			break;
		case ">":
			if(compare(Operator.GREAT , compareValue, getProperty("compareToValue"))){
				complete("io_true", "true");
			}else{
				complete("io_false", "false");
			}
			break;
		case ">=":
			if(compare(Operator.GREAT_EQUAL , compareValue, getProperty("compareToValue"))){
				complete("io_true", "true");
			}else{
				complete("io_false", "false");
			}
			break;
		default:
			if(compareValue.equals(getProperty("compareToValue").toString())){
				complete("io_true", "true");
			}else{
				complete("io_false", "false");
			}
			break;
		}
	}
	
	private static enum Operator {
		LESS, LESS_EQUAL, GREAT , GREAT_EQUAL
    }
	
	//private int intOperandA;
	//private int intOperandB;
	private double doubleOperandA; //20150721 Clifford Liu
	private double doubleOperandB; //20150721 Clifford Liu	
	
	private boolean compare(Operator operator, String compareValue, Object compareToValue) {
		boolean isCompareToValueNumber = false;
		// perform behavior depending on type of compareToValue
		
		// determine compareToValue type
		try {
			//20150721 Clifford Liu, support number type double
			doubleOperandB = Double.parseDouble(compareToValue.toString());
//			intOperandB = Integer.parseInt(compareToValue.toString());
			
			
			isCompareToValueNumber = true;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		if (isCompareToValueNumber) {
			try {
				// rule -- both int
				// 20150721 Clifford Liu, modify rule to support number type both double and int
				doubleOperandA = Double.parseDouble(compareValue);
//				intOperandA = Integer.parseInt(compareValue);
								
				// operations
				switch (operator) {
					case LESS:
						if( doubleOperandA < doubleOperandB ){
							return true;
						}else{
							return false;
						}
					case LESS_EQUAL:
						if( doubleOperandA <= doubleOperandB ){
							return true;
						}else{
							return false;
						}
					case GREAT:
						if( doubleOperandA > doubleOperandB ){
							return true;
						}else{
							return false;
						}
					case GREAT_EQUAL:
						if( doubleOperandA >= doubleOperandB ){
							return true;
						}else{
							return false;
						}
					default:
						return false;				
				}
				
			} catch (Exception e) { 
				// rule -- b int, a string
				// TODO: should define detailed behaviors
				// string operation, compare the length
				
				// operations
				switch (operator) {
					case LESS:
						if( compareValue.length() < doubleOperandB ){
							return true;
						}else{
							return false;
						}
					case LESS_EQUAL:
						if( compareValue.length() <= doubleOperandB ){
							return true;
						}else{
							return false;
						}
					case GREAT:
						if( compareValue.length() > doubleOperandB ){
							return true;
						}else{
							return false;
						}
					case GREAT_EQUAL:
						if( compareValue.length() >= doubleOperandB ){
							return true;
						}else{
							return false;
						}
					default:
						return false;
				}
			}
		} else {
			// rule -- both string
			// TODO: should define detailed behaviors
			// string operation, compare the length
			switch (operator) {
			case LESS:
				if( compareValue.length() < compareToValue.toString().length() ){
					return true;
				}else{
					return false;
				}
			case LESS_EQUAL:
				if( compareValue.length() <= compareToValue.toString().length() ){
					return true;
				}else{
					return false;
				}
			case GREAT:
				if( compareValue.length() > compareToValue.toString().length() ){
					return true;
				}else{
					return false;
				}
			case GREAT_EQUAL:
				if( compareValue.length() >= compareToValue.toString().length() ){
					return true;
				}else{
					return false;
				}
			default:
				return false;
		}
		}
	}
}
