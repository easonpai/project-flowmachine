package com.appmachine.flowmachine.components.externals;

//import com.asus.ctc.ams.smbdm.ComponentManager;
//import com.asus.ctc.ams.smbdm.IComponent;

import com.appmachine.flowmachine.Constants;
import com.appmachine.flowmachine.components.interfaces.IComponent;

import java.util.List;
import java.util.Map;

public class GEO_FENCE extends IComponent {

	@Override
	public void execute(String state, Map<String, ?> property) {
		log("[State] " + getProperty(Constants.Key.TYPE) + " (" + getProperty(Constants.Key.ID) + ")");
		log("\t > input = " + ((List) getProperty("gps")));
	}
}
