package com.appmachine.flowmachine.components;

import java.util.Map;

import com.appmachine.flowmachine.ComponentManager;
import com.appmachine.flowmachine.components.interfaces.IComponent;

public class GOTO_SCENE extends IComponent {
	
	@Override
	public void execute(String state, Map<String, ?> property) {
		log( "[State] "+getProperty("type") + " (" + getProperty("id") + ")" );
		log( "\t > input = "+ getProperty("scene") );
		
		// send event to player, it is possible to choose not to send protocol
//		sendPlayerProtocol();
		
		gotoScene(getProperty("scene").toString());
		
        // no need to wait for complete
//        complete(getProperty("scene").toString());
        
//		// TODO: output setting is needed
//		// since player protocol is sent, component now will automatically compare returned event ack
//		waitForComplete(getProperty("scene").toString());
	}
	
}
