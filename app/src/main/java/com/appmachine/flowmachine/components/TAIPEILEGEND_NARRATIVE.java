package com.appmachine.flowmachine.components;

//import com.asus.ctc.ams.smbdm.ComponentManager;
//import com.asus.ctc.ams.smbdm.IComponent;

import com.appmachine.flowmachine.Constants;
import com.appmachine.flowmachine.components.interfaces.IComponent;

import java.util.Map;

public class TAIPEILEGEND_NARRATIVE extends IComponent {

	@Override
	public void execute(String state, Map<String, ?> property) {
		log("[State] " + getProperty(Constants.Key.TYPE) + " (" + getProperty(Constants.Key.ID) + ")");
		log( "\t > input = "+ getProperty("eventType") );
		// this is an example of external API for TAIPEILEGEND_NARRATIVE
	}

	@Override
	public void postExecute(String event, String command, Object data) {
		log( "\t > postExecute = "+ event +", " + command +", " + data );
			complete(Constants.Runtime.COMPONENT_END + getProperty("id"), "");
	}
}
