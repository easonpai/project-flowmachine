package com.appmachine.flowmachine.components;

import java.util.Map;

import com.appmachine.flowmachine.ComponentManager;
import com.appmachine.flowmachine.components.interfaces.IComponent;

/*
 *  {
                            "type": "play_sound",
                            "posx": 880,
                            "posy": 100,
                            "label": "Play Sound",
                            "id": "97081aa0-9e80-11e4-a6bf-9f5d96a608ce",
                            "source": "",
                            "repeat": 0,
                            "crossScene": false,
                            "output": [
                                {
                                    "id": "97081aa1-9e80-11e4-a6bf-9f5d96a608ce",
                                    "type": "defaultOutputType",
                                    "dataType": "null",
                                    "label": "null"
                                }
                            ],
                            "goto": [
                                {
                                    "id": "fac6b300-7770-11e4-a8c9-8b96d8c44571",
                                    "perform": "enter",
                                    "output": "97081aa1-9e80-11e4-a6bf-9f5d96a608ce"
                                }
                            ]
                        }
 * */
/**
 * @author Eason_Pai
 */
public class PLAY_SOUND extends IComponent {

	@Override
	public void execute(String state, Map<String, ?> property) {
		log( "[State] "+getProperty("type") + " (" + getProperty("id") + ")");
		log( "\t > input = "+ getProperty("source")+","+ getProperty("repeat")+","+ getProperty("crossScene") );
		
		// send event to player, it is possible to choose not to send protocol
		sendPlayerProtocol();
		
		// TODO: output setting is needed
		// since player protocol is sent, component now will automatically compare returned event ack
		waitForComplete(getProperty("source").toString());
	}
}
