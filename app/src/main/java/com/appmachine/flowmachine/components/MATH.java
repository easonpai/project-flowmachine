package com.appmachine.flowmachine.components;

import java.util.Map;
import java.util.Random;

import com.appmachine.flowmachine.ComponentManager;
import com.appmachine.flowmachine.components.interfaces.IComponent;

/**
 * 
 * Example Config:
 * <pre>
 * {
	 "type": "math",
	 "input": "operandA",
	 "operandA": "${my_belief_2}",
	 "operator": "plus",
	 "operandB": 0,
	 "id": "615dca60-7770-11e4-d41b-a3e42f774c45",
	 "output": [
	 {
	 "id": "615e1880-7770-11e4-d41b-a3e42f774c45",
	 "type": "defaultOutputType",
	 "dataType": "string",
	 "label": "string"
	 }
	 ],
	 "goto": [],
	 "setBelief": ""
 }
 </pre>
 * 
 * */
/**
 * @author Eason_Pai
 *
 */
public class MATH extends IComponent {
	//private int intOperandA;
	//private int intOperandB;
	private double doubleOperandA; //20150721 Clifford Liu
	private double doubleOperandB; //20150721 Clifford Liu
	private boolean isOpAInt = true; //20150721 Clifford Liu
	private boolean isOpBInt = true; //20150721 Clifford Liu

	@Override
	public void execute(String state, Map<String, ?> property) {
		log("[State] " + getProperty("type") + " (" + getProperty("id") + ")");
		log("\t > input = " + getProperty("operandA") + "," + getProperty("operator") + ","
				+ getProperty("operandB"));

		// TODO: parse rule should define more details
		String operandA = getProperty("operandA").toString();
		Object operandB = getProperty("operandB");

		operation(getProperty("operator").toString() , operandA , operandB);
	}
	
	private void operation(String operator, String operandA, Object operandB) {
		// perform behavior depending on type of operandB
		boolean isOperandBValueNumber = false;
		
		// determine operandB type
		try {		
			//20150721 Clifford Liu, support number type double
			String opB = operandB.toString();
			if (opB.contains(".")) { //if string contains "." means the number type is double
				isOpBInt = false;				
			}
			doubleOperandB = Double.parseDouble(operandB.toString());
			
//			intOperandB = Integer.parseInt(operandB.toString());						
			
			isOperandBValueNumber = true;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		if (isOperandBValueNumber) {
			try {
				// rule -- both int
				// 20150721 Clifford Liu, modify rule to support number type both double and int
				if (operandA.contains(".")){					
					isOpAInt = false;
				}
				doubleOperandA = Double.parseDouble(operandA);
				
//				intOperandA = Integer.parseInt(operandA);
				
				// operations
				//20150721 Clifford Liu
				double dAns = 0.0;
				switch (operator) {
					case "plus":
						dAns = doubleOperandA + doubleOperandB;
						break;
					case "minus":
						dAns = doubleOperandA - doubleOperandB;						
						break;
					case "multiply":
						dAns = doubleOperandA * doubleOperandB;						
						break;
					case "divide":
						dAns = doubleOperandA / doubleOperandB;						
						break;
					case "random":
						// start from operandA
						int nextNum = (int)(doubleOperandB-doubleOperandA);
						dAns = doubleOperandA + 1 + new Random().nextInt(nextNum);						
						break;
					case "remainder":
						dAns = doubleOperandA % doubleOperandB;	
						break;
					default:
						dAns = doubleOperandA + doubleOperandB;
						break;
				}
				if (isOpAInt && isOpBInt){ //20150721 Clifford Liu, do here only if both A and B are int
					int iAns = (int) dAns;
					complete(String.valueOf(iAns));
				} else {
					complete(String.format("%.2f", dAns));
				}
				
				
			} catch (Exception e) {
				// rule -- b is int, a is string
				// TODO: define useful behaviors for string operation
				switch (operator) {
					case "plus":
						String opB;
						if (isOpBInt){ //20150721 Clifford Liu
							int iOpB = (int)doubleOperandB;
							opB = String.valueOf(iOpB);
						}							
						else {
							opB = String.valueOf(doubleOperandB);
						}							
						complete(operandA + opB);
						break;
					case "minus":
						// TODO: substring
						complete(operandA.substring(0, operandA.length()-1));
						break;
					case "multiply":
						// TODO: repeat string
						complete(operandA);
						break;
					case "divide":
						// TODO: divide string
						complete(operandA);
						break;
					case "random":
						// TODO: check if operandA is a Belief
						complete(operandB.toString());
						break;
					case "remainder":
						complete(operandB.toString());
					default:
						if (isOpAInt && isOpBInt){
							int iOpA = (int)doubleOperandA;
							int iOpB = (int)doubleOperandB;
							complete(String.valueOf(iOpA + iOpB));
						} else {
							complete(String.valueOf(doubleOperandA + doubleOperandB));
						}
						
						break;
				}
			}
			;
		} else {
			// rule -- both string
			switch (operator) {
				case "plus":
					complete(operandA + operandB.toString());
					break;
				case "minus":
					// TODO: define behavior
					complete(operandA);
					break;
				case "multiply":
					// TODO: define behavior
					complete(operandA);
					break;
				case "divide":
					// TODO: define behavior
					complete(operandB.toString());
					break;
				case "random":
					// TODO: check if operandA is an belief
					complete(operandA);
					break;
				case "remainder":
					// TODO: check if operandA is an belief
					complete(operandA);
					break;
				default:
					if (isOpAInt && isOpBInt){
						int iOpA = (int)doubleOperandA;
						int iOpB = (int)doubleOperandB;
						complete(String.valueOf(iOpA + iOpB));
					} else {
						complete(String.valueOf(doubleOperandA + doubleOperandB));					
					}				
					break;
			}
		}
	}
	

}
