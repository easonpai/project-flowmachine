package com.appmachine.flowmachine.components;

import java.util.Map;

import com.appmachine.flowmachine.ComponentManager;
import com.appmachine.flowmachine.components.interfaces.IComponent;

/**
 * 
 * Example Config:
 * 
 * <pre>
 * {
 *       "type": "csr",
 *       "input": null,
 *       "timeout": 10,
 *       "sentences": [
 *         "yes",
 *         "no"
 *       ],
 *       "id": "e6d7cd60-7770-11e4-de2e-e75581d50c85",
 *       "output": [
 *         {
 *           "id": "e6d7f470-7770-11e4-de2e-e75581d50c85",
 *           "type": "defaultOutputType",
 *           "dataType": "string",
 *           "label": "string"
 *         },
 *         {
 *           "id": "e6d7f471-7770-11e4-de2e-e75581d50c85",
 *           "type": "conditionalOutputs",
 *           "value": "yes",
 *           "label": "yes"
 *         },
 *         {
 *           "id": "e6d81b80-7770-11e4-de2e-e75581d50c85",
 *           "type": "conditionalOutputs",
 *           "value": "no",
 *           "label": "no"
 *         },
 *         {
 *           "id": "e6d81b81-7770-11e4-de2e-e75581d50c85",
 *           "type": "conditionalDefaultOutput",
 *           "value": "time_out",
 *           "label": "time_out"
 *         },
 *         {
 *           "id": "e6d81b82-7770-11e4-de2e-e75581d50c85",
 *           "type": "conditionalDefaultOutput",
 *           "value": "output_default",
 *           "label": "output_default"
 *         }
 *       ],
 *       "goto": [
 *         {
 *           "id": "7f32df30-7770-11e4-e54b-2f2936c929e0",
 *           "perform": "enter",
 *           "output": "e6d81b80-7770-11e4-de2e-e75581d50c85"
 *         },
 *         {
 *           "id": "f3cf9310-7770-11e4-e54b-2f2936c929e0",
 *           "perform": "enter",
 *           "output": "e6d7f471-7770-11e4-de2e-e75581d50c85"
 *         },
 *         {
 *           "id": "7895e160-7770-11e4-e54b-2f2936c929e0",
 *           "perform": "enter",
 *           "output": "e6d81b81-7770-11e4-de2e-e75581d50c85"
 *         }
 *       ],
 *       "description": "",
 *       "setBelief": ""
 *     }
 * </pre>
 * 
 * */
public class SET_PROPERTY extends IComponent {

	@Override
	public void execute(String state, Map<String, ?> property) {
		log("[State] " + getProperty("type") + " (" + getProperty("id") + ")");
		log("\t > input = " + getProperty("target") + "," + getProperty("property") + ","
				+ getProperty("value")+ ", waitForComplete= " + getProperty("waitForComplete"));

			// send event to player, it is possible to choose not to send protocol
			sendPlayerProtocol();
			// TODO: output setting is needed
			// since player protocol is sent, component now will automatically compare returned event ack
			waitForComplete(getProperty("value").toString());
	}

}
