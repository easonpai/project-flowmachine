package com.appmachine.flowmachine.components;

import java.util.Map;

import com.appmachine.flowmachine.ComponentManager;
import com.appmachine.flowmachine.components.interfaces.IComponent;
//import com.asus.atc.client.common.CallResult;

/*
 * {
 "type": "set_belief",
 "input": "value",
 "key": "level",
 "value": "1",
 "id": "e391ee00-7770-11e4-e54b-2f2936c929e0",
 "output": [
 {
 "id": "e3921510-7770-11e4-e54b-2f2936c929e0",
 "type": "defaultOutputType",
 "dataType": "string",
 "label": "string"
 }
 ],
 "goto": [
 {
 "id": "71340e30-5060-11e4-c70e-a1c7777f98bf",
 "perform": "enter",
 "output": "e3921510-7770-11e4-e54b-2f2936c929e0"
 }
 ],
 "description": "",
 "setBelief": ""
 }
 * */
public class SET_BELIEF extends IComponent {

	@Override
	public void execute(String state, Map<String, ?> property) {
		log("[State] " + getProperty("type") + " (" + getProperty("id") + ")");
		log("\t > input = " + getProperty("key") + "," + getProperty("value") + ", "
				+ getProperty("setToDB"));
//		if (getProperty("setToDB") != null) {
//			// save to db
//			if (getProperty("setToDB").toString().equals("true")) {
//				// ////////////////////////////////StoreData
//				// API///////////////////////////////////////
//				try {
//					CallResult call_result = getEngine().getUserAccountDataManagementService()
//							.storeData(getProperty("key").toString(),
//									getProperty("value").toString());
//					if (call_result.isOk())
//						log("StoreData(...) storing a String object is tested ok.");
//					else {
//						log("***** StoreData(...) storing a String object *** FAILED ***.");
//					}
//
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//
//				// //////////////////////////////////////////////////////////////////////////
//			}
//		}

		// save to local
		setBelief(getProperty("key").toString(), getProperty("value").toString());
		complete(getProperty("value").toString());
	}
}
