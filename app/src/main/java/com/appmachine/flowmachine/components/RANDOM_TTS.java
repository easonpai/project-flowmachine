package com.appmachine.flowmachine.components;

import java.util.List;
import java.util.Map;
import java.util.Random;

import com.appmachine.flowmachine.Constants;
import com.appmachine.flowmachine.components.interfaces.IComponent;
import com.appmachine.flowmachine.FlowMachine.LOG_LEVEL;

/**
 * 
 * Example Config:
 * <pre>
 * {
      "type": "random_tts",
      "input": "null",
      "sentences": [
        "Are You Ready?",
        "GO!"
      ],
      "allowRepeat": false,
      "id": "7f32df30-7770-11e4-e54b-2f2936c929e0",
      "output": [
        {
          "id": "7f332d50-7770-11e4-e54b-2f2936c929e0",
          "type": "defaultOutputType",
          "dataType": "string",
          "label": "string"
        },
        {
          "id": "7f332d51-7770-11e4-e54b-2f2936c929e0",
          "type": "conditionalOutputs",
          "value": "Are You Ready?",
          "label": "Are You Ready?"
        },
        {
          "id": "7f332d52-7770-11e4-e54b-2f2936c929e0",
          "type": "conditionalOutputs",
          "value": "GO!",
          "label": "GO!"
        }
      ],
      "goto": [
        {
          "id": "7b770230-5060-11e4-c70e-a1c7777f98bf",
          "perform": "enter",
          "output": "7f332d52-7770-11e4-e54b-2f2936c929e0"
        },
        {
          "id": "7895e160-7770-11e4-e54b-2f2936c929e0",
          "perform": "enter",
          "output": "7f332d51-7770-11e4-e54b-2f2936c929e0"
        }
      ],
      "description": "",
      "setBelief": ""
    }
    </pre>
 * 
 * */
public class RANDOM_TTS extends IComponent {
	private String resultOutput;
	private String resultEvent;

	@Override
	public void execute(String state, Map<String, ?> property) {
		// TODO: pseudo code, should be replace with related module call
		log("[State] " + getProperty("type") + " (" + getProperty("id") + ")");
		log("\t > input = " + getPermittedTriggers());
		
		// get sentences
		List<String> sentences = (List<String>) getProperty("sentences");
		
		// random sentences
		Random random = new Random();
		int seed = random.nextInt(sentences.size());
		resultOutput = sentences.get(seed);
		resultEvent = getConditionalOutputValues().get(seed);
		
		// call TTS module
//		if(getEngine().getSysContext()!=null){
//			getEngine().getSysContext().getOutboundEventSender().postEvent_StartTTS(
//					"flowEngine"/*callerHint*/, this, resultOutput);
//		}
		
		waitForPostExecute();
	}
	
	@Override
	public void postExecute(String event, String command, Object data) {
		log( " \t>postExecute = "+ event +"," + command +"," + data );
		if(Constants.Runtime.EVENT_TTS_COMPLETED.equals(event)){
			complete(resultEvent , resultOutput);
		}else{
			getEngine().log(LOG_LEVEL.DEBUG, " Event is declined in RandomTTS ("+getEngine().getVersion()+")");
		}
	}
	
}
