package com.appmachine.flowmachine.components;

import java.util.Map;

import com.appmachine.flowmachine.Constants;
import com.appmachine.flowmachine.components.interfaces.IComponent;
//import com.asus.atc.client.common.CallResult;

/*
 * {
 "id": "d100c0f0-9e80-11e4-abc0-5fcc47c02a7b",
 "input": "key",
 "description": "",
 "posy": 131,
 "posx": 182,
 "setBelief": "",
 "goto": [],
 "label": "Get Belief",
 "output": [
 {
 "id": "f63411b3-9e80-11e4-b4dc-d92af0bced76",
 "type": "defaultOutputType",
 "dataType": "string",
 "label": "string"
 }
 ],
 "type": "get_belief",
 "key": "User ID"
 }
 * */
public class GET_BELIEF extends IComponent {

	@Override
	public void execute(String state, Map<String, ?> property) {
		log("[State] " + getProperty("type") + " (" + getProperty("id") + ")");
		log("\t > input = " + getProperty("key") +", "+ getProperty("getFromDB"));

		// TODO: should automatically inflate system infos into belief
		switch (getProperty("key").toString()) {
		case Constants.Key.USER_ID:
			// get user id
			complete(getEngine().getUserId().toString());
			return;
		case Constants.Key.APP_ID:
			complete(getEngine().getProductId());
			return;
		case Constants.Key.APP_LANG:
			complete(getEngine().getConfigModelManager().getG11N());
			return;
		}
		
//		if(getProperty("getFromDB")!=null){
//			if (getProperty("getFromDB").toString().equals("true")) {
//				// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//				String retrieved_string = null;
//
//				try {
//					CallResult call_result = getEngine().getUserAccountDataManagementService()
//							.queryStringData(getProperty("key").toString());
//					if (call_result.isOk()) {
//						retrieved_string = (String) call_result.getData(); // fetch the returned data
//					} else {
//						retrieved_string = "0";
//					}
//
//					// update loaded belief to local belief
//					setBelief(getProperty("key").toString(), retrieved_string);
//
//					complete(retrieved_string);
//					return;
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//
//				// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//			}
//		}
		
		complete(getBelief(getProperty("key").toString()));

	}
}
