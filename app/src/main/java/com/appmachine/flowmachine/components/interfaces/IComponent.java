package com.appmachine.flowmachine.components.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import com.appmachine.flowmachine.ComponentManager;
import com.appmachine.flowmachine.Constants;
import com.appmachine.flowmachine.FlowMachine;
import com.appmachine.flowmachine.FlowMachine.LOG_LEVEL;
import com.appmachine.flowmachine.FlowMachine.PlayerProtocolType;
import com.appmachine.flowmachine.PathManager.QueueDO;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * @author Eason_Pai
 *
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public abstract class IComponent {

	protected ComponentManager manager;
	protected Map<String, ?> property;
	private Map<String, Object> mPlayerProtocolSent;
	private String mOutputBelief;
	private String mOutputValue;
	private String mId, mType;
//	private ArrayList<ComponentManager.TableBeliefDataObject> mTableDOs;

	public IComponent() {
	}

	abstract public void execute(String state, Map<String, ?> property);

	/**
	 * call complete and send out event and output
	 * 
	 * @param event
	 *            used to trigger transition of current state
	 * @param output
	 *            is the result value produced by this component
	 */
	public void complete(String event, Object output) {

		getManager().removeRegisteredEvent();

		// reset timer
//		if (waitTimer != null) {
//			waitTimer.stop();
//		}

		getManager().componentEnd(property, event, output);

		// clean references
		property = null;
		manager = null;
	}

	/**
	 * this method is used by external component registration
	 * the difference between complete method is that this method won't removeRegisteredEvent
	 * and clear component
	 *
	 * @param event
	 * @param output
     */
	public void completeListner(String event, Object output) {
		getManager().removeRegisteredEvent();
		getManager().componentEnd(property, event, output);
		// clean references
//		property = null;
//		manager = null;
	}

	/**
	 * call complete() to send out a COMPONENT_END event
	 * 
	 * @param output
	 */
	public void complete(Object output) {
		complete(Constants.Runtime.COMPONENT_END + getProperty("id"), output);
	}

	public void postExecute(String event, String command, Object data) {
		// TODO: make statusData.isWaitForComplete = false;
		// for event, we check only event protocol
		if (isEventAckPass((JsonElement) data)) {
			// TODO: compare source
			complete(Constants.Runtime.COMPONENT_END + getProperty("id"), "");
		}
	}

	private boolean isEventAckPass(JsonElement data) {
		if (getEngine().isSimulationMode()) {
			return true;
		} else {
			JsonObject asJsonObject = ((JsonElement) data).getAsJsonObject();
			return asJsonObject.get(Constants.Runtime.PLAYER__EVENT_ACK).getAsString()
					.equals(getEngine().getAppDO().playerEventAckSent);
		}
	}

	/**
	 * @return the mOutputBelief
	 */
	public String getOutputBelief() {
		return mOutputBelief;
	}

//	protected void appendOutputToTable(ArrayList<ComponentManager.TableBeliefDataObject> tableDOs) {
//		// init table DO
//		if (tableDOs == null)
//			return;
//
//		if (this.mTableDOs == null) {
//			this.mTableDOs = new ArrayList<ComponentManager.TableBeliefDataObject>();
//		}
//		this.mTableDOs.addAll(tableDOs);
//	}

//	protected void appendOutputToTable(int id, String title, String value) {
//		// init table DO
//		if (this.mTableDOs == null) {
//			this.mTableDOs = new ArrayList<ComponentManager.TableBeliefDataObject>();
//		}
//		this.mTableDOs.add(new ComponentManager.TableBeliefDataObject(id, title, value));
//	}

	public void gotoScene(String scene) {
		getEngine().gotoScene(scene);
	}

	public void setupExecute(Map<String, ?> property, ComponentManager manager) {
		setManager(manager);
		setProperty(property);
		execute(this.mId, property);
	}

	public void waitForComplete(String output) {
		ArrayList<QueueDO> queue = getEngine().getPathManager().getRuntimeQueues();
		for (QueueDO statusData : queue) {
			if (statusData.stateId.equals(mId)) {
				statusData.isWaitForComplete = true;
			}
		}

	}

	public void notifyAndWaitForPostExecute(String output) {

		sendPlayerProtocol();

		// if is in simulation mode, no need to wait for player event
		if (getEngine().isSimulationMode()) {
			complete(Constants.Runtime.COMPONENT_END, output);
		} else {
			getManager().registerEvent(this, output);
		}
	}

	public void waitForPostExecute() {
		getManager().registerEventForPostProcess(this);
	}

	private Timer waitTimer;
	private boolean isWaitingForPlayerEvent;

	public void waitForComplete(int time, String output) {
		// TODO: timeout only works when corresponding transiton exists
//		waitTimer = new Timer(time, new ActionListener() {
//			public void actionPerformed(ActionEvent evt) {
//				complete("time_out", "");
//				waitTimer.stop();
//			}
//		});
//		waitTimer.start();

		getManager().registerEvent(this, output);
	}

	protected boolean isEventAckPassed(Object data) {
		// start to parse player event
		JsonObject eventJson = ((JsonElement) data).getAsJsonObject();
		if (eventJson.get("type").getAsString().equals("ack")) {
			// check if key of eventAck is passed
			if (((Map) getEngine().getPlayerProtocolSent().get("data")).get("eventAck").equals(
					eventJson.get("eventAck").getAsString())) {
				// ACK key is passed!
				return true;
			} else {
				// TODO: incorrect ack, need confirm
				getEngine().log(LOG_LEVEL.PLAYER, "Event Ack not matched, nothing triggered");
				return false;
			}
		}
		return false;
	}

	public void setProperty(Map<String, ?> property) {
		this.property = property;

		// some short hand properties
		this.mOutputBelief = getProperty(Constants.Key.SAVE_OUTPUT_TO_BELIEF) == null ? null : getProperty(
				Constants.Key.SAVE_OUTPUT_TO_BELIEF).toString();
		// reset it to null when it is zero length string
		this.mId = property.get(Constants.Key.ID).toString();
		this.mType = property.get(Constants.Key.TYPE).toString();
	}

	public HashMap<String, Object> getRootProperty() {
		HashMap<String, Object> evaluated = new HashMap<String, Object>();
		for (String key : property.keySet()) {
			evaluated.put(key, getProperty(key));
		}
		return evaluated;
	}

	public void setRootProperty(HashMap<String, Object> properties) {
		// save common properties back to map
		// type
		if (!properties.containsKey(Constants.Key.TYPE)) {
			properties.put(Constants.Key.TYPE, property.get(Constants.Key.TYPE));
		}

		if (!properties.containsKey(Constants.Key.ID)) {
			properties.put(Constants.Key.ID, property.get(Constants.Key.ID));
		}

		// belief
		if (!properties.containsKey(Constants.Key.SAVE_OUTPUT_TO_BELIEF)) {
			properties.put(Constants.Key.SAVE_OUTPUT_TO_BELIEF,
					property.get(Constants.Key.SAVE_OUTPUT_TO_BELIEF));
		}
		// erase
		property = properties;
	}

	public ArrayList<String> getConditionalOutputValues() {
		ArrayList<String> outputs = new ArrayList<String>();
		Map<String, ?> sourceProperty = getRootProperty();
		for (Map<String, String> output : (List<Map>) sourceProperty.get("output")) {
			if (output.get(Constants.Key.TYPE).equals(Constants.Key.OUTPUT__TYPE_CONDITIONAL)) {
				outputs.add(output.get(Constants.Key.VALUE));
			}
		}
		return outputs;
	}

	public String parseExpression(String source) {
		return getManager().parseExpression(source);
	}

	/**
	 * 
	 * @param jsonString
	 * @return
	 */
	public Map<String, Object> getJsonAsMap(String jsonString) {
		return getManager().getJsonAsMap(getManager().parseExpression(jsonString));
	}

	/**
	 * @return
	 */
	public Map<String, Object> sendPlayerProtocol() {
		return sendPlayerProtocol(true);
	}

	/**
	 * send event to player, it is possible to choose not to send when this
	 * method is called,
	 * 
	 * @param wait
	 *            a flag tells player that this state is gonna wait for ack or
	 *            not
	 * @return Map protocol
	 */
	public Map<String, Object> sendPlayerProtocol(boolean wait) {
		setWaitingForPlayerEvent(true);
		this.setPlayerProtocolSent(getManager().notifyPlayer(PlayerProtocolType.COMPONENT, wait, getRootProperty()));
		return this.getPlayerProtocolSent();
	}

	/**
	 * send direct event to player, instead of composing config of component
	 * 
	 * @param protocol
	 *            protocol that goes to player directly
	 * @return Map protocol
	 */
	public Map<String, Object> sendPlayerProtocol(HashMap<String, Object> protocol) {
		setWaitingForPlayerEvent(true);
		this.setPlayerProtocolSent(getManager().notifyPlayer(PlayerProtocolType.COMPONENT, true, protocol));
		return this.getPlayerProtocolSent();
	}

	protected String getID() {
		return this.mId;
	}

	public Object getProperty(String key) {
		return getManager().getProperty(property, key);
	}

	// belief
	protected void setBelief(String key, String value) {
		getManager().setBelief(key, value);
	}

	protected String getBelief(String key) {
		return getManager().getBelief(key);
	}

	// utilities
	public void log(String msg) {
		FlowMachine flowEngine = getEngine();
//		if (flowEngine != null && flowEngine.getSysContext() != null
//				&& flowEngine.getSysContext().getOutboundEventSender() != null)
//			flowEngine.getSysContext().getOutboundEventSender()
//					.postEvent_WebSimualtor(this, "[ " + new Date().toString() + " ][IRP] " + msg);

		flowEngine.log(msg);
	}

	/**
	 * exclude system transition
	 * 
	 * @return
	 */
	protected ArrayList<String> getPermittedTriggers() {
		ArrayList<String> original = new ArrayList<String>();
		ArrayList<String> filteredData = new ArrayList<String>();
		original.addAll(getEngine().getMachineAdapter().getAcceptableEventsById(getID()));
		// TODO: need to have a more structure way to exclude system transition
		for (String transition : original) {
			for (String sys_tran : Constants.System.SYSTEM_INNER_TRANSITIONS) {
				if (transition.indexOf(sys_tran) == -1) {
					filteredData.add(transition);
				}
			}
		}
		return filteredData;
	}

	public ComponentManager getManager() {
		return manager;
	}

	public FlowMachine getEngine() {
		return manager.getEngine();
	}

	public void setManager(ComponentManager manager) {
		this.manager = manager;
	}

	/**
	 * @return the isWaitingForPlayerEvent
	 */
	public boolean isWaitingForPlayerEvent() {
		return isWaitingForPlayerEvent;
	}

	/**
	 * @param isWaitingForPlayerEvent
	 *            the isWaitingForPlayerEvent to set
	 */
	private void setWaitingForPlayerEvent(boolean isWaitingForPlayerEvent) {
		this.isWaitingForPlayerEvent = isWaitingForPlayerEvent;
	}

	/**
	 * @return the mPlayerProtocolSent
	 */
	public Map<String, Object> getPlayerProtocol() {
		return getPlayerProtocolSent();
	}

	/**
	 * @return the mPlayerProtocolSent
	 */
	public Map<String, Object> getPlayerProtocolSent() {
		return mPlayerProtocolSent;
	}

	/**
	 * @param mPlayerProtocolSent
	 *            the mPlayerProtocolSent to set
	 */
	protected void setPlayerProtocolSent(Map<String, Object> mPlayerProtocolSent) {
		this.mPlayerProtocolSent = mPlayerProtocolSent;
	}

	protected void setEngineStopped() {
		getManager().setEngineStopped();
	}

	/**
	 * @return the mId
	 */
	public String getId() {
		return mId;
	}

	/**
	 * @return the mId
	 */
	public String getType() {
		return mType;
	}

	/**
	 * @return the mOutputValue
	 */
	public String getOutputValue() {
		return mOutputValue;
	}

	/**
	 * @param mOutputValue the mOutputValue to set
	 */
	public void setOutputValue(String mOutputValue) {
		this.mOutputValue = mOutputValue;
	}
}
