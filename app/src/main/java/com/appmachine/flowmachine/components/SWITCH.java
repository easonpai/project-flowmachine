package com.appmachine.flowmachine.components;

import java.util.List;
import java.util.Map;

import com.appmachine.flowmachine.ComponentManager;
import com.appmachine.flowmachine.components.interfaces.IComponent;

/*
 * {
      "type": "switch",
      "id": "443b9420-7770-11e4-d41b-a3e42f774c45",
      "input": "switchValue",
      "cases": [
        "Hello",
        "World"
      ],
      "switchValue": "Hello",
      "goto": [],
      "setBelief": "",
      "output": [
        {
          "id": "7f537281-7770-11e4-d41b-a3e42f774c45",
          "type": "defaultOutputType",
          "dataType": "string",
          "label": "string"
        },
        {
          "id": "7f537282-7770-11e4-d41b-a3e42f774c45",
          "type": "conditionalOutputs",
          "value": "Hello",
          "label": "Hello"
        },
        {
          "id": "7f537283-7770-11e4-d41b-a3e42f774c45",
          "type": "conditionalOutputs",
          "value": "World",
          "label": "World"
        },
        {
          "id": "7f537284-7770-11e4-d41b-a3e42f774c45",
          "type": "conditionalDefaultOutput",
          "value": "output_default",
          "label": "output_default"
        }
      ]
    }
 * */
/**
 * @author Eason_Pai
 *
 */
public class SWITCH extends IComponent {

	@Override
	public void execute(String state, Map<String, ?> property) {
		log( "[State] "+getProperty("type")+ " (" + getProperty("id") + ")" );
		log( "\t > input = "+ getProperty("switchValue") +","+ getProperty("cases"));
		
		String switchValue = getProperty("switchValue").toString();
		for (String case_element : (List<String>) getProperty("cases") ) {
			if(switchValue.equals(case_element)){
				complete(case_element , case_element);
				return;
			}
		}
		complete("output_default" , "");
	}
}
