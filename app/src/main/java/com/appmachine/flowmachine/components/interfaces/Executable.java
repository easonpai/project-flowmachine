package com.appmachine.flowmachine.components.interfaces;

import java.util.Map;

/**
 * Created by eason on 2016/11/15.
 */

public abstract class Executable extends IComponent {
    public void execute(String state, Map<String, ?> property){
    }
}
