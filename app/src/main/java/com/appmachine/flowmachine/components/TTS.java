package com.appmachine.flowmachine.components;

import java.util.Map;

import com.appmachine.flowmachine.ComponentManager;
import com.appmachine.flowmachine.Constants;
import com.appmachine.flowmachine.components.interfaces.IComponent;

/**
 * 
 * Example Config:
 * <pre>
 * {
        "text": "test text",
        "emotion": "0",
        "alpha": "0"
        "description": "",
        "label": "TTS",
        "goto": [
            {
                "id": "7b770230-5060-11e4-c70e-a1c7777f98bf",
                "perform": "enter",
                "output": "71343540-5060-11e4-c70e-a1c7777f98bf"
            }
        ],
        "setBelief": "",
        "type": "tts",
        "output": [
            {
                "id": "71343540-5060-11e4-c70e-a1c7777f98bf",
                "dataType": "string",
                "label": "string",
                "type": "defaultOutputType"
            }
        ]
    }
    </pre>
 * 
 * */
public class TTS extends IComponent {

	@Override
	public void execute(String state, Map<String, ?> property) {
		log( "[State] "+getProperty("type") + " (" + getProperty("id") + ")");
		log( "\t > input = "+ getProperty("text") );
		
		// call TTS module
//		if(getEngine().getSysContext()!=null){
//			getEngine().getSysContext().getOutboundEventSender().postEvent_StartTTS("flowEngine", this, getProperty("text").toString());
//		}
		
		// wait for tts end event
		// TODO: before shorthand method waitForComplete is extended, use postExecute instead 
		waitForPostExecute();
	}
	
	@Override
	public void postExecute(String event, String command, Object data) {
		log( "\t > postExecute = "+ event +", " + command +", " + data );
		
//		complete(event, data);
		complete(data);
	}
	
}
