package com.appmachine.flowmachine.components;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.appmachine.flowmachine.components.interfaces.IComponent;
import com.appmachine.flowmachine.FlowMachine;
import com.appmachine.flowmachine.FlowMachine.EnginePhrase;

public class SET_TIMER extends IComponent {
	long sec;
	long delay;
	private int duration, durationOrig;

	@Override
	public void execute(String state, Map<String, ?> property) {
		log( "[State] "+getProperty("type") + " (" + getProperty("id") + ")");
		log( "\t > input = "+ getProperty("duration") );
		
		// Start Timer Function
        final Timer timer = new Timer();
        
        // THIS STEP IS IMPORTANT
        // listen to engine life cycle and cancel timer
        getEngine().addEventListener(new FlowMachine.EngineChangeAction() {
			@Override
			public void action(EnginePhrase output) {
				if(output == EnginePhrase.INITIALIZING_APP){
					if(timer!=null){
						timer.cancel();
					}
				}
			}
		});
        
        durationOrig = Integer.parseInt(getProperty("duration").toString());
        duration = Integer.parseInt(getProperty("duration").toString());
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
            	duration--;
            	if (duration < 3 ){
//            		log( "\t > timer remaining = " + duration );
            	}
                if (duration== 0){
                	timer.cancel();
                	complete(getProperty("duration").toString());
                }
            }
        }, 0, 1000);
	}

}
