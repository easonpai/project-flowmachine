package com.appmachine.flowmachine.components;

import java.util.Map;

import com.appmachine.flowmachine.ComponentManager;
import com.appmachine.flowmachine.components.interfaces.IComponent;

/**
 * Example Config:
 * <pre>
 * {
      "id": "e446fa50-5060-11e4-9f15-ebaeff15ef64",
      "duration": 2,
      "input": null,
      "goto": [
        {
          "id": "5737dfc0-5060-11e4-cc5d-c99660c267fa",
          "perform": "enter",
          "output": "1c765ec3-5060-11e4-cc5d-c99660c267fa"
        },
        {
          "id": "70d1fb50-5060-11e4-cc5d-c99660c267fa",
          "perform": "enter",
          "output": "1c765ec2-5060-11e4-cc5d-c99660c267fa"
        },
        {
          "id": "7e707660-5060-11e4-cc5d-c99660c267fa",
          "perform": "enter",
          "output": "1c765ec1-5060-11e4-cc5d-c99660c267fa"
        },
        {
          "id": "8c7e06f0-5060-11e4-cc5d-c99660c267fa",
          "perform": "enter",
          "output": "1c765ec0-5060-11e4-cc5d-c99660c267fa"
        },
        {
          "id": "98530ca0-5060-11e4-cc5d-c99660c267fa",
          "perform": "enter",
          "output": "1c7610a2-5060-11e4-cc5d-c99660c267fa"
        }
      ],
      "label": "Parallel",
      "output": [
        {
          "id": "c28dd270-7770-11e4-d41b-a3e42f774c45",
          "type": "defaultOutputType",
          "dataType": "string",
          "label": "string"
        },
        {
          "id": "1c765ec3-5060-11e4-cc5d-c99660c267fa",
          "combo": [
            "1c7610a3-5060-11e4-cc5d-c99660c267fa",
            "1c7637b0-5060-11e4-cc5d-c99660c267fa",
            "1c7637b1-5060-11e4-cc5d-c99660c267fa"
          ],
          "label": "(He) (Ba) (Be)",
          "type": "conditionalComboOutput"
        },
        {
          "id": "1c765ec2-5060-11e4-cc5d-c99660c267fa",
          "combo": [
            "1c7637b0-5060-11e4-cc5d-c99660c267fa",
            "1c7637b1-5060-11e4-cc5d-c99660c267fa"
          ],
          "label": "(Ba) (Be)",
          "type": "conditionalComboOutput"
        },
        {
          "id": "1c765ec1-5060-11e4-cc5d-c99660c267fa",
          "combo": [
            "1c7610a3-5060-11e4-cc5d-c99660c267fa",
            "1c7637b1-5060-11e4-cc5d-c99660c267fa"
          ],
          "label": "(He) (Be)",
          "type": "conditionalComboOutput"
        },
        {
          "id": "1c765ec0-5060-11e4-cc5d-c99660c267fa",
          "combo": [
            "1c7610a3-5060-11e4-cc5d-c99660c267fa",
            "1c7637b0-5060-11e4-cc5d-c99660c267fa"
          ],
          "label": "(He) (Ba)",
          "type": "conditionalComboOutput"
        },
        {
          "id": "1c7610a2-5060-11e4-cc5d-c99660c267fa",
          "value": "time_out",
          "label": "time_out",
          "type": "conditionalDefaultOutput"
        }
      ],
      "regions": [
        "e9429cd0-5060-11e4-9f15-ebaeff15ef64",
        "ed562940-5060-11e4-9f15-ebaeff15ef64",
        "f13deac0-5060-11e4-9f15-ebaeff15ef64"
      ],
      "type": "parallel",
      "timeout": 10
    }
 * </pre>
 * 
 * */
public class PARALLEL extends IComponent {

	@Override
	public void execute(String state, Map<String, ?> property) {
		log( "[State] "+ getProperty("type") +"" );
		log( "\t > input = "+ getProperty("regions") + ", " + getProperty("duration") + ", " + getProperty("timeout") );
		
		// wait for component to complete
//		waitForComplete(getProperty("regions").toString());
//		waitForComplete(Integer.parseInt(getProperty("timeout").toString()) * 1000);
	}

}
