package com.appmachine.flowmachine.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.appmachine.flowmachine.ComponentManager;
import com.appmachine.flowmachine.components.interfaces.IComponent;

/**
 * 
 * Example Config:
 * 
 * <pre>
 * {
 *       "type": "random_choice",
 *       "input": "null",
 *       "choice": [
 *         "Hello",
 *         "World"
 *       ],
 *       "allowRepeat": false,
 *       "id": "b051ce20-7770-11e4-d41b-a3e42f774c45",
 *       "output": [
 *         {
 *           "id": "b051f530-7770-11e4-d41b-a3e42f774c45",
 *           "type": "defaultOutputType",
 *           "dataType": "string",
 *           "label": "string"
 *         },
 *         {
 *           "id": "b0521c40-7770-11e4-d41b-a3e42f774c45",
 *           "type": "conditionalOutputs",
 *           "value": "Hello",
 *           "label": "Hello"
 *         },
 *         {
 *           "id": "b0521c41-7770-11e4-d41b-a3e42f774c45",
 *           "type": "conditionalOutputs",
 *           "value": "World",
 *           "label": "World"
 *         }
 *       ],
 *       "goto": [],
 *       "description": "",
 *       "setBelief": ""
 *     }
 * </pre>
 * 
 * */
public class RANDOM_CHOICE extends IComponent {

	@Override
	public void execute(String state, Map<String, ?> property) {
		log("[State] " + getProperty("type").toString() + "," + getProperty("choice") + ", " +  getProperty("allowRepeat").toString()+ " (" + getProperty("id") + ")");

		// get sentences
		List<String> choices = (List<String>) getProperty("choice");
		log("\t > input = " + choices.toString());


		boolean allowRepeat = (getProperty("allowRepeat").toString().equals("true"))? true:false;
		// random sentences
		int seed = getEngine().getBeliefManager().getRandomInt(getProperty("id").toString(), choices.size(), allowRepeat);
		String result = choices.get(seed);

		ArrayList<String> outputs = getConditionalOutputValues();
		complete(outputs.get(seed), result);
	}
	
}
