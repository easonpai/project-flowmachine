package com.appmachine.flowmachine.components;

import java.util.HashMap;
import java.util.Map;

import com.appmachine.flowmachine.Constants;
import com.appmachine.flowmachine.components.interfaces.IComponent;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class EVENT extends IComponent {
	// default value is true
	private boolean isSingleShot = true;
	private Map<String, Object> protocolSent;
	private String eventAckSent;

	@Override
	public void execute(String state, Map<String, ?> property) {
		log( "[State] " + getProperty(Constants.Key.TYPE) + " (" + getProperty(Constants.Key.ID) + ")");
		log( "\t > input = "+ getProperty("eventType") + "," + getProperty("source") + "," + getProperty("single") );
		
		//	TODO: workaround before Start Point of Scene is added
		if(getProperty("eventType").equals(Constants.Event.SCENE_BEGIN)){
			complete(Constants.Runtime.COMPONENT_END + getProperty("id"), "");
			return;
		}
		
		// TODO: all these usage can be more module-lize
		// event type
		Object singleObj = getProperty("single");
		if(singleObj!=null){
			isSingleShot = (boolean) singleObj;
		}
		
		// revise event type to REGISTER_EVENT
		HashMap<String, Object> newProperties = getRootProperty();
		newProperties.put(Constants.Key.TYPE, Constants.Unit.REGISTER_EVENT);
		
		// reset property and send
		setRootProperty(newProperties);
		protocolSent = sendPlayerProtocol();
		eventAckSent = ((Map) protocolSent.get(Constants.Key.DATA)).get(Constants.Runtime.PLAYER__EVENT_ACK).toString();
		// 
		waitForPostExecute();
	}
	
	@Override
	public void postExecute(String event, String command, Object data) {
		log( "\t > postExecute = "+ event +", " + command +", " + data );
		
		if(data==null){
			log( "\t > " + Constants.Notification.EVENT_DECLINED);
			return;
		}
		
		// for event, we check only event protocol
//		JsonObject asJsonObject = ((JsonElement) data).getAsJsonObject();
//		if(eventAckSent.equals(asJsonObject.get(Constants.Runtime.PLAYER__EVENT_ACK).getAsString())){
			// TODO: compare source
			complete(Constants.Runtime.COMPONENT_END + getProperty("id"), "");
//		}
		
	}
}
