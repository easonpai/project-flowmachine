package com.appmachine.flowmachine;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appmachine.flowmachine.components.interfaces.Executable;
import com.appmachine.flowmachine.FlowMachine.LOG_LEVEL;
import com.appmachine.flowmachine.FlowMachine.PlayerProtocolType;
import com.appmachine.flowmachine.PathManager.QueueDO;
import com.appmachine.flowmachine.components.interfaces.IComponent;
import com.dep.DialogueManagerContext;
import com.dep.OntologyQuestionAnswerEngine;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * @author Eason_Pai
 */
@SuppressWarnings("rawtypes")
public class ComponentManager {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    // component events
    public static final String TTS_END = "tts.completed";

    // package path for components
    private static final String COMP_PKG = "com.appmachine.flowmachine.components.";

    private FlowMachine mFlowEngine;

    private IComponent mEventObserver;
    private boolean mEventWaitingForPostProcess = false;
    private Map<String, Executable> mExecutables;


    public ComponentManager(FlowMachine flowEngine) {
        setEngine(flowEngine);
        mExecutables = new HashMap<>();
    }

    /**
     * according to state
     * determine the final executable behaviors
     *
     * @param state
     */
    public void componentBegin(String state) {

        Map componentConfig = findUnitModelById(state);

        if(componentConfig!=null){

            if (!componentConfig.get(Constants.Key.TYPE).toString().equals(Constants.Unit.PARALLEL)) {

                executeComponent(state);

            }

            else {

                // see if parallel started

                if(getEngine().getPathManager().getParallelQueueById(state)==null){

                    // queue parallel

                    getEngine().getPathManager().addQueue(state, componentConfig);
                }

                    // execute sub-states of parallel, this could vary when sub-states is moving
                    for (Object deepState : getEngine().getMachineAdapter().getSubStates(state)) {

                        QueueDO deepQueue = getEngine().getPathManager().getQueueById(deepState.toString());

                        // sub-state already executed

                        if(deepQueue != null){

                            if(deepQueue.isParallel){

                                // check sub-flows inside parallel

                                componentBegin(deepState.toString());

                            }else{
                                getEngine().log("componentBegin :: "+ deepState.toString()+" already begun");
                            }
                        }

                        // found new component action

                        else{

                            // found a new parallel

                            if(findUnitModelById(deepState.toString()).get(Constants.Key.TYPE).equals(Constants.Unit.PARALLEL)){

                                // execute actions in parallel

                                componentBegin(deepState.toString());
                            }

                            else{

                                // execute action

                                executeComponent(deepState.toString());
                            }
                        }

                    }


            }
        }

    }

    /**
     * execute component action
     *
     * @param state
     * @return
     */
    private IComponent executeComponent(String state) {
        Map componentConfig = findUnitModelById(state);
        if (componentConfig != null) {
            QueueDO queue = getEngine().getPathManager().addQueue(state, componentConfig);
            return setupExecution(componentConfig, queue).compInstance;
        }
        return null;
    }

    /**
     *
     * determine if component action is registered or not
     *
     *
     * @param componentConfig
     * @param queue
     * @return
     */
    private QueueDO setupExecution(Map componentConfig, QueueDO queue) {

        Executable executable = getRegisteredComponentAction(
                componentConfig.get(Constants.Key.TYPE).toString().toUpperCase());

        if (executable != null) {
            queue.compInstance = executable;
            executable.setupExecute(componentConfig, this);

        }else{
            queue.compInstance = executeInternal(componentConfig);
        }

        return queue;
    }


    public void resetListeners() {
        mExecutables.clear();
    }

    public void registerComponent(String component, Executable executable) {
        mExecutables.put(component, executable);
    }

    //TODO: not yet supported
    public boolean hasRegisteredById(String state) {
        return false;
    }

    /**
     * @param type
     * @return
     */
    public boolean hasRegistered(String type) {
        return mExecutables.containsKey(type.toUpperCase());
    }

    // ===========================================================

    /**
     * compute output belief
     *
     * end of life cycle of component, this algorithm defines the most important
     * part related to how to go to next state
     *
     * @param config component config
     * @param output output data that defined by component
     */
    public void componentEnd(Map<String, ?> config, String event, Object output) {

        // reset
        mEventWaitingForPostProcess = false;

        // save output to belief if is needed

        Object saveOutputToBelief = config.get(Constants.Key.SAVE_OUTPUT_TO_BELIEF);
        if (saveOutputToBelief != null) {
            if (saveOutputToBelief.toString().trim().length() > 0) {
                getEngine().log("\t > output = " + output + " ( belief = " + saveOutputToBelief.toString() + ")");
                setBelief(saveOutputToBelief.toString(), output);
            } else {
                getEngine().log("\t > output = [empty]");
            }
        } else {
            getEngine().log("\t > output = [null]");
        }

        // set queue to complete and fire event

        String stateId = config.get(Constants.Key.ID).toString();
        getEngine().getPathManager().setQueueCompleted(stateId);
        getEngine().getMachineAdapter().fire(event);

    }

    public IComponent getListener() {
        return mEventObserver;
    }

    /**
     * When component is waiting for event, any received event will be propagate
     * to here. Here we will do a filter process that filter by command.
     *
     * @param event
     * @param command
     * @param data
     */
    public void delegateEvent(String event, String command, Object data) {

        switch (command) {
            case Constants.Runtime.COMMAND_QA:

                getEngine().log(LOG_LEVEL.ENGINE, Constants.Notification.QA_FINISHED);

                // TODO: should block Component other than CSR to have such behavior
                String currentState = getListener().getRootProperty().get(Constants.Key.ID).toString();

                // TODO: should check last state is feasible to jump
                // go back to last flow
                getEngine().gotoPrevious();

                break;
            case Constants.Runtime.COMMAND_SLU:
                if (mEventWaitingForPostProcess) {
                    getListener().postExecute(event, command, data);
                } else {
                    getEngine().log(LOG_LEVEL.ENGINE, Constants.Notification.EVENT_DECLINED);
                }
                break;
            case Constants.Runtime.COMMAND_PLAYER:

                // handle player event
                if (mEventObserver.isWaitingForPlayerEvent()) {

                    // waitForPostExecute or waitForComplete is being called by
                    // component
                    if (mEventWaitingForPostProcess) {
                        mEventWaitingForPostProcess = false;
                        getListener().postExecute(event, command, data);
                    } else {
                        // start to parse player event
                        JsonObject eventJson = ((JsonElement) data).getAsJsonObject();
                        // check event format
                        if (eventJson.get("type").getAsString().equals("ack")) {
                            // this step is the key to see to pass or not
                            // while event format is correct, pass
                            if (((Map) mEventObserver.getPlayerProtocol().get("data")).get("eventAck").equals(
                                    eventJson.get("eventAck").getAsString())) {
                                // ACK is passed
                                getListener().complete(Constants.Runtime.COMPONENT_END, "");
                            } else {
                                // TODO: incorrect ack, need confirm
                                getEngine().log(LOG_LEVEL.PLAYER, Constants.Notification.EVENT_ACK_NOT_MATCHED);
                            }
                        } else {
                            getEngine().log(LOG_LEVEL.PLAYER, Constants.Notification.NOT_A_PLAYER_EVENT);
                        }
                    }

                } else {
                    getEngine().log(LOG_LEVEL.PLAYER, Constants.Notification.EVENT_DECLINED);
                }

                break;

            default:
                if (mEventObserver.isWaitingForPlayerEvent()) {
                    getEngine().log(LOG_LEVEL.PLAYER,
                            Constants.Notification.NOT_A_PLAYER_EVENT.replace("%", "\"" + event + "\""));
                }
                mEventWaitingForPostProcess = false;
                getListener().postExecute(event, command, data);
                break;
        }
    }

    public void registerEvent(IComponent component, String output) {

        // TODO: deprecated
        mEventWaitingForPostProcess = false;
        mEventObserver = component;
    }

    public void registerEventForPostProcess(IComponent component) {
        mEventWaitingForPostProcess = true;
        mEventObserver = component;
    }

    public void removeRegisteredEvent() {
        mEventObserver = null;
        mEventWaitingForPostProcess = false;
    }

    // public void notifyObserver(Object msg) {
    // getEngine().notifyObserver(msg);
    // }

    /**
     * get property helper
     *
     * @param property
     * @param key
     * @return
     */
    public Object getProperty(Map<String, ?> property, String key) {

        // handle special type
        Object value;
        if (property.containsKey(key)) {
            value = property.get(key);
        } else {
            return null;
        }

        // no need to parse system property
        if (key.equals(Constants.Key.OUTPUT) || key.equals(Constants.Key.GOTO)) {
            return value;
        }

        if (value instanceof List) {
            // TODO: so far we handle only String type, every type except string
            // is ignored
            if (((List) value).size() == 0) {
                return value;
            }
            if (((List) value).get(0) instanceof String) {
                ArrayList<String> parsedList = new ArrayList<String>();
                for (String item : (List<String>) value) {
                    parsedList.add(parseExpression(item.toString()));
                }
                return parsedList;
            } else {
                return value;
            }
        } else if (value instanceof Map) {
            // TODO: parse expression
        } else if (value instanceof String) {
            return parseExpression(value.toString());
        }

        return value;
    }

    /**
     * parse Belief Expression to get value
     *
     * @param source
     * @return
     */
    public String parseExpression(String source) {

        // plain text
        if (!source.contains("$")) {
            return source;
        }

//		// TODO: only 1 table belief is allowed
//		// extract table belief data
//		BeliefManager.SyntaxQueryData syntaxData = getEngine().getBeliefManager().getTableBeliefSyntax(source);
//		// table belief exists, inflate table belief first
//		if (syntaxData != null) {
//			syntaxData.syntax = inflateBelief(syntaxData.syntax);
//			// parse table belief value
//			String tbValue = getEngine().getBeliefManager().getTableBeliefValue(syntaxData.id, syntaxData.syntax);
//			return syntaxData.corpse.replace("#", tbValue);
//		} else {
        return inflateBelief(source);
//		}

    }

    /**
     * Multiple expression is allowed algorithm is not support complex syntax,
     * ex: "{\"username\":\"user a\",\"userid\":${user_id}}", this will fail
     *
     * @param source
     * @return
     */
    private String inflateBelief(String source) {

        // plain text
        if (!source.contains("$")) {
            return source;
        }

        // TODO: modification on string may cause memory leak

        // replace curly braces
        Pattern pattern = Pattern.compile("\\$\\{([^}]*)\\}");
        Matcher matcher = pattern.matcher(source);
        while (matcher.find()) {
            source = source.replaceAll("\\$\\{" + matcher.group(1) + "\\}", getBelief(matcher.group(1)));
        }

        return source;
    }

    /**
     * provide api for both engine and component to set belief with various
     * types, also with table belief data if needed
     *
     * @param key
     * @param value
     */
//	public void setBelief(String key, Object value) {
//		setBelief(key, value);
//	}

//	public void setBelief(String key, Object value, ArrayList<TableBeliefDataObject> tableDOs) {
    public void setBelief(String key, Object value) {

        if (value == null) return;

        // append data to table
//		if (tableDOs != null) {
//			getEngine().getBeliefManager().createTable(key);
//			int id = getEngine().getBeliefManager().getNextAvailableTableIndex(key);
//			for (TableBeliefDataObject item : tableDOs) {
//				getEngine().getBeliefManager().addDataToTable(key, (id + item.tableId - 1), item.tableTitle,
//						item.tableValue);
//				getEngine().log(
//						"extra CSR output, " + key + " = " + (id + item.tableId - 1) + ", " + item.tableTitle + ", "
//								+ item.tableValue);
//			}
//		}

        // cast to normal belief or table belief
        if (value instanceof String) {
            getEngine().getBeliefMap().put(key, value);
        } else if (value instanceof ArrayList) {

//			if (tableDOs == null) {
//				getEngine().getBeliefManager().createTable(key);
//			}
//			for (String item : (ArrayList<String>) value) {
//				int id = getEngine().getBeliefManager().getNextAvailableTableIndex(key);
//				getEngine().getBeliefManager().addDataToTable(key, id, "group", "array");
//				getEngine().getBeliefManager().addDataToTable(key, id, "value", item);
//				getEngine().log("csr tag: save to " + key + " id = " + id + ", value = " + item);
//
//			}
        } else {
            getEngine().getBeliefMap().put(key, value);
        }

        // clean tableDOs
        value = null;
//		tableDOs = null;
    }

    public Map<String, Object> getJsonAsMap(String jsonString) {
        return getEngine().getConfigModel().parseJson(jsonString);
    }

    public String getBelief(String key) {
        // TODO: default value should be defined
        if (getEngine().getBeliefMap().get(key) == null) {
            getEngine().getBeliefMap().put(key, "0");
        }
        return getEngine().getBeliefMap().get(key).toString();
    }

    private IComponent executeInternal(Map<String, ?> componentConfig) {
        try {
            Class<?> clazz = Class.forName(COMP_PKG + componentConfig.get("type").toString().toUpperCase());
            Constructor<?> constructor = clazz.getConstructor();
            IComponent componentInstance = (IComponent) constructor.newInstance();
            componentInstance.setupExecute(componentConfig, this);
            return componentInstance;
            // TODO: profiling to make sure no memory leak
        } catch (NoSuchMethodException ex) {
            logger.info(ex.toString());
            getEngine().getEventManager().notifyPlayerSysErr(ex.toString(), componentConfig.get("type").toString());
        } catch (ClassNotFoundException ex) {
            logger.info(ex.toString());
            getEngine().getEventManager().notifyPlayerSysErr(ex.toString(), componentConfig.get("type").toString());
        } catch (InstantiationException ex) {
            logger.info(ex.toString());
            getEngine().getEventManager().notifyPlayerSysErr(ex.toString(), componentConfig.get("type").toString());
        } catch (InvocationTargetException ex) {
            logger.info(ex.toString());
            getEngine().getEventManager().notifyPlayerSysErr(ex.toString(), componentConfig.get("type").toString());
        } catch (IllegalAccessException ex) {
            logger.info(ex.toString());
            getEngine().getEventManager().notifyPlayerSysErr(ex.toString(), componentConfig.get("type").toString());
        } catch (Exception ex) {
            getEngine().getEventManager().notifyPlayerSysErr(ex.toString(), componentConfig.get("type").toString());
        }
        return null;
    }

    /**
     * @return the mFlowEngine
     */
    public FlowMachine getEngine() {
        return mFlowEngine;
    }

    public DialogueManagerContext getDMContext() {
        return getEngine().getDMContext();
    }

    public OntologyQuestionAnswerEngine getQuestionAnswerEngine() {
        return getEngine().getQaEngine();
    }

    public void setEngineStopped() {
        getEngine().setEngineLifeCycle(FlowMachine.EnginePhrase.APP_EXITED);
    }

    /**
     * @param mFlowEngine the mFlowEngine to set
     */
    public void setEngine(FlowMachine mFlowEngine) {
        this.mFlowEngine = mFlowEngine;
    }

    public HashMap<String, Object> notifyPlayer(PlayerProtocolType type, boolean waitForComplete,
                                                HashMap<String, Object> protocol) {
        return getEngine().getEventManager().notifyPlayer(type, waitForComplete, protocol);
    }


    public Executable getRegisteredComponentAction(String name) {
        return mExecutables.get(name);
    }

    // =====================================
    // UTILITY
    // =====================================

    private Map findUnitModelById(String state) {
        return getEngine().getConfigModelManager().findUnitModelById(state);
    }
}
