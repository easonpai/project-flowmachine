package com.appmachine.flowmachine;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.appmachine.flowmachine.components.interfaces.IComponent;
import com.dep.UserAccountDataManagementService;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

//import org.joda.time.DateTime;
//import org.joda.time.Days;
//import org.joda.time.Hours;
//import org.joda.time.Minutes;
//import org.joda.time.Seconds;

//import com.asus.atc.client.common.CallResult;
//import com.asus.atc.client.irp.uadm.api.UserAccountDataManagementService;

/**
 * @author Eason_Pai
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class PathManager {

	private static final int MINUTE_DELTA = 1;
	private static final String SWITCH_FEEDBACK__TOO_LONG = "[Status] Last time you opened this app is % ago, do you want to start this app all over or resume to last status?";
	private HashMap<String, QueueDO> mStoredStateSnapshot;
	private FlowMachine mFlowEngine;
	private UserAccountDataManagementService mUserDataService;
	// store whole runtime flows

	private ArrayList<QueueDO> mHistoryQueues; // store all ran flow

	// TODO: workaround for squirrel , keep track of last completed queue, there are no such information in squirrel
	// TODO: workaround for squirrel , this queue is used to trace sub-parallel, because we can only get the top-most parallel in squirrel
	private QueueDO mLastCompletedQueue;


	private ArrayList<QueueDO> mRuntimeQueues; // store current unfinished flows during runtime


	private ArrayList<QueueDO> mUnfoldQueues; // store unfold & passed flow, without hierarchy
	private StringBuilder mHistoryLog;

	public PathManager(FlowMachine flowEngine) {
		mStoredStateSnapshot = new HashMap<>();
		mHistoryQueues = new ArrayList<>();
		mUnfoldQueues = new ArrayList<>();
		mRuntimeQueues = new ArrayList<>();
		mHistoryLog = new StringBuilder();
		setFlowEngine(flowEngine);

		// check available, while local testing we will get a
		// UserAccountDataManagementService instead of SysContext
		if (getEngine().getSysContext() != null) {
			mUserDataService = getEngine().getSysContext().getUsrAccMgrService();
		} else {
			mUserDataService = getEngine().getUserAccountDataManagementService();
		}
	}

	public QueueDO addQueue(String id, Map component) {
		QueueDO queueDO = new QueueDO(mHistoryQueues.size(), getEngine().getCurrentSceneName(), getEngine()
				.getCurrentScene(), id, component);
		mHistoryQueues.add(queueDO);
		return queueDO;
	}

	public ArrayList<QueueDO> getQueues() {
		return mHistoryQueues;
	}

	public QueueDO getParallelQueueById(String id) {
		for (QueueDO queue : mHistoryQueues) {
			if(queue.stateId.equals(id) && queue.isParallel){
				return queue;
			}
		}
		return null;
	}

	public QueueDO getLastCompletedQueue() {
		return mLastCompletedQueue;
	}

	public QueueDO getQueueById(String id) {
		for (QueueDO queue : mHistoryQueues) {
			if(queue.stateId.equals(id)){
				return queue;
			}
		}
		return null;
	}
	public QueueDO getRuntimeQueueById(String id) {
		for (QueueDO queue : mHistoryQueues) {
			if(queue.stateId.equals(id) && !queue.hasCompleted){
				return queue;
			}
		}
		return null;
	}

	public ArrayList<QueueDO> getRuntimeQueues() {
		ArrayList<QueueDO> result = new ArrayList<>();
		for (QueueDO queue : mHistoryQueues) {
			if(!queue.hasCompleted){
				result.add(queue);
			}
		}
		return result;
	}
	public ArrayList<String> getRuntimeQueueIDs() {
		ArrayList<String> result = new ArrayList<>();
		for (QueueDO queue : mHistoryQueues) {
			if(!queue.hasCompleted){
				result.add(queue.stateId);
			}
		}
		return result;
	}
	public ArrayList<String> getRuntimeQueueIDsWithoutParallel() {
		ArrayList<String> result = new ArrayList<>();
		for (QueueDO queue : mHistoryQueues) {
			if(!queue.hasCompleted && !queue.isParallel){
				result.add(queue.stateId);
			}
		}
		return result;
	}


	public ArrayList<String> getCurrentDeepQueueIds() {
		ArrayList<String> result = new ArrayList<>();
		for (QueueDO queue : mHistoryQueues) {
			if(!queue.hasCompleted){
				result.add(queue.stateId);
			}
		}
		return result;
	}

	/**
	 * set queue to complete, remove queue from deep queue list , also exam if
	 * is parallel reaches its final state
	 * 
	 * @param stateId
	 */
	public void setQueueCompleted(String stateId) {
		for (QueueDO queue : mHistoryQueues) {
			if (queue.stateId.equals(stateId) && !queue.hasCompleted) {

				mLastCompletedQueue = queue;
				queue.hasCompleted = true;
				return;
			}
		}
	}

	public String getPreviousStateInHistory(String stateId) {

		// it is possible to find many previous in history
		ArrayList<QueueDO> founds = new ArrayList<QueueDO>();
		for (QueueDO state : mUnfoldQueues) {
			if (state.config.containsKey(Constants.Key.GOTO)) {
				for (Map go_to : (List<Map>) state.config.get(Constants.Key.GOTO)) {
					if (go_to.get(Constants.Key.ID).equals(stateId)) {
						founds.add(state);
					}
				}
			}
		}

		// our rule here is to get the last one
		if (founds.size() > 0) {
			return founds.get(founds.size() - 1).stateId;
		} else {
			return null;
		}

	}

	public String logHistory() {
		mHistoryLog = new StringBuilder();

		mHistoryLog.append("");
		mHistoryLog.append("============================================================\n");
		mHistoryLog.append("▼ Flow Path ▼ (user id = \"" + getEngine().getUserId() + "\", app id = \""
				+ getEngine().getProductId() + "\", history length =" + mUnfoldQueues.size() + ")\n");
		for (QueueDO element : mHistoryQueues) {
			mHistoryLog.append("\t ├─ (" + element.index + ") scene = \"" + getEngine().getCurrentScene() + "\", state => \""
					+ element.stateType + "\" (" + element.stateId + ")\n");
			if (element.isParallel) {
				for (ArrayList<QueueDO> region : element.regions) {
					// TODO: need to use region DO instead of list of DO
					mHistoryLog.append("\t ├─ region  (" + element.stateId + ")\n");
					for (QueueDO queueDO : region) {
						mHistoryLog.append("\t ├─ ├─ (" + queueDO.index + ") scene = \"" + queueDO.sceneName
								+ "\", state => \"" + queueDO.stateType + "\" (" + queueDO.stateId + ")\n");
					}
				}
			}
		}
		getEngine().log(FlowMachine.LOG_LEVEL.DEBUG, mHistoryLog.toString());
		return mHistoryLog.toString();
	}

	public void saveInstanceState(String appId) {

		// store internally
		mStoredStateSnapshot.put(getStatusId(appId), new QueueDO(appId, getEngine().getCurrentScene(), getEngine()
				.getState().toString(), getEngine().getBeliefMap()));

		// store globally
		// TODO: not so formal way to insert keys into Belief Map
		getEngine().getBeliefMap().put("$app_id", appId);
		getEngine().getBeliefMap().put("$app_scene_id", getEngine().getCurrentScene());
		getEngine().getBeliefMap().put("$app_state_id", getEngine().getState().toString());
		getEngine().getBeliefMap().put("$app_store_time", new Date().getTime());

		try {
			mUserDataService.storeData(getStatusId(appId), getEngine().getBeliefMap());
			getEngine().log("[Status] Save App =" + appId + ", Data =" + getEngine().getBeliefMap().toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public QueueDO restoreInstanceState(String appId) {

		// restore internally
		// StatusData statusData = mStoredState.get(getStatusId(appId));
		// inflate belief data to engine
		// mFlowEngine.setBelief(statusData.stateBelief);
		// return statusData;

		// restore globally
		// TODO: not so formal way to insert keys into Belief Map
//		try {
//			HashMap<String, Object> restoreData = (HashMap<String, Object>) mUserDataService.queryHashMapData(
//					getStatusId(appId)).getData();
//			getEngine().log(FlowMachine.LOG_LEVEL.ENGINE, "[Status] Restore App =" + appId + ", Data =" + restoreData.toString());
//			return new QueueDO(appId, restoreData.get("$app_scene_id").toString(), restoreData.get("$app_state_id")
//					.toString(), restoreData);
//		} catch (Exception e) {
//			e.printStackTrace();
			return null;
//		}

	}

	public void stateChangingAgent(String appId, long lastTime) {
		// calculate time delta
		DateTime timeNow = new DateTime(new Date().getTime());
		DateTime timeLast = new DateTime(lastTime);

		String timePassed = Days.daysBetween(timeLast, timeNow).getDays() + " days, "
				+ Hours.hoursBetween(timeLast, timeNow).getHours() % 24 + " hours, "
				+ Minutes.minutesBetween(timeLast, timeNow).getMinutes() % 60 + " minutes, "
				+ Seconds.secondsBetween(timeLast, timeNow).getSeconds() % 60 + " seconds ";
		getEngine().log(FlowMachine.LOG_LEVEL.ENGINE, "[Status] App(" + appId + ") is opened = " + timePassed + "ago");

		// TODO: agent policy
		// rule 1: minutes delta > 1 (for testing)
		if (Minutes.minutesBetween(timeLast, timeNow).getMinutes() % 60 > MINUTE_DELTA) {
			getEngine().log(SWITCH_FEEDBACK__TOO_LONG.replace("%", timePassed));
			getEngine().log(FlowMachine.LOG_LEVEL.ENGINE,
					"[Status] It's been a while, do you wanna (1) start from beginning or (2) resume last state ?");
		} else {
			getEngine().log(SWITCH_FEEDBACK__TOO_LONG.replace("%", timePassed));
		}
	}

	private String getStatusId(String appId) {
		return "app_status-" + getEngine().getUserId() + "-" + appId;
	}

	public boolean hasStoredState(String appId) {
		return mStoredStateSnapshot.containsKey(getStatusId(appId));
	}

	public void cleanStoredState(String appId) {
		mStoredStateSnapshot.remove(getStatusId(appId));
	}

	public void reset() {
		mRuntimeQueues.clear();
		mHistoryQueues.clear();
		mUnfoldQueues.clear();
	}

	/**
	 * @return the mFlowEngine
	 */
	private FlowMachine getEngine() {
		return mFlowEngine;
	}

	/**
	 * @param mFlowEngine
	 *            the mFlowEngine to set
	 */
	public void setFlowEngine(FlowMachine mFlowEngine) {
		this.mFlowEngine = mFlowEngine;
	}

	// TODO: should extend to support "Group" type
	public class QueueDO {
		public int index;
		String appId;
		String sceneName;
		public String sceneId;
		public String stateId;
		public String stateType;
		public HashMap<String, Object> stateBelief;
		public HashMap<String, Object> config;
		IComponent compInstance = null;
		public boolean isParallel = false;
		public boolean isWaitForComplete = true;
		public boolean isWaitForPostExecute = false;
		public boolean hasCompleted = false;
		// / list (a1 -> a2 -> ...)
		// parallel sub-states - list (b1 -> b2 -> ...)
		// \ list (c1 -> c2 -> ...)
		public ArrayList<ArrayList<QueueDO>> regions;
		public ArrayList<String> parallelPath;
		private ArrayList<String> mParallelFinals;

		public QueueDO(String app, String scene, String state, HashMap<String, Object> belief) {
			appId = app;
			sceneId = scene;
			stateId = state;
			stateBelief = belief;
		}

		public QueueDO(int index, String sceneName, String sceneId, String stateId, Map component) {
			this.index = index;
			this.sceneName = sceneName;
			this.sceneId = sceneId;
			this.stateId = stateId;
			this.config = (HashMap<String, Object>) ((HashMap<String, Object>) component).clone();
			this.stateType = component.get(Constants.Key.TYPE).toString();

			// parallel
			if (this.stateType.equals(Constants.Unit.PARALLEL)) {
				this.isParallel = true;
				this.regions = new ArrayList<ArrayList<QueueDO>>();
				this.parallelPath = new ArrayList<String>();
				// clone list data
				this.mParallelFinals = new ArrayList<String>(
						((List<String>) component.get(Constants.Unit.PARALLEL_FINALSTATES)));
			}
		}

		/**
		 * @return the finalEnds
		 */
		public ArrayList<String> getParallelFinals() {
			return (ArrayList<String>) mParallelFinals.clone();
		}

		public String toString(){
			return "[QueueDO] stateId = " + stateId + ", stateType = " + stateType + ", isParallel = " + isParallel + ", hasCompleted = "+ hasCompleted;
		}
	}

//	public QueueDO findDeepQueueById(String queue) {
//		getEngine().log("findDeepQueueById::getCurrentDeepQueues()" + getCurrentDeepQueues());
//		for (QueueDO queueDO : getCurrentDeepQueues()) {
//			if(queue.equals(queueDO.stateId)){
//				return queueDO;
//			}
//		}
//		return null;
//	}

	public QueueDO findQueueOfPathById(String id) {
		for (QueueDO queueDO : mUnfoldQueues) {
			if (queueDO.stateId.equals(id)) {
				return queueDO;
			}
		}
		return null;
	}

	public QueueDO findQueueOfParallelById(String parallelId, String id) {
		QueueDO parallelQueue = findQueueOfPathById(parallelId);
		for (ArrayList<QueueDO> statusGroup : parallelQueue.regions) {
			for (QueueDO queueDO : statusGroup) {
				if (queueDO.stateId.equals(id)) {
					return queueDO;
				}
			}
		}
		return null;
	}

}
