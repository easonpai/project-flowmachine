package com.dep;

/**
 * Created by easonp on 2016/12/21.
 */
public class SystemContext_SMEngine {
    private UserAccountDataManagementService usrAccMgrService;
    private Object outboundEventSender;

    public UserAccountDataManagementService getUsrAccMgrService() {
        return usrAccMgrService;
    }

    public Object getOutboundEventSender() {
        return outboundEventSender;
    }
}

