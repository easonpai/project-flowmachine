package com.dep;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by easonp on 2016/12/21.
 */
public class DialogueManagerContext {
    private OntologyQuestionAnswerEngine questionAnswerEngine;
    private String bestCorrectedSentence;
    private ArrayList<HashMap<String, String>> flowResults;

    public OntologyQuestionAnswerEngine getQuestionAnswerEngine() {
        return questionAnswerEngine;
    }

    public String getBestCorrectedSentence() {
        return bestCorrectedSentence;
    }

    public void setFlowResults(ArrayList<HashMap<String, String>> flowResults) {
        this.flowResults = flowResults;
    }
}
