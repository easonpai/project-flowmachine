package com.dep;

/**
 * Created by easonp on 2016/12/21.
 */
public class CallResult {
    private boolean ok;
    private Object data;

    public boolean isOk() {
        return ok;
    }

    public Object getData() {
        return data;
    }
}
